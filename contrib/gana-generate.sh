#!/bin/sh
# This file is in the public domain.
#
# Helper script to recompute error codes based on submodule
# Run from exchange/ main directory.
set -eu

domake ()
{
    # $1 -- dir under contrib/
    dir="contrib/$1"

    make -C $dir
}

ensure ()
{
    # $1 -- filename
    # $2 -- src dir under contrib/
    # $3 -- dst dir under ./
    fn="$1"
    src="contrib/$2/$fn"
    dst="./$3/$fn"

    if ! diff $src $dst > /dev/null
    then
        test ! -f $dst || chmod +w $dst
        cp $src $dst
        chmod -w $dst
    fi
}

domake                    sigp
ensure donau_signatures.h sigp src/include
