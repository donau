\def\fileversion{1.7}
\def\filedate{2015/03/31}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                            CLASS pqcrypto.cls
%
%---------------------------------------------------------------------
%
% LaTeX class for TALER deliverable in LaTeX report/article style
% (based on the ECRYPT and CACE and ... deliverable templates)
%
%          \documentclass{taler-short}
%
% uses a report style without tale of contents & list  of figures etc.
%
%          \documentclass[article]{taler}
%
% uses an article style.
%
% version 1.8, 2023.12.01, T. Lange (converted from PQCRYPTO to TALER)
% version 1.7, 2015.03.31, T. Lange (converted from PUFFIN to PQCRYPTO)
% version 1.6, 2012.06.11, D. J. Bernstein (converted from CACE to PUFFIN)
% version 1.5, 21-2-08, Guido Blady (added \wpcontrib and \cacekeywords commands)
% version 1.4, 20-2-08, Guido Blady (converted to CACE deliverable template)
% version 1.3, 5-12-07, Christian Cachin (removed broken PDF graphics opt)
% version 1.2,  2-2-06, Christian Cachin (added \ecryptabstract command)
% version 1.1, 14-2-05, Marc Joye
% version 1.0, 27-1-05, Antoon Bosselaers & Christian Cachin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\typeout{^^J  ***  TALER Class \fileversion\space for LaTeX2e ***^^J}
\ProvidesClass{taler}[\filedate]

%%------------------------ (class options) ----------------------------
\newcommand\mj@class{}
\newif\ifmj@report\mj@reporttrue
\DeclareOption{article}{\mj@reportfalse\renewcommand\mj@class{article}}
\DeclareOption{report}{\renewcommand\mj@class{report}}

\newcommand\mj@papersize{}
\DeclareOption{a4paper}{\renewcommand\mj@papersize{a4}}
\DeclareOption{a5paper}{\renewcommand\mj@papersize{a5}}
\DeclareOption{b5paper}{\renewcommand\mj@papersize{b5}}
\DeclareOption{letterpaper}{\renewcommand\mj@papersize{letter}}
\DeclareOption{legalpaper}{\renewcommand\mj@papersize{legal}}
\DeclareOption{executivepaper}{\renewcommand\mj@papersize{executive}}

\newcommand\mj@ptsize{}
\DeclareOption{10pt}{\renewcommand\mj@ptsize{0}}
\DeclareOption{11pt}{\renewcommand\mj@ptsize{1}}
\DeclareOption{12pt}{\renewcommand\mj@ptsize{2}}

\newcommand\mj@titlep@ge{}
\DeclareOption{titlepage}{\renewcommand\mj@titlep@ge{titlepage}}
\DeclareOption{notitlepage}{\renewcommand\mj@titlep@ge{notitlepage}}

\newcommand\mj@twoside{}
\DeclareOption{twoside}{\renewcommand\mj@twoside{twoside}}
\DeclareOption{oneside}{\renewcommand\mj@twoside{oneside}}

\ExecuteOptions{report,a4paper,11pt,titlepage,twoside}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\mj@class}}
\ProcessOptions
\LoadClass[\mj@papersize paper,1\mj@ptsize pt,\mj@titlep@ge,\mj@twoside]{\mj@class}
%---Chapters automatically start on a new right-hand page
\ifmj@report\@openrighttrue\fi
%%---------------------- (end class options) --------------------------



%%------------------------ (page settings) ----------------------------
\IfFileExists{a4wide.sty}{\RequirePackage{a4wide}}{}
\IfFileExists{url.sty}{\RequirePackage{a4wide}}{\let\url\texttt}
\setlength\arraycolsep{1.4\p@}
\setlength\tabcolsep{1.4\p@}
\def\vtopx{\hsize9cm\vtop}
\def\marginnote#1{\marginpar{\footnotesize #1}}
\pagestyle{myheadings}
%%---------------------- (end page settings) --------------------------



%%---------------------------- (macros) -------------------------------
\newcommand{\F}{{\bfseries F}}
\newcommand{\complet}{{\bfseries TO BE COMPLETED}}
\renewcommand{\pmod}[1]{\allowbreak \mkern 10mu({\operator@font mod}\,\,#1)}
%%-------------------------- (end macros) -----------------------------



%%------------------------- (user options) ----------------------------
%% \title[...]{...}        (mandatory!!!)
%% \author{...}
%% \doccode{...}
%% \leadcontractor{...}
%% \duedate{...}
%% \actualdate{...}
%% \version{...}
%% \dissemination{...}
%% \reportabstract{...}

\long\def\title{\@ifnextchar [{\mj@title}{\mj@title[]}}
\def\mj@title[#1]#2{\gdef\@shorttitle{#1}\gdef\@title{#2}}
\newcommand{\doccode}[1]{\gdef\thedoccode{#1}}
\newcommand{\leadcontractor}[1]{\gdef\theleadcontractor{#1}}
\newcommand{\duedate}[1]{\gdef\theduedate{#1}}
\newcommand{\wpcontrib}[1]{\gdef\thewpcontrib{#1}}
\newcommand{\actualdate}[1]{\gdef\theactualdate{#1}}
\newcommand{\version}[1]{\gdef\theversion{#1}}
\newcommand{\dissemination}[1]{\gdef\thedissemination{#1}}
\renewcommand{\date}[1]{\gdef\@date{\let\today\mjtoday #1}}
\newcommand{\reportabstract}[1]{\gdef\thereportabstract{#1}}
\newcommand{\reportkeywords}[1]{\gdef\thereportkeywords{#1}}
\newcommand{\changetable}[1]{\gdef\thechangetable{#1}}

\edef\mjtoday{%
  \number \day.\space%
  \ifcase \month \or January\or February\or March \or April\or May\or
  June\or July\or August\or September\or October\or November\or 
  December\fi\space \number \year}
\global\let\today\mjtoday

\def\@title{\@empty}
\def\@author{\@empty}
\def\@shorttitle{\@empty}
\def\thedoccode{D.XXX.Y}
\def\theleadcontractor{\\Eindhoven University of Technology\\
{\tt www.taler.net/eurotaler}}
\def\theduedate{\@empty}
\def\theactualdate{\mjtoday}
\def\thewpcontrib{\@empty}
\def\theversion{Revision 1.0}
\def\thedissemination{PU}
\def\@date{\theactualdate\\ \theversion}
\def\thereportabstract{\@empty}
\def\thereportkeywords{\@empty}
\def\thechangetable{\@empty}
%%----------------------- (end user options) --------------------------



%%---------------------- (warnings and errors) ------------------------
\newcommand{\mj@warning}[1]{%
  \GenericWarning {\space \space \space \@spaces \@spaces \@spaces }%
  {TALER Warning: #1}}
%--- Warning without line number
\newcommand{\mj@warning@no@line}[1]{%
  \GenericWarning {\space \space \space \@spaces \@spaces \@spaces }%
  {TALER Warning: #1\@gobble}}
\newcommand{\mj@error}[2]{%
  \GenericError {\space \space \space \@spaces \@spaces \@spaces }%
  {TALER Error: #1}{See the documentation for explanation.}{#2^^J}}
%%-------------------- (end warnings and errors) ----------------------



%%------------------- (maketitle and titlepage) -----------------------

\if@titlepage
  \renewcommand{\maketitle}{%
    \ifx\thetitle\undefined
      \if!\@title!\mj@error{No title is given}{Give a title by
        \string\title{...}\space in the preamble of the document.}
      \fi
    \fi
    \if!\@author!\mj@warning@no@line{No \noexpand\author given}\fi
    \begin{titlepage}%
      \mj@titlepage
    \end{titlepage}
    \newpage
    \pagestyle{empty}

\begin{center}
\begin{tabular}{|c|c|p{10cm}|}\hline
\multicolumn{3}{|c|}{HISTORY OF CHANGES}\\\hline
VERSION&
PUBLICATION &
\multicolumn{1}{c|}{CHANGE}\\
 &
DATE&\\\hline
\thechangetable
\end{tabular}
\end{center}



    \begin{titlepage}
      \def\thanks##1{\unskip{}} 
      \let\footnoterule\relax \let\footnote\thanks 
      \null\vfil \vskip 60\p@
      \begin{center}
        {\LARGE\bfseries \ifx\thetitle\undefined\@title\else\thetitle\fi\par}
        \vskip 3em
        \if!\@author!\relax\else
          {\large \lineskip .75em%
           \begin{tabular}[t]{c}\@author\end{tabular}\par}
        \fi
        \vskip 1.5em
        {\large \@date\par}
      \end{center}
      \renewcommand{\thefootnote}{\fnsymbol{footnote}}
      \footnotetext[0]{The work described in this report has been funded (in
part) by the European Union and by the Swiss State Secretariat for Education,
Research and Innovation in the HORIZON-CL4-2023-HUMAN-01-CNECT call in
project 101135475 TALER. Views and opinions expressed are however those of the author(s) only and do not necessarily reflect those of the European Union. Neither the European Union nor the granting authority can be held responsible for them.}
      \vfil\null
    \end{titlepage}
    \newpage{\pagestyle{empty}\cleardoublepage}
    \pagenumbering{roman}
    \begin{abstract}
      \thereportabstract\\[1em]
      {\bf Keywords: }\thereportkeywords
    \end{abstract}
    \cleardoublepage
    \pagenumbering{arabic}
    \raggedbottom
    \if!\@shorttitle!\let\@shorttitle\@title\fi
    \markboth{TALER --- Taxable Anonymous Libre Electronic Reserves}{\thedoccode\ --- 
      \ifx\thetitle\undefined \@shorttitle\else\thetitle\fi}}
\else
  \renewcommand{\maketitle}{%
    \if!\@title!\mj@error{No title is given}{Give a title by
      \string\title{...}\space in the preamble of the document.}
    \fi
    \if!\@author!\mj@warning@no@line{No \noexpand\author given}\fi
    \@maketitle}
\fi

%---Title page for a report/article
\newcommand{\mj@titlepage}{%
  \begin{center}
    \includegraphics[width=0.3\textwidth]{../logo-NGI_TALER_Bold.png}
%    \hfill
%  	\includegraphics[width=80pt]{flag-with-h2020.jpg}
  \end{center}

  \vspace*{0.3cm}

  \begin{center}
    \Large {\bf TALER}\\[0.2cm]
    \Large {\bf Taxable Anonymous Libre Electronic Reserves}\\
  \end{center}

  \vspace*{0.2cm}

  \begin{flushleft}
    Project number: Horizon Europe 101135475\\
  \end{flushleft}

  \vspace*{0.5cm}

  \begin{center}
    {\bfseries\Large \thedoccode\\[3mm]
      \ifx\thetitle\undefined\@title\else\thetitle\fi}
  \end{center}

  \vspace*{1cm}

  \begin{center}
    \begin{tabular}{ll}
      \if!\theduedate!\else Due date of deliverable: \theduedate & \\\fi
      Actual submission date:  \theactualdate & \\[1em]
      \if!\thewpcontrib!\else WP contributing to the deliverable:  \thewpcontrib & \\\fi
    \end{tabular}
  \end{center}

  \vspace*{0.5cm}

  \begin{flushleft}
    Start date of project: 1.\ December 2023 \hfill Duration: 3 years\\[1cm]
    Coordinator: \theleadcontractor
  \end{flushleft}

  \vspace*{0.2cm}
  \begin{flushright}
    \theversion
  \end{flushright}

  \vspace*{0.5cm}

%%% disemination level %%%
%
% put an X next to the dissemination level, 
% if it is public (PU), no modification is needed
%
  {\footnotesize
    \begin{center}
      \begin{tabular}{|l|l|c|}\hline
        \multicolumn{3}{|c|}{\bf Project co-funded by the European
        Commission within Horizon Europe}\\ \hline 
        \multicolumn{3}{|c|}{\bf Dissemination Level}\\ \hline
        {\bfseries PU} & Public &
        \def\mj@tmp{PU}\ifx\thedissemination\mj@tmp X\fi\\ \hline
        {\bfseries PP} & Restricted to other programme participants
        (including the Commission services) &
        \def\mj@tmp{PP}\ifx\thedissemination\mj@tmp X\fi\\ \hline
        {\bfseries RE} & Restricted to a group specified by the consortium
        (including the Commission services) &
        \def\mj@tmp{RE}\ifx\thedissemination\mj@tmp X\fi\\ \hline
        {\bfseries CO} & Confidential, only for members of the consortium
        (including the Commission services) &
        \def\mj@tmp{CO}\ifx\thedissemination\mj@tmp X\fi\\ \hline
      \end{tabular}
    \end{center}}}
%%----------------- (end maketitle and titlepage) ---------------------


\RequirePackage{graphicx}
\RequirePackage{amsmath}
\numberwithin{table}{section}
\numberwithin{figure}{section}

\endinput
%% 
%% End of file 'taler.cls'.
