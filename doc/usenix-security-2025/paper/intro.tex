\section{Introduction}\label{intro}

This paper presents the design and implementation of a protocol for
donation handling that satisfies a broad range of potential technical
requirements and desiderata for donation systems.  The protocol
enables {\em donors} to make incognito donations to registered {\em
  charities} and still receive a tax benefit for donations to
charities recognized by {\em tax authorities}, while at the same time
preventing fraud (Figure~\ref{fig:stakeholders}).

\begin{figure}[ht]
\begin{center}
\begin{tikzpicture}
    \node (image) at (0,0) {\includegraphics[width=0.05\textwidth]{stickman}};
    \node at (0,-1.3) {Donor};
    %arrow
    \draw (1,0) -- (5,0) node [midway,above] {donation};
    %charity
    \node (image) at (5.5,0) {\includegraphics[width=0.075\textwidth]{charity}};
    \node at (5.5,-0.7) {Charity};
    %arrow
    \draw (5,-1) -- (3.5,-2.5) node [midway, below, rotate=45] { recognition };
    %server
    \node (image) at (3,-3) {
    \includegraphics[width=0.06\textwidth]{tax-authority}};
    \node at (3,-3.8) {Tax Authority};
    %arrow
    \draw (1,-1) -- (2.5,-2.5) node [midway, below, rotate=-45] { taxation };
\end{tikzpicture}
\end{center}
\caption{Stakeholders present in the Donau system.} \label{fig:stakeholders}
\end{figure}

Donating is an important way for people to empower causes they believe in and
facilitate collective action. In many countries there is explicit state
recognition of the wider public benefit of enabling such generosity: a friendly
tax treatment of donations. This makes financial sense as well: money immediately given
away to a recognized, independently administered good cause is not
income that will be used for private consumption. So, conceptually,
it deserves a different tax treatment.

Today, charities issue donation receipts which generally bear the
name of the charity.  The donor often has to include the donation
receipts in their tax declaration; this means the tax authority not
only learns the amount that the tax payer donated to charitable
organizations but also how much they gave to which.

%% JL: I strongly suggest completely removing this paragraph. I'm not sure we
%% need to be so abstractly idealistic, and even if we do, the citation is
%% ideologically dissonant with the text that precedes it. ALEC is a right-wing
%% pro-corporation model legislation factory whose goal is to eviscerate US
%% civil liberties, labor rights, environmental regulation, etc. It actively
%% fights against the civil rights mentioned just before it.

% Donations can serve many causes, but quite often they are an obvious expression
% of the human right towards the freedom of thought, conscience and religion.
% Unencumbered financial contributions to public benefit organisations as a means
% of collective action exhibits significant conceptual overlap with often
% strongly protected civil liberties like freedom of speech and freedom to
% assemble. The American Legislative Exchange Council, the largest voluntary
% membership organization of state legislators in the USA, adopted a model
% resolution in 2016 \cite{ALEC2016} in support of nonprofit donor privacy,
% stating that "nonprofit organizations are a primary mechanism by which groups
% of people assemble to practice free speech and express their opinions on
% political and nonpolitical subjects".

Individual spending quickly becomes very intimate and personal, as even
aggregate spending habits can reveal a great deal about people through
behavioral analytics and
psychographic profiling.~\cite{purchase2018wen,purchasepsyco2019gladstone}
This holds even more for
acts of donating, which is typically highly revealing about e.g. belief systems
and intersectionality of the individuals in question.~\cite{religiondonation2015deabreu}

Protecting donation confidentiality is therefore important to protect
those freedoms. We have to recognize that in some situations the mere
fact that someone has -- in private -- donated to some cause at some
point in their life, can later put them at risk in another context.
The right to privacy is thus a critical aspect of donating.

International human rights law also provides a non-ambiguous responsibility
to promote and protect the right to privacy:
Both freedom of thought and informational self-determination are
anchored in key international treaties and covenants such as the
Universal Declaration on Human Rights (Article 12)~\cite{udhr1948}, the European
Convention for the Protection of Human Rights and Fundamental Freedoms
(Article 8)\cite{ecphrff} and many more.


\subsection{Protection towards all sides}

Privacy protection against outside parties is not the only concern.
Threats to privacy may come from the recipient charity as well.
Even when a donor considers a
particular philanthropic cause to be worthwhile, the charity may use
information about the donor's past giving to aggressively solicit them for more
contributions afterward.
This happens in particular when such organizations
employ third party (often for-profit) agencies to help ``yield'' more donations
on a commission basis.  For-profit fund
raising agencies often engage in privacy-invasive practices to identify and contact potential donors.
One common scenario is that after a first donation, such agencies % calling them "bad actors" might be unnecessarily opinionated. From a privacy perspective they are not good, but many organizations also rely on these services to not go bankrupt. -JL
start to aggressively pressure a particular donor for more --- with personalized
emails, letters, phone calls and even in-person visits.
They also reuse donor information between charities,
leading to an avalanche of donation requests from organizations that the donor might not be interested in supporting.

In the era of data-driven donations and corporate social media
surveillance, this kind of behavior has unfortunately become so easy
that there are not just pro bono but even paid services (e.g.,
\href{https://www.donateursbelangen.nl/opzegservice}{Stichting
  Donateursbelangen} in the Netherlands) to de-register and exercise the ``right to be
forgotten'' after donating.

These concerns suggest that a privacy-preserving donation system
should also allow donors to remain anonymous with respect to the charity itself,
if that is their wish.

% Commenting out, too much repetition for my taste. -CG
%Even without such excesses, there are many circumstances when people
%like to donate something to their preferred causes without revealing
%their identity.  Some people just prefer to stay anonymous because of
%personal beliefs or even religious requirements, or simply do not want
%to have publicity which might lead to a cascade of efforts from fund
%raisers.

%\subsection{Donation confidentiality}

%Making a financial donation is a deeply personal choice to share part
%of one's wealth in order to benefit a cause one cares about. Some
%traditional ways of donating (for instance passing around baskets or
%even plates in a religious gathering) are vulnerable to group
%pressure, and door to door fundraising is also confrontational and
%puts people on the spot.
%
%Donations in their purest form should be devoid of such pressures and in cases
%where there is no need for, e.g., virtue signaling, donation confidentiality
%comes into play. Historically, people wanting to make an anonymous donation
%might have an envelope with cash or a box of goods delivered. Obviously, this
%was never compatible with providing tax benefits. Alternatively, they might
%arrange for an expensive intermediary like a notary (although that would not be
%fully anonymous and depends on the discretion of the notary).
%
%Technically guaranteed donation confidentiality is certainly
%non-trivial to implement in the digital payment era. What you donate to and why
%may be strictly personal, but along the financial pipeline there is an
%uncomfortable number of actors handling sensitive data that allows for
%profiling and targeted discrimination on grounds. And there are even more that
%later on may get access to it. Digital payments are logged and made accessible
%to many different actors, and reporting donations to tax authorities adds yet
%(at least) one more actor to the pipeline.


\subsection{Overview of the requirements analysis}

There are two types of donations. The first is {\em
  ad hoc} or {\em informal donations}, which are made from individual
to individual as {\em one time gifts} typically out of spontaneous compassion
or in appreciation of the
work being done by an individual or collective. The second category is
{\em regulated donations} involving at least one {\em recognized}
philanthropic organization or charity.  Both involve voluntary
transferal of some financial assets for which no products or services
are rendered in return.
% NOTE[oec]: what types of donations are _not_ considered, and why?
% TL for the first time I'd include ad-hoc donations to beggars or to some
% collection boxes; that doesn't fit well with the appreciateion but rather
% with pity or compassion

We focus on donations to charities
which would be eligible for claiming tax benefits as that scenario triggers the
most complex requirements.

As part of their regular operations as well as their recognition as
public benefit organizations, registered charities are already typically
subject to a variety of audits as well as strict regulatory and fiscal
scrutiny. Good causes that do not adhere to these rules are stripped from any
fiscal benefits.
From a regulatory point of view, it should be compliant to have donations to
recognized public benefit organizations
be confidential: donors should be able to freely choose whichever
of the approved philanthropies they donate to, without having to disclose which.

We note that in some countries there are different tiers of philanthropies.
Some countries like Italy and the Netherlands have for instance particular tax
facilities for cultural philanthropies, offering more attractive rates of tax
benefits than for regular philanthropies. Obviously, this needs to be taken
into account when designing a system, but does not take away the fundamental
premise that within those categories it is no concern of a government which
particular recognized causes are supported.

In this work we solve the issue of privacy-preserving donations with
tax deductions by adhering to ``privacy by design'': In cases where
perfect confidentiality is not (yet) feasible, we provide fallbacks
that best serve the interest of donors, give them choice and respect
their privacy as well as the current context allows.


\subsection{Digital Cash}

Digital cash~\cite{Chaum89} based on tokens issued using blind
signatures has previously been
suggested~\cite{donations2003blind} as a foundation for donation systems that allow
donors to remain anonymous but easily identify donation recipients.
The untraceability of the underlying payment system for purchases easily provides untraceability in the donation context as well.
The crucial difference is that the Donau system gives tax-deductible donation
receipts without revealing the charity donated to.

Our current implementation is designed to work in conjunction with the
GNU Taler~\cite{Taler} payment system.  GNU Taler is a {\em digital
  commons}, based on Free Software~\cite{stallman2009free} and advanced
cryptography. This means that -- unlike proprietary products -- anyone
can easily extend and customize the core system.

As the underlying acronym (``Taxable Anonymous Libre
Electronic Resources'') suggests, GNU Taler bridges two seemingly opposing
requirements: 1) providing privacy to citizens with regard to how they
spend their money in the digital realm, and 2) making these expenditures (on the receiving end) transparent to and auditable by appropriate financial institutions.
These high-level
objectives philosophically match nicely with our objective of
achieving privacy-preserving donations with tax-deductability.


\subsection{Approach}

At a high level, the Donau protocol consists of five steps:
\begin{enumerate}
\item Charities are recognized by the tax authority and their
  credentials are registered at the Donau service provider.
\item Tax payers are assigned tax payer identification numbers.
\item A donor makes a donation to a charity and receives
  blindly signed donation confirmation tokens bound to
  their tax payer identification number in return.
\item After the tax period ends, each donor submits their
  collected donation confirmation tokens to the Donau and
  receives a summary donation statement over the total
  amount bound to their tax payer identification number.
\item Donors submit the summary donation statement with
  their tax filing to the donation authority, which validates
  the digital signature from the Donau.
\end{enumerate}

The Donau protocol makes it possible for the donor to give an
unforgeable proof of the combined amount they donated to registered
charities, without the charities or the tax authorities learning who
donated to whom. The privacy features obviously require that there is
more than one charity and more than one donor. The Donau protocol
itself is actually oblivious to how the payment underlying the
donation happens. If the donor chooses to donate by credit card or
bank transfer, then their identity may become known to the charity
through the payment process.
%
However, a relevant feature of the protocol is that the charity does
not need to learn the identity of the donor. Hence, if payments are
made with GNU Taler or a similar privacy-preserving payment method, the
Donau protocol will preserve the privacy properties of that payment
system.

The design requires the creation of a Donation Authority (Donau), an
additional service separate from the charities and the payment system.
The Donau is responsible for recognizing charitable organizations and
tracking the total amount of donation receipts each charity is issuing
for the charitable contributions the charity is receiving.  It is
typically expected that each competent tax authority would operate
a Donau for the taxpayers in its domain.  We note that the Donau does
not receive sensitive private information about donors: privacy is
achieved using cryptography to unlink proofs of donations from the
actual donation process. Even the taxpayer identification number is
only ever disclosed with the final tax statement to the tax authority,
but not to the Donau service or the charity.


\subsection{Structure of the paper}

Section~\ref{requirements} provides some deeper analysis on the
various requirements that donation systems may need to satisfy.  There
are many aspects to donations and for the technical design and
implementation we chose to focus on a design that provides privacy for
donations.  Section~\ref{technical} provides technical details on the
core design of the Donau protocol, while ignoring some of the more
complex use-cases from Section~\ref{requirements}.
Section~\ref{implementation} gives an overview of our existing implementation of the Donau server.
Finally, Section~\ref{discussion} explains extensions of the core design that
could be used to address all of the main use-cases.  Many of these
extensions are simply a matter of proper integration and user
interface design, while a few presume the existence of a widely
available digital identity system (such as citizen ID cards, or the European
Commission's \href{https://ec.europa.eu/digital-building-blocks/sites/display/EUDIGITALIDENTITYWALLET/EU+Digital+Identity+Wallet+Home}{Digital Identity Wallet}) that provides a single
unlinkable pseudonym for each citizen per charity.

Navigating donation regulations involves adhering to a multitude of
directives on transparency, anti-money laundering, tax compliance, and
data protection while also meeting specific requirements in individual
countries. Compliance ensures trust in the philanthropic sector,
promoting ethical giving practices within a complex regulatory
landscape.  Cross-border donations are particularly challenging.
We review some of the legal and regulatory background in
Appendix~\ref{app-back}.
