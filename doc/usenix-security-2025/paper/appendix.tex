\section{Further information on donations}
\label{app-back}

\subsection{General background information}

This section contains general background information pertaining donations.

% FIXME: make this less EU-specific for USENIX???

\subsubsection{General Regulatory Framework}

European Union (EU) member states regulate donations through a blend
of EU-wide directives and country-specific laws. While there is no
uniform regulation that applies to all donations in Europe, certain EU
directives and principles affect donation practices, particularly
those related to transparency, anti-money laundering (AML), tax
compliance, and donor data protection.

\subsubsection{Transparency and Accountability}

Transparency in charitable donations is crucial to maintain public
trust and deter financial misuse. European countries typically require
organizations that receive donations to adhere to transparency
measures, including:

\begin{itemize}
\item {\bf Public Financial Reporting:} Most European countries
  mandate that charities, nonprofits, and similar organizations
  publish annual financial reports. These reports generally include
  detailed breakdowns of income sources, donation amounts, and
  expenditures.
\item {\bf Disclosures for Large Donations:} In some countries, large
  donations must be reported to regulatory authorities. This threshold
  and the specific requirements vary by country. For example, Germany
  requires registration for organizations receiving public donations,
  while the UK mandates certain reporting for donations above a
  particular threshold.
\item {\bf Third-Party Audit Requirements:} To verify the financial
  integrity of charitable organizations, many countries mandate
  independent audits for organizations surpassing specific revenue
  thresholds.
\end{itemize}

\subsubsection{Anti-Money Laundering (AML) and Counter-Terrorism Financing (CTF)}

Given the potential for abuse of charitable donations for money
laundering and financing illegal activities, EU-wide Anti-Money
Laundering Directives (such as the AMLD5) require organizations to
implement stringent controls.

\begin{itemize}
\item {\bf Know Your Donor (KYD):} Similar to the Know Your Customer
  (KYC) practices in the financial sector, some countries require
  organizations to verify the identity of donors making significant
  contributions. This requirement is typically tied to AML laws.
\item {\bf Transaction Monitoring and Reporting:} Charitable
  organizations must monitor donation transactions and report any
  suspicious activities to relevant national authorities.
\item {\bf Registration with Financial Intelligence Units (FIUs):}
  Nonprofits are encouraged, and sometimes required, to register with
  FIUs in certain EU countries to facilitate AML compliance.
\end{itemize}

\subsubsection{Taxation and Deductibility}

The tax treatment of donations varies across Europe, but many
countries provide tax incentives to encourage charitable
giving. Donations to qualifying nonprofit organizations are often
tax-deductible, either partially or fully, depending on local laws.

\begin{itemize}
\item {\bf Eligibility of Donors and Organizations:} Both the donor
  and the recipient organization usually need to meet specific
  criteria. For instance, only donations to accredited charities
  registered with national authorities are often eligible for tax
  relief.
\item {\bf Limits on Deductions:} Most countries place caps on
  deductible donations, typically as a percentage of the donor’s
  income. For example, France allows deductions up to 20\% of taxable
  income, whereas Germany permits deductions up to 20\% of annual
  income or corporate profits.
\item {\bf Cross-Border Donations and Tax Relief:} The EU's ``Stauffer
  doctrine'' principle requires member states to treat cross-border
  donations similarly to domestic donations if the recipient
  organization meets equivalent standards, which facilitates
  cross-border charitable giving across the EU.
\end{itemize}

\subsubsection{Data Protection and Privacy (GDPR)}

The General Data Protection Regulation (GDPR) is a significant EU law
that affects how personal data is collected, stored, and managed,
including for donations.

\begin{itemize}
\item {\bf Consent for Data Collection:} Donors must be informed of
  how their personal data will be used, and organizations must obtain
  explicit consent if data will be used for purposes beyond the
  donation transaction itself, such as marketing.
\item {\bf Data Minimization and Retention:} Organizations are
  expected to collect only the data necessary for processing the
  donation, retain it only as long as necessary, and ensure proper
  data deletion practices.
\item {\bf Right to Access and Erasure:} Donors have the right to
  request access to their personal data held by an organization and
  can request deletion or correction of their data under specific
  circumstances.
\end{itemize}

\subsubsection{Corporate Donations and Sponsorships}

Corporate donations are also regulated, particularly when related to
tax deductibility, disclosures, and compliance requirements.

\begin{itemize}
\item {\bf Transparency in Corporate Sponsorships:} European countries
  may require public disclosure of corporate donations or sponsorship
  arrangements, especially when public funds are involved. Many
  countries also enforce rules against donations that may appear to be
  intended for influencing legislation or government actions.
\item {\bf Limits on Corporate Donations:} Some countries impose caps
  on corporate donations eligible for tax relief to prevent excessive
  deductions and potential misuse.
\end{itemize}

\subsubsection{Cross-Border Giving and EU Philanthropy Initiatives}

The European Union encourages philanthropy across borders within
Europe, but the process is still complex due to varying national tax
and legal frameworks.

\begin{itemize}
\item {\bf European Foundation Statute and the European Philanthropy
  Manifesto:} These initiatives aim to harmonize cross-border
  philanthropy regulations. The proposed European Foundation Statute,
  for instance, would create a legal form of a foundation operating
  across the EU.
\item {\bf Transnational Requirements for Nonprofits:} Nonprofits must
  navigate both the tax and regulatory requirements of each country in
  which they operate or fundraise, including any special
  registrations, tax filings, or documentation for cross-border
  transactions.
\end{itemize}

\subsubsection{Ethical Standards and Codes of Conduct}

Some countries have established or encouraged adoption of ethical
standards or codes of conduct for fundraising activities. Examples
include:

\begin{itemize}
\item {\bf Code of Conduct for Fundraising:} Many countries have
  adopted codes of conduct, which may govern methods for soliciting
  donations, advertising practices, and donor interaction
  protocols. There are also private initiatives such as the Donor
  Pledge from the Dutch foundation Donateursbelangen (``Donor Interest
  Foundation'').
\item {\bf Charity Commissions and Regulatory Bodies:} Several
  European countries have independent regulatory bodies that oversee
  charitable organizations, such as the Charity Commission in the UK,
  to ensure compliance and ethical conduct in donations.
\end{itemize}

\subsection{Country-Specific Considerations}

While EU-wide directives provide a framework, each country has unique
laws.  Here are a few examples:

\begin{itemize}
\item {\bf Germany:} Nonprofit organizations must register with local
  authorities to receive tax exemptions, and donations exceeding
  10\,000 EUR must be reported.
\item {\bf France:} Nonprofits must adhere to the ``Loi de 1901'' and
  comply with annual reporting requirements to remain eligible for
  public donations.
\item {\bf Italy:} Nonprofits are eligible for tax incentives if they
  register as ONLUS (Organizzazione Non Lucrativa di Utilità Sociale)
  or a similar designation under Italian law.
\end{itemize}


\ifodd0
Some bits of thoughts

Article 56 TFEU guarantees free movement of services throughout the
EU.  In particular, this obliges each EU country to recognize the
charitable organizations that are registered in other countries, as
confirmed by the following decision of the Court of Justice of the
European Union:

\url{https://op.europa.eu/en/publication-detail/-/publication/d3892f27-39b1-4a26-98b3-451a7ffb101d/language-en}



\subsection{Yearly Donation Limit}

In some tax jurisdictions, the tax authority may set a limit on the
total amount of donations that a charity may receive in a given tax
year.
%XXX ~\cite{?}  A Donation Authority must enable tracking and enforcement of such a limit.

\fi
