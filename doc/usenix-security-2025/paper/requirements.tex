\section{Requirements Analysis}\label{requirements}

This section provides an overview of requirements to provide
donors with donation privacy and tax authorities with adequate proof
that a donation was indeed clean and made according to the rules for
donations in their region of operation.

Tax authorities are creative, and taxation is an ever evolving area of
complexity. We will therefore not claim to provide a definitive
overview, but to provide a good start for bootstrapping a donation
ecosystem in the full knowledge that this will need to be updated.

In particular, this section should not be misunderstood as an overview of
current legal requirements across the world on how taxation on donations work.
Taxation is predictably unpopular, despite its clear essential function in
modern society, and therefore a very political topic that makes both
fiscal legislation and the way it is interpreted subject to frequent change and
much variation. Just like taxation on labor and profits, property,
inheritance, investment or gambling income, and consumption of
products and services, there is no universal agreement on how or whether
donations should be treated with respect to taxation. Ad hoc
regulation as part of political shifts makes tax rules {\em context-specific}
and {\em temporal}. We are unaware of any attempt even by
large stakeholders at providing such an overview as an up-to-date public
resource, and the cost of creating and subsequently maintaining such an effort
is actually prohibitive due to the need to cover many different jurisdictions
with in-depth fiscal expertise in an ongoing manner.

The goal of this section is instead only to provide an overview of
generic requirements that {\em could} be applied to a donation flow in
order to comply with regulations.

One should note that, in many jurisdictions, the {\em receiving end} of
donations does not necessarily have or need the same protections as the
donating side has. This {\em asymmetry in treatment} makes common sense: money
that has been parted with is no longer present at the side of the donor, and so
future actions by the donor do not easily become problematic.
All the action is on the other end of the
donation pipeline, as at some point after money arrives it will become active.
'Follow the money' therefore makes a lot of sense: while donations should be
given without return consideration, there are of course many financial
transactions (such as gifts or donations from business or lobby groups to
political parties) that are not clean in this respect. This calls for
transparency and professional scrutiny on the charities receiving
donations. The fact that in this case we are dealing with
legal entities and not private individuals makes this much less of a problem.


\subsection{Assumptions}

The basic assumptions when defining requirements for a donation flow are as follows:

\begin{itemize}
\item A donor donates from their {\em own assets}, and is willing to
  go on record (by means of a self-declaration) as acting on their own
  accord. Violation of this principle would then constitute fraud at
  their end.
\item A tax authority wants to assert that a donation comes from the
  legitimate donor, and is not made by some third party on their
  behalf.
\item There is no inverse relationship between the donor and donee,
  where the donor stands to receive money back from the donee in some
  concrete (in)direct way as result of the donation.
\item Donors are willing and able to provide privacy-preserving
  attestation of some unique and non-falsifiable personal or
  organizational property (such as a tax identification number) {\em
    at the time of donation} in order to be able to add up multiple
  donations within a single tax reporting period and validate that
  these do not extend beyond a threshold set by the tax authority or
  other regulators.
\item The philanthropies or charities are subject to {\em regulatory
  oversight}, {\em proper governance} and {\em regular audits}, so
  that money laundering is not relevant.
\item It is acceptable for some third party to be involved, but only
  based on Free/Libre Open Source software (FLOSS) and on a zero
  knowledge basis.
%- philanthropies are able to provide valid digital signatures
\item All parties involved own and can operate digital devices so that
  they can store digital identifiers, cryptographic keys, and donation
  receipts or records.
\item Donors are expected to have a device that can hold a wallet for
  permanent storage of donation receipts.
\item Charities and tax authorities are willing and able to run basic
  infrastructure.
\end{itemize}

\subsection{Central Design Goals} \label{sec:designgoals}

The central design goals for the Donau protocol are the following:

\begin{itemize}
\item Accommodate a donor's wish to remain fully incognito, also
  towards the organization(s) donated to.
\item The donor should be able to claim the tax benefits they are
  entitled to without having to disclose any of the organization(s)
  they donated to, including not to the tax authority.
\item The donor may accumulate any number of smaller or larger
  donations towards different eligible organizations (ideally even
  cross-border, in the presence of suitable fiscal arrangements such
  as within the European Union).
\item Since donations are cumulative and often spontaneous, a donor
  should not have to decide upfront whether they will request tax
  benefits for their donations later on. Hence, all donations to
  suitable registered charities should result in a form of donation
  receipt.
\item At the same time, the wallet of a donor should offer plausible deniability
of any specific donations.
\end{itemize}

\subsection{Optional Features} \label{sec:optionalfeatures}

The following covers optional features permitting a donation system
to have a maximum fit with as many fiscal regimes as possible for
both informal and regulated donations, while at the same time serving
the interest of the donors in question in the best possible
manner. Specific realizations may weigh these differently based on
local regulations and capabilities, but most need to be be provided in
some form.

\ifodd0
\begin{itemize}
\item Provide fiscal statement
\item Proof of registration
\item Providing a configurable self-testimony from the donor that they comply with specific legislation or regulation related to donations
\item Cumulative donation counter from same donor to same cause
\item Providing a notarized affidavit asserting uniqueness
\item Unique ID for voting/Donor Advised Choices
\item Making a compound weighted donation
\item Cost transparency
\item Staged donation
\item Bandwidth donations
\item Codes of conduct
\item Restricted access mechanism
\item Donation matching with a reference
\item Anonymous donation matching by employer
\end{itemize}

\noindent
We will elaborate on each of these features below.
\fi

\subsubsection{Feature: Provide fiscal statement}

The ability to provide a fiscal statement from the receiving charity
linked to the donation is the starting point for most regulated
donations, in order to comply to current practices.  For example, with
a time-stamped and printable fiscal statement of the amount, digitally
signed by the charity, a donor can prove their donations in person to
a tax authority.

It should be possible to obtain this statement at the time of
donation, and ideally within a reasonable period afterwards -- in both
cases without having to expose any additional information to anyone
(such as an IP address which is typically visible when downloading a
document via the web).

There might be a need to include personal data/attributes in the
attestation (e.g. a name, password ID, etc). There is no need for the
charity itself to have any knowledge about such information, so it may
be included encrypted with a key accessible exclusively to the
donor/the tax authority/an auditor or other suitable independent third
party.

The information should be configurable, and it should be clear which
information is somehow independently validated.

\subsubsection{Feature: Proof of registration}

In some countries (e.g. Belgium) donors are required to register
themselves with the tax authority before making a donation. While we
believe that to be an anti-feature, it should be possible to include a
checksummed code provided by the tax authority or a charity that makes
sure that only registered donors can donate.

\subsubsection{Feature: Configurable pledge}

It may be necessary for the donor to testify (prior to the donation)
that they comply with some legislative or regulatory requirement, or
agree with a policy set by the charity in question.

As a generic requirement, this translates to a configurable pledge by
the donor (e.g. ``I am not an employee or grantee of the organization
I am donating to, and am acting on my own accord. I stand to make no
direct financial gains from making this donation'').

The potential for abuse of donations to regulated charities is very
limited.  Such a self-testimony will allow the default to be to treat
donations in a ``good faith'' manner rather than with a top-heavy and
restrictive one-size-fits-all method.

\subsubsection{Feature: Cumulative donation counter from same donor to same
cause}

One way to bypass restrictions in terms of allowed donation sizes
before possible ``Know Your Donor'' requirements kick in, is to split
up donations~\cite{welling1989smurfs}.
If limits per donor are in place it becomes necessary to
be able to assert that cumulative donations from a donor stay below a
set threshold, where the threshold might have a temporal aspect (e.g., per
year, per quarter, per two years).

\subsubsection{Feature: Notarized affidavit}

More generically---for instance when there is a minimum age for
donations to certain class of causes---a privacy-preserving solution
might be to have a notarized affidavit independently asserting the
requirements have been met to be included in the metadata of the
payment.

Such a privacy-preserving affidavit would not be traceable back to any
underlying private information of the donor or to the charity in
question.  It might contain a counter or append-only record, and a
date stamp with an accuracy no more precise than a calendar week (to
avoid correlation attacks).

It is better for this affidavit not to be provided by individual
charities but by trusted third parties otherwise ignorant of the
transactions in questions: it involves an isolated task which can
easily be outsourced to an independent service. That independent
service only needs to perform this singular task based on having
access to the proof/attribute(s) in question and does not need to have
any further knowledge of any of the actors. The latter assumes that
any unique identifier in the affidavit is uniquely linked to the donor
so that they cannot circumvent limits by going via different third
parties.

As long as the affidavit is non-falsifiable and irrevocable, it should
suffice to assert uniqueness and allow to prove that the required
conditions were met.

\subsubsection{Feature: Unique ID for donor advised decisions}

Also from the side of a donor, there might be a need for having a
unique ID for voting. In the same vein as
Donor Advised Funds~\cite{berman2015donor}, a
crowd-sourced version could be Donor Advised Choices where donors can
vote on specific options (``Shall we prioritize stretch goal A or B'',
or ``We see a new opportunity, is it okay to replace some stated work
with something else'') -- either on a weighted variant (larger
donation gives more weight) or on a one person, one vote (all unique
donors get the same one vote each).

Alternatively, a preference vote encoded inside the payment (based on
e.g. Condorcet voting) could provide a one-time donor advised voting
mechanism.

\subsubsection{Feature: Compound weighted donation}

The general idea is that donors can make a single donation, but it
consists of multiple payments to multiple recipients. This is
particularly relevant for informal donations to the developers of free
and open source projects that do not make use of a fiscal host. In
such a situation, the donations may be divided across the individual
developers with a certain weight. Each of the recipients receives a
direct donation from the donor, which typically will be far below the
threshold for taxation.

There can be a suggested/default weight, but the donor should be able
to tweak the relative weights and/or block specific recipients.

\subsubsection{Feature: Cost transparency}

It should be transparent to the donor what percentage of their
donation is actually used for the effort for which funds are being
raised. In particular it should be possible for the {\em cost for
  fundraising} to be made explicit, especially if this involves third
parties. It should be possible to choose to donate without paying for
fundraising.

(This might use the features from compound weighted donation.)

\subsubsection{Feature: Staged donation}

This is a feature that works along the lines of so-called smart
contracts. As goals are incrementally met by the project, donated
funds are released. If the goals are not met according to the preset
stages, the part of the money that is concerned with work that is not
delivered is not paid and may ultimately be restored to its rightful
owner, the donor.

\subsubsection{Feature: Bandwidth donations}

When people are pooling together resources to make some goal possible,
in order to stimulate the broadest possible donations, the amount
donated can be made flexible (within a certain {\em donation
  bandwidth}). Instead of stretching goals (which donors might not
agree with) and promoting freeloading, the size of individual
donations could shrink as well. This would stimulate to share the
collective load.

\subsubsection{Feature: Code of conduct}

Donors transfer part of their (sometimes scarce) earthly possessions
to support the good work of a cause they believe in, and it is only
logical that this altruism comes with certain expectations in terms of
how the organization receiving that money will subsequently spend it.

A {\em Code of Conduct} is the equivalent of the product warranty,
where charities declare themselves accountable and promise to uphold
certain best practices and adhere to public scrutiny -- and are
subsequently held to their promise by stakeholder organizations like
Donateursbelangen.

An example of such a Code of Conduct public benefit organizations can
subscribe to is the
\href{https://www.donateursbelangen.nl/de-donateursbelofte}{Donor
  Pledge} (``Donateursbelofte'' in Dutch). It should be possible for a
charity to adhere to multiple such Code of Conducts and offer them as
part of their donation portal.

Similarly, there are certification schemes for charities qualifying as
public benefit organizations. These offer a reverse link from the
certifying organization to the charity. It should be possible to
include the certification conditions and this reverse link alongside
the payment.

\subsubsection{Feature: Restricted access mechanism}

In order to engage donors with the work being done, philanthropies
might want to give ``behind the scenes'' access to ongoing work to
their donors. In order for that to happen, it should be possible to
provide (limited) access to restricted materials for donors only. On a
technical level, this could be handing out {\em One Time Passwords} or
other forms of proof of donation that will allow donors to get access
to restricted areas.

\subsubsection{Feature: Unlock thank you artwork}

Making a donation is not just a clinical financial transaction where
money is transferred from A to B, but something that also has
emotional weight: the donor has taken a step they may have pondered
about for a long time. Celebrating this altruistic win is part of the
donation experience. ``Thank you'' artwork consists of images, video
and/or audio used to enliven the financial transaction.

In some cases artists or other creatives might donate a work to the
charity in question for this purpose, in other cases a charity might
use photos of their day to day work or other personal tokens.

For transferring physical objects, the donor would need to be
identifiable as such. At the same time, it should be possible for a
donor to decline receiving such gifts and retain at least anonymity, to
the extent that this does not conflict with other regulations.

\subsubsection{Feature: Donation matching with a reference}

In some cases, a benefactor will want to incentivize others
contemplating a donation to a specific good cause to go ahead. That is
not necessarily something that needs privacy: some people and
organizations use donations to publicly profile themselves. A common
mechanism to incentivize others is to promise to match their donations
to the organization in question, which is frequently done by
announcing a period in which other people's donations will be
``matched'' (as in: donor A promises to donate as much as all other
donations in that time period combined).

However, this is obviously a very crude mechanism, only suitable for
benefactors with very deep pockets. It also does not give much
opportunity for the benefactor to explain why they do this (and, let
us be realistic, get some PR out of it as well).

By allowing the donor to include a reference to e.g. a social media
post or blog post announcing the matching and requesting other donors
to include that reference when making their donations, the donor
providing the matching can `see' that they are being heard/are getting
PR mileage out of their donation.

Conversely, while one would like to be able to trust each and every claim on a
website or social media account towards matching of donations, donation
matching is a form of social engineering that is potentially easy and
attractive to tamper with. A critical donor may prefer to have actual proof of
such altruistic matching irrevocably taking place, in order to weed out any
attempt to trick them into a false sense of urgency - believing their donation
will temporarily have a disproportionately larger effect. As such, it would be
interesting to be able to verify whether the matching donation actually took
place.

\subsubsection{Feature: Incognito donation matching by employer }

Quite a few large employers do donation matching as part of their
corporate responsibility or human resource management (HRM)
efforts. This is typically not tied to a single cause.  Many larger
employers sponsor such matching gift programs, either by themselves
(such as the U.S. Office of Personnel Management's
\href{https://givecfc.org}{Give CFC}) or via (currently expensive)
third party organizations such as Benevity, Submittable, WeSpire,
Goodera, etc.

In many cases, this practice is rather privacy-invasive. If you donate
to, e.g., a reproductive rights organization, an NGO promoting climate
justice, or a digital rights organization, an employer might want to
find out from whom that donation originated. This makes it attractive
for the donor to have a chance to stay incognito while nevertheless
ensuring that their donation is matched as one done by an employee of
the company.  This would require a mechanism where charities could
prove to an employer that some eligible person (typically an employee
or retiree) has donated money which needs to be matched -- obviously,
without disclosing anything else.
