\section{Implementation}\label{implementation}

This section describes the current implementation of the Donau 
\ifanonymous
server,
\else
server (available from \url{https://git.taler.net/donau.git}),
\fi
which
primarily consists of an HTTP server with a REST API
\ifanonymous
\else
(\url{https://docs.taler.net/core/api-donau.html})
\fi
and a Postgresql
database (Figure~\ref{fig:donau_system_arch}).  The Donau backend is
written in C, as it reuses parts of the codebase from GNU Taler
exchange component.  The Donau has a similar architecture and uses
cryptographic blinded signatures in a similar way as the GNU Taler
exchange does, including encapsulating its private keys in separate
``security module'' helper processes.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{donau_system_arch}
  \end{center}
  \caption{Donau server architecture} \label{fig:donau_system_arch}
\end{figure}

\subsection{REST API Overview} \label{rest_api}

The main endpoints of the REST API are briefly summarized
in the following sections.
\ifanonymous
For examples see Appendix~\ref{app-impl}.
\fi

\subsubsection{\texttt{/keys}}

The \texttt{GET /keys} request returns all valid donation unit public keys
offered by the Donau, as well as the Donau's current EdDSA public signing key.

\subsubsection{\texttt{/charities}}

In order for a charity to be able to issue receipts by a specific
Donau it must be registered by this Donau.  The Donau provides an API
to manage charities.  By default only the Donau administrator can
change the list of registered charities.  The charity itself is able
to request a donation report to keep track of their total donations in
the current year.  The response includes the maximum donation amount
and the current donated amount for the charity of the current year.

\begin{figure*}[h]
\includegraphics[width=0.7\textwidth]{donau_flow_issue_receipt}
\caption{Flow of the issue receipt process, incl. an optional check on donation limits
per charity} \label{fig:donau_flow_issue_receipt} % XXX: this diagram mentions the "check donation limit" function which was removed from the text. -JL
\end{figure*}

\subsubsection{\texttt{/batch-issue}}

Only recognized charities are allowed to issue receipts for their donors.
A \texttt{POST} issue receipt request includes an array of \texttt{BKP}s. A
\texttt{BKP} consists of a \texttt{BUDI} and a hash of a public donation unit
key (see section~\ref{notation_and_definitions}).
The charity signs the request with its own EdDSA private key. The
corresponding public key was given to the Donau in the registration process of
the charity.
After the Donau checks the signature from the charity it signs the
\texttt{BUDI}s with the corresponding donation unit private key. Before the
signatures are returned to the charity the Donau saves a hash of the request
and all donation unit signatures to detect replays (see section~\ref{donau_database}).

\subsubsection{\texttt{/batch-submit}}

The batch-submit route is used by the donor to summarize their donation
receipts into one donation statement, which then gets signed by the Donau with
their EdDSA signature.
The request is composed of the donation receipts (see section~\ref{donau_creates_donation_receipt}),
the corresponding year and the Donor Identification \DI, which is the salted hash of the donor's taxpayer ID.
When processing the request, the Donau checks the validity of the donation
receipts and searches its database for other saved donation receipts made in the requested
year.
The Donau computes
a donation statement, consisting of a signature over
the total value of the donation units of all donation receipts of the year,
the salted hash of the taxpayer ID, and the current year,
and stores this in its database along with the submitted receipts (see section~\ref{donau_database}).

In our implementation the Donau does not return this donation statement under
this call but under the {\tt /donation-statement} endpoint, which returns
a signature over the cummulative amount under the same \DI.

\subsection{Donau database}\label{donau_database}

The Donau database contains five tables.  The \texttt{donation\_units}
and \texttt{donau\_sign\_keys} table store the keys necessary for
signing and creating donation receipts. Donation receipts that are
issued to be signed by the Donau are stored in the
\texttt{receipts\_issued} table while the receipts that are already
signed are stored in the \texttt{receipts\_submitted} table.  The
\texttt{history} table keeps the donation records of the past years.

\subsection{Verification URL syntax}

The verification process used by the tax authority to check the
donation statement (see
section~\ref{donor_sends_final_statement_to_a_validator}).  A
verification app is expected to scan a QR code that encodes the
donation statement, verifies the signature and displays the signed
values of the donation statement.  The verification URL
encodes all binary values in base64 and has the following syntax:

\begin{verbatim}
   donau://DOMAIN/YEAR/TAXID:SALT/AMOUNT/EDSIG
\end{verbatim}


\subsection{Other components}

For the charities, support for donations is being integrated into
the GNU Taler merchant backend. Similarly, GNU Taler wallets
will store donation receipts and export donation statements.
