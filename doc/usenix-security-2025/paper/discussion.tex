\section{Discussion of features} \label{discussion}

In this section we show how the presented design relates to the
various requirements discussed in Section~\ref{sec:optionalfeatures}
and what extensions could be made to address almost all of them.

\subsection{Feature: Provide fiscal statement}

Both the individual donation receipts and the annual donation statement
provided by Donau satisfy the requirement of providing a fiscal statement.
In general, the annual donation statement is more user-friendly and more
privacy-friendly as it only contains the total amount and is more compact.

\subsection{Feature: Proof of registration}\label{discussion:registration}

In the Donau protocol, the donor is identified using a unique donor
identifier. The format of the identifier is not fixed by the protocol,
and could easily be replaced by the (hash over) donor registration
data. While in this case the charity and Donau cannot check the validity of the
donor's registration due to blinding, the tax authority would be
assured that only donors who properly registered prior to their donation get a
donation receipt that is valid for them. This enforces registration for tax
deductable donations.

If the purpose of the registration is to limit charities to only accept money
from registered donors additional components need to be added. The
cryptographic concept of attribute-based credentials~\cite{abc}
can be used to build a suitable functionality.
When a donor registers, they are provided a credential to prove that they are
registered. When donating to a
charity, the donor uses their credential to sign their donation. The charity checks
that the signature is valid
and is thus obtains proof that they are interacting with a registered donor as
nobody else could make a valid signature. There are different approaches to
attribute-based credentials and features differ between them. To keep the
privacy guarantees of the Donau protocol and only add donor registration, the
system should be unlinkable and anonymous.

We acknowledge that this design does not provide a link between the
donor identity $\DI$ used in the BKP and the donor registration as
that is incompatible with the property of providing anonymity to the
donor. A registered donor with a valid credential may choose to submit
BKPs that include invalid $\DI$s or somebody else's $\DI$. However,
the design gives a proof to the charity that the payment they receive
comes from a registered donor. (See Section~\ref{sec:threats} for
a discussion of threats.)

Given that only few countries require donor registration and we
consider this a requirement that hampers charities, we chose not to include
this feature in our core design.



\subsection{Feature: Configurable pledge}

The Donau protocol is expected to be integrated with a payment process,
such as the GNU Taler payment protocol. Here, the actual payment would
sign over contract terms between the donor and the charity. The pledge
can be easily integrated into these contract terms.

\subsection{Feature: Cumulative donation counter from same donor to same
cause}\label{discussion:limit}

Limiting donations per donor is difficult when donations are supposed
to be anonymous and adding this feature to the Donau protocol requires
additional components.

Donors could be issued some credentials, to be used in an attribute-based
credential scheme which is anonymous but linkable. This could be set up
to have one credential per charity and requiring a signature under a valid
credential for each donation. This would mean that donations to the same
charity by the same donor are linked, and the charity would be required to check
the signature and to keep all donations and signatures, in order to decline further
donation attempts by donors who have reached their limit.

As a practical instantiation it is conceivable to combine the Donau
protocol with an eID solution that provides some unlinkable
pseudonym derived from the donor's main identity~\cite{hasini2016unlinkableeid}.
The unlinkable pseudonym could be unique to the donor,
charity and time period.
By signing the donation process with such an unlinkable pseudonym
it is possible to prevent smurfing donations~\cite{welling1989smurfs},
alas at the expense
of reducing anonymity to pseudonymity.

As discussed above in Section~\ref{discussion:registration}, the signature on
the donation does not guarantee that the $\DI$ hidden in the BKPs matches that
of the signer.

\subsection{Feature: Notarized affidavit}

GNU Taler already supports privacy-preserving age-restrictions on
payments~\cite{esorics2022age}, thus it would be trivial to prove that the donor is of
legal age as part of the payment process (without disclosing any
further information). In general, including other attestations may
be possible, but is likely to be highly problematic from a privacy
point of view. Not to mention that birthdates are commonly recorded
and thus age can be easily attested by the payment service provider,
other types of attestations would require building the corresponding
certification infrastructure.


\subsection{Feature: Unique ID for donor advised decisions}

One way to enable donors to advise decisions without being linked to
the specific dontation would be for the donation process to return
blindly signed e-voting tokens (in addition to the donation receipt)
to enable the donor to vote. This would be similar to the discount and
subscription tokens already proposed for GNU Taler.

Alternatively, if advise should be linked to a specific donation, one
could build on an inherent feature of the GNU Taler protocol which is
that the donor signs with private keys of digital coins to pay for a
donation.  Creating additional signatures the same private coin keys
could also be used to advise decisions while providing a clear link to
a specific donation.

Limiting decisions to one per donor instead of proportional to the
amount donated is more complex, as it would require a solution
similarly to enforcing cumulative limits per donor as discussed in
Section~\ref{discussion:limit}.

A shared practical challenge independent of the cryptographic solution
is to find a good way to inform anonymous donors when their input is
solicited. In theory, anonymous remailers~\cite{mixminion} could be
used for this purpose.

\subsection{Feature: Compound weighted donation}

In general, the simplest way to do a compound weighted donation
would be to break up the donation into multiple donations at
the time when the donation is made. Given GNU Taler's ability
to handle micropayments, doing multiple smaller payments is
generally not an issue. Alternatively, the payment system could
be enhanced with escrow functionalities that would allow the
donation to be split up according to a given set of weights
which is only provided after the donation was made.

\subsection{Feature: Cost transparency}

Fees in the GNU Taler system are given as part of the protocol
and always shown to the payer, ensuring cost transparency. The
design does not require the use of additional parties beyond
the payment system, donor and charity.  If fundraising parties
are involved as well, their cuts could be easily made transparent
in the GNU Taler contract terms used in the payment process.

\subsection{Feature: Staged donation}\label{discussion:staged}

Staged donations would require the payment service to hold funds in
escrow until certain conditions are met (or refund them).  GNU Taler
can already do refunds (without infringing on the privacy of the
donor~\cite{taler2019dold}), and {\em escrow of funds} is a feature
planned for the future. Escrow means that the payment service provider
is responsible for releasing the funds to the charity only when
contractually specified conditions are met (likely attested by some
trusted third party), and otherwise the funds are refunded to the
donor.

As with other donations, the donor would create blinded donation
receipts upon payment, matching the amounts to the various stages.
However, the blinded donation receipts would only be requested from
the Donau once the funds are released to the charity from escrow.
Instead of using an anonymous remailer, the nature of a staged
donation allows the wallet software to be informed at what times it
should periodically contact the charity server to request a status
update on the project. Here, the wallet can easily identify itself as
the donor using the private coin keys.  Depending on the project's
progress the charity would then request blinded donation receipts
from the Donau and return those to the wallet, or the wallet would
receive a refund from the payment service provider.

%
%A problem, however, is that the donor is anonymous and thus cannot be reached
%at the time that later goals are met.
%
%A solution is for the charity to further blind or encrypt the Donau signatures
%and to publicly post the keys when the next stage of funding is reached and the
%donation becomes effective. In the following we assume that the donation is
%split up by funding stage and that the donor submitted an array of BKPs per
%funding stage, so
%$\vec{\mu_j} = (\bar{\mu}_{j1}, \bar{\mu}_{j2}, \ldots)$ is the array of BKPs for
%funding stage $j$.
%To make the multitude of keys manageable, the charity uses a random string
%$t_j$ per funding stage and {\em encrypts} the Donau response
%$(\beta_{j1}, \beta_{j2}, \ldots)$ for the stage $j$ payments
%with $H(t_j, \vec{\mu_j})$. The encrypted Donau responses (one per stage) are
%returned to the donor at the time of donation instead of the plaintext Donau
%response.
%
%When work progresses to reach
%stage $j$ and the donation payments are collected, the charity posts $t_j$
%publicly. Every donor can then compute the key $H(t_j, \vec{\mu_j})$ and decrypt
%their donation receipts for stage $j$. This requires the donor to store the BKPs along with
%the encrypted donation receipts until stage $j$ is reached and $t_j$ is
%released. It requires the charity to have a bulletin board for posting $t_j$
%but charities soliciting staged donations typically post extensive progress
%reports justifying them declaring success on the previous stage and this report
%should then link to the key $t_j$.

\subsection{Feature: Bandwidth donations}

To support bandwidth donations, the donated amount may shrink based on
independent donations to the same charity by other parties and some
allocation rule.  This can also be achieved via the design discussed
in Section~\ref{discussion:staged}. As before, during the period that
the charity is soliciting funding, all donations would be held in
escrow.

Once the funding period ends, all pledges are publicly posted (say by
the charity) allowing the wallets to (1) confirm that their pledge was
properly included, (2) compute the total amount pledged, (3) compute
their share based on some previously agreed allocation rule, and (4)
to finally collect the resulting amount of donation receipts and
refunds.

A minor limitation here is that the donor should probably be required
to create blinded donation receipts at the time of the donation to
avoid enabling them to change the taxpayer ID at a later time. This
means that the granularity of the final allocations is fixed when the
donations are made, slightly limiting the flexibility of the
allocation rule algorithm.

\subsection{Feature: Code of conduct}

A code of conduct could easily be integrated into the contract
terms for of the payment process.

\subsection{Feature: Restricted access mechanism}

The envisioned discount token and subscription extensions of the
GNU Taler protocol could be used to return to the donor a token that
would grant them access to additional information.

\subsection{Feature: Unlock thank you artwork}

The GNU Taler protocol can already be used to buy digital goods,
including newspaper articles, videos, images or audio resources. Thus,
this can easily be done by using the payment process to authorize
bypassing what in a commercial setting would be called a paywall.

Sending physical gifts requires having an address of the donor and thus is not
compatible with the anonymity feature. However, there is nothing in the design
that stops the charity and donor from exchanging address information during the
donation process.

\subsection{Feature: Donation matching with a reference}\label{sec:matching}

Donation matching is an agreement between the charity and a matching donor.
The charity can show proof of the payments received; if these are done using
GNU Taler then the charity can show the deposit confirmations issued
by the GNU Taler exchange to the charity. This way, the
charity can prove to the match funder that they received a
certain amount of donations, and they can even include the
contract terms to show that the donors intended to participate
in the match funding project.

Similarly, a match funder could escrow their donation at the payment
service provider, only to be released to the charity once matching
donations have been made. The deposit confirmation on the escrowed
donation can then be used to convince potential donors that the match
funding is real.

Both donor and match funder can in principle share their final
donation receipts publicly to advertise their good deed, but they then
of course void their anonymity.

\subsection{Feature: Anonymous donation matching by employer}

Donation matching by employer could be achieved by combining approaches
similar to what was discussed in Section~\ref{discussion:registration}
and Section~\ref{sec:matching}.

If the match funder is a company with a taxpayer ID known to their
employees and the employer knows the donors' taxpayer IDs (as is
common in an employment scenario) there is a simpler approach.  This
approach exploits the loophole that donations in the Donau system do
not prove that the donation is actually made by the taxpayer with the
{\tt TAXID}.

Thus, it is possible for the employee to simply donate the full amount
(their contribution as well as the match funding) to the charities of
their choice, but to include their own $\DI$ in half of the amount and
one derived from the {\tt TAXID} of their employer in the other
half. Then they show the donation receipts for both halves to the
match funder. The match funder can verify validity of both receipts
and that the proportion of their match is correct. They then pay back
the amount that was donated on their behalf to the employee and use
the donation receipt for their {\tt TAXID} when filing their tax
statement.
