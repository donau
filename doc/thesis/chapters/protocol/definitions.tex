This chapter describes the Donau protocol. In the first section notations and
definitions are established which are then used in the later section where
the protocol details are described.
To fully comprehend the cryptographic concepts discussed in this chapter, the
reader may need prior knowledge or background in this field.

\section{Notation \& Definitions}\label{notation_and_definitions}
\subsection{Notation}\label{notation}
The following are notations used in the following pages of this chapter.
\begin{itemize}
 \item $\langle a, b, ... \rangle$ is used to represent a Pair or tuple
\end{itemize}

\subsection{Definitions}\label{definitions}
\begin{itemize}
  \item \textbf{Cryptographic Hash Function}
    \begin{displaymath}
      h := H(m)
    \end{displaymath}
    where $m$ is a message and $h$ the resulting hash.

  \item \textbf{Blinding Function}
    \begin{displaymath}
      \overline{u} := blind(u, b, K_x^{pub})
    \end{displaymath}
    where $u$ is the value to blind, $b$ the blinding factor to apply and $K_x^{pub}$ the public key of the Donation Unit that will be used for signing.

    The blinding can be done with either the \textbf{RSA} blind signature scheme or the Blinded \textbf{Clause-Schnorr} signature scheme.

  \item \textbf{Unblinding Function}
    \begin{displaymath}
      \beta := unblind(\overline{\beta}, b, K_x^{pub})
    \end{displaymath}
    where $\overline{\beta}$ is the value to unblind, $b$ the blinding factor to apply and $K_x^{pub}$ the public key of the Donation Unit that was used for signing.

    The unblinding must be carried out using the \textbf{same} signature scheme that has already been used for the blinding.

  \item \textbf{Donation Unit Key generation}
    \begin{displaymath}
      \langle K_x^{pub}, K_x^{priv} \rangle := Keygen^B(\omega)
    \end{displaymath}
    where $\omega$ is a source of entropy. The resulting key pair represents a \textbf{Donation Unit}. The result is a public key $K_x^{pub}$ and private key $K_x^{priv}$. The equivalent used in Taler system is a \texttt{Denomination}.

  \item \textbf{Donau Key generation}
    \begin{displaymath}
      \langle D^{pub}, D^{priv} \rangle := Keygen^D(\omega)
    \end{displaymath}
    where $D^{pub}$ and $D^{priv}$ are the respective public and private Donau keys.

  \item \textbf{Charity Key generation}
    \begin{displaymath}
      \langle C^{pub}, C^{priv} \rangle := Keygen^C(\omega)
    \end{displaymath}
    where $C^{pub}$ and $C^{priv}$ are the respective public and private Charity keys.

  \item \textbf{Donation Unit (DU)}
    \begin{displaymath}
      \langle K_x^{pub}, K_x^{priv} \rangle
    \end{displaymath}
    A Donation Unit consists of a public and private key where $x$ is the associated value (e.g. 2 EUR).

  \item \textbf{Donor Identifier (DI)}
    \begin{displaymath}
      i := H(\texttt{TAXID}, S)
    \end{displaymath}
    where $S$ is a random salt with sufficient entropy to prevent guessing attacks to invert the hash function.

  \item \textbf{Unique Donor Identifier (UDI)}
    \begin{displaymath}
      u := \langle i, N \rangle
    \end{displaymath}
    where $N$ is a high-entropy nonce to make the resulting hash \textbf{unique} per donation.

  \item \textbf{Blinded Unique Donor Identifier (BUDI)}
    \begin{displaymath}
      \overline{u} := blind( u, b, K_x^{pub} )
    \end{displaymath}
    A \textbf{BUDI} is the result of blinding a Unique Donor Identifier $u$ where $b$ is the blinding factor and $K_x^{pub}$ the associated Key. The blinding is done to protect the privacy of the donor.

  \item \textbf{Blinded Unique Donor Identifier Key Pair (BKP)}
    \begin{displaymath}
      p := \langle \overline{u}, H(K_x^{pub}) \rangle
    \end{displaymath}
    A \textbf{Blinded Unique Donor Identifier Key Pair} is the result of adding the corresponding hash of the \textbf{Donation Unit} public key to the \textbf{Blinded Unique Donor Identifier} $\overline{u}$ where $H(K_x^{pub})$ is the hash of the \textbf{Donation Unit} public key.

  \item \textbf{Signing}
    \begin{itemize}
      \item \textbf{Normal signing (e.g. EdDSA):}
      \begin{align}
        \fbox{$s := sign(m,k^{priv})$}
      \end{align}
      where $m$ is a message and $k^{priv}$ is the private key used to sign the message, for example the Donau private key $D^{priv}$ or the Charity private key $C^{priv}$.\\

      Applications:
      \begin{itemize}
      \item Signatures over a \textbf{Blinded Unique Donor Identifier Key Pair}:
        \begin{align}
          \fbox{$\vec{\mu}_s := sign(\vec{p},C^{priv})$}
        \end{align}
          where $H(K_x^{pub})$ indicates which \textbf{Donation Unit} key should be used by the Donau to sign the resulting \textbf{Donation Receipt}. Thus, this hash carries the information about the exact value, the final Donation Receipt should carry.

        A charity signs a collection of \textbf{Blinded Unique Donor Identifier Key Pairs} before transfering them to the Donau to issue the \textbf{Donation Receipts}

      \item Generation of the \textbf{Donation Statement}
      \end{itemize}

      \item \textbf{Blind signing(e.g. RSA/CS):}
      \begin{align}
        \fbox{$\overline{\beta} := blind\_sign(\overline{u},K_x^{priv})$}
      \end{align}
      where $\overline{u}$ is a blinded value and $K_x^{priv}$ is the private key used to blind sign the message.\\

      Application:
      \begin{itemize}
        \item The Donau blind signs \textbf{Blinded Unique Donor Identifiers} received from the Charity with the private key matching the public key in the received \textbf{Blinded Unique Donor Identifier Key Pair}
      \end{itemize}
    \end{itemize}

  \item \textbf{Verify Functions}

  To verify the signatures $m$ corresponds to the message and $s$ to the signature:

  \begin{itemize}
    \item \textbf{normal verify}
    \begin{displaymath}
      verify(m,s, P^{pub})
    \end{displaymath}
    where $P^{pub}$ can be the Donau public key $D^{pub}$ or Charity public key $C^{pub}$.

    \item \textbf{blind verify}
      \begin{displaymath}
        verify\_blind(m,s,K_x^{pub})
      \end{displaymath}
      verify a signature that was made blind and made with a Donation Unit private key $K_x^{priv}$.
  \end{itemize}

  \item \textbf{Donation Receipt}
    \begin{displaymath}
      r := \langle u, \beta, H(K_x^{pub}) \rangle
    \end{displaymath}
    where $\beta$ is the unblinded signature sent to the Donau to get the \textbf{Donation Statement}.

  \item \textbf{Donation Statement Signature}
    \begin{displaymath}
      \sigma := sign(\langle i, \Sigma{\vec{r}}, \texttt{Year}\rangle, D^{priv})
    \end{displaymath}
    The \textbf{Donation Statement Signature} is the signature over the sum (amount donated) of all the \textbf{Donation Receitps} $\Sigma{\vec{r}}$, that a donor has received from donating throughout the year where $i$ is the \textbf{Donor Identifier}. The \textbf{Donation Statement} itself includes all sign values and the signature itself.

    These \textbf{Donation Statement Signatures} attest the amount donated in a particular year by a specific donor.

\end{itemize}
