\section{Protocol Details}\label{protocol_details}

\subsection{Key generation and initial setup}\label{key_generation_and_initial_setup}

\subsubsection{Donau key generation}\label{donau_key_generation}
\begin{enumerate}
  \item The Donau generates a Donau public key $D^{pub}$ and private key $D^{priv}$ for EdDSA signing.
  \item The Donau generates the \textbf{Donation Units} consisting of a public key $K_x^{pub}$ and private key $K_x^{priv}$ where $x$ is the associated value.
\end{enumerate}

\subsubsection{Charity key generation}\label{charity_key_generation}
\begin{enumerate}
  \item The Charity generates a charity public key $(C^{pub}$ and private key $C^{priv})$ and fetches the \textbf{Donation Unit} public keys from the Donau.
  \item The Charity transmits its public key $C^{pub}$ and the requested yearly donation limit to the party controlling the Donau (e.g the local tax authority) using a \textbf{secure channel}.
  \item The party in charge of Donau administration ensures that the applying charity is authentic and publicly recognized as a charitable organisation. Furthermore, it ensures that all eventual restrictions by law are followed. After the verification was successful the Charity public key $C^{pub}$ together with its requested yearly donation limit are registered in the Donau database.
\end{enumerate}

\subsection{Donating to a charity}\label{donating_to_a_charity}
% \subsubsection{Donor donates to charity and transmits \textbf{Unique Donor identifiers} (future donation receipts)}
In order to make a donation the donor has to first download the \textbf{Donation Unit} public keys $K_x^{pub}$ from the Donau for the current year.
After that the donor generates his \textbf{Donor Identifier} which is a salted hash of his tax number.
As each \textbf{Donation Unit} holds a specific value the donor has to split the donation amount into \textbf{Donation Units} offered by the Donau.

Donor Identifier $i$:
\begin{align*}
  i := H(\texttt{TAXID, S})
\end{align*}

\emph{Example: With \textbf{Donation units} $\{1,2,4\}$ beeing available, and a donation of $7$, the donation amount is split into the valus $4$, $2$ and $1$.}

For every \textbf{Donation Unit} the donor generates a \textbf{Unique Donor Identifier} by adding a nonce to his \textbf{Donor Identifier} $i$.
If a \textbf{Donation Unit} of the same value has to be present more than once to represent the target sum, multiple \textbf{Unique Donor Identifiers} of same \textbf{Donation Unit} has to be generated.

\emph{In our example, there are $3$ \textbf{Unique Donor Identifiers} needed to represent the value of $7$}.

Unique Donor Identifiers $u_1, u_2, u_3$:
\begin{align*}
  u_1 :&= \langle i, \texttt{N}_1 \rangle \\
  u_2 :&= \langle i, \texttt{N}_2 \rangle \\
  u_3 :&= \langle i, \texttt{N}_3 \rangle
\end{align*}
where $S$ is the salt and $N$ a Nonce.

In a next step the donor needs to blind the \textbf{Unique Donor Identifiers} using a \emph{different} blinding factor $b$ for every \textbf{Unique Donor Identifier}.
This ensures that no identifiable information is leaked to a third party including the Donau and charity. This results in a \textbf{Blinded Unique Donor Identifier}.

Blinded Unique Donor Identifiers $\overline u_1, \overline u_2, \overline u_3$
\begin{align*}
  \overline u_1 :&= blind (u_1, b_1, K_1^{pub}) \\
  \overline u_2 :&= blind (u_2, b_2, K_2^{pub}) \\
  \overline u_3 :&= blind (u_3, b_3, K_4^{pub})
\end{align*}

So far, the \textbf{Blinded Unique Donor Identifiers} do not carry information about their value. The \emph{intended effective value is now indicated} by grouping each \textbf{Unique Donor Identifier} with the according hash of the \textbf{Donation Unit} public key $K^{pub}_x$. Resulting in a \textbf{Blinded Unique Donor Identifier Key Pair} (\textbf{BKP})

It is only the \emph{intended effective} value because the value will only be attributed later on with the signature of the Donau.

\emph{Note: The public key is not in relation with the sequential index of the \textbf{BKP}, it only relates to the value of the pair!}

Blinded Unique Donor Identifier Key Pairs $\overline mu_1, \overline mu_2, \overline mu_3$
\begin{align*}
     \overline \mu_1 :&= \langle \overline u_1, h({K^{pub}_1}) \rangle \\
     \overline \mu_2 :&= \langle \overline u_2, h({K^{pub}_2}) \rangle \\
     \overline \mu_3 :&= \langle \overline u_3, h({K^{pub}_4}) \rangle
\end{align*}

These individual \textbf{BKP}'s are then put in an array of \textbf{BKP}'s $\vec{\mu}$
\begin{align*}
     \vec{\mu} :&= \langle \overline \mu_1,
     \overline \mu_2,\overline \mu_3
     \rangle
\end{align*}

The donor sends the array of \textbf{BKP}'s $\vec{\mu}$ as well as the corresponding \textbf{payment} to the charity.

\subsection{Charity receives donation}\label{charity_receives_donation}
Upon receiving the \textbf{BKP}'s $\vec{\mu}$ with the corresponding payment the charity has to verify that the amount requested (based on the \textbf{Donation Unit} public key hash $h(K_x^{pub})$) is \textbf{lower or equal} to the effective amount of the donation.

If the payment was successful with the correct amount present, the charity signs (using EdDSA) a structure containing all unsigned \textbf{BKP}'s $\vec{\mu}$ coming from the donor.

Signing the array of BKP's:
\begin{align*}
    \sigma_c = sign(\vec{\mu}, C^{priv})
\end{align*}

The charity sends the \textbf{BKP}'s $\vec{\mu}$ and the signature $\sigma_c$ to the Donau.

\subsection{Donau creates donation receipt material}\label{donau_creates_donation_receipt}
The Donau now has received the \textbf{BKP}'s $\vec{\mu}$ previously sent by the charity. The Donau must ensure that the charity signature is valid.

Verifing the charity signature $\sigma_c$:
\begin{align*}
  verify(\vec{\mu},\sigma_c, C^{pub})
\end{align*}

Once verified the Donau has to check for any legal restrictions such as the yearly donation limit per charity. Then the Donau increments the current amount of the donations received per year of the charity. This value is increased by the total amount of the \textbf{Blinded Unique Donor Identifier (BUDI)}'s, if the increment does not exceed the annual limit.

After that the Donau blind signs all the \textbf{BUDI}'s using the \textbf{Donation Unit} private keys $K_x^{priv}$ matching the public keys used in the hash $h(K^{pub})$ which was inturn used in the \textbf{BKP}'s.

Donau blind signing Blinded Unique Donor Identifiers $\overline u_1, \overline u_2, \overline u_3$:
\begin{align*}
  \overline{\beta_1} = blind\_sign(\overline u_1, K_1^{priv}) \\
  \overline{\beta_2} = blind\_sign(\overline u_2, K_2^{priv}) \\
  \overline{\beta_3} = blind\_sign(\overline u_3, K_4^{priv})
\end{align*}

The signatures $\overline{\beta_1}, \overline{\beta_2}, \overline{\beta_3}$ are then sent back to the charity which in turn forwards them to the donor. This is done out of simplicity as the charity has already a secure channel open with the donor, eliminating the need to open another channel.

\subsection{Donor receives donation receipt material}\label{donor_receives_donation_receipt}
Upon receiving the Donau signatures $\overline{\beta_1}, \overline{\beta_2}, \overline{\beta_3}$ via the charity, the Donor checks if the blind signatures over the \textbf{Blinded Unique Donor Identifiers} $\overline u_1, \overline u_2, \overline u_3$  are valid:
\begin{align*}
  verify\_blind(u_1,\overline{\beta_1}, K_1^{pub}) \\
  verify\_blind(u_2,\overline{\beta_2}, K_2^{pub}) \\
  verify\_blind(u_3,\overline{\beta_3}, K_4^{pub})
\end{align*}

Once verified the donor unblinds the signatures of the \textbf{BUDI}'s to get the signatures over the \textbf{Unique Donor Identifier (UDI)}'s. This results in a collection of \textbf{Donation Receipt (DR)}'s each consisting of the \textbf{UDI}, the signature $\beta$ and the hash of the \textbf{Donation Unit} public key $h(K_x^{pub})$.

Donor unblinds Donau signatures $\overline{\beta_1}, \overline{\beta_2}, \overline{\beta_3}$:
\begin{align*}
  \beta_1 &= unblind(\overline{\beta_1}, b_1, K_1^{pub}) \\
  \beta_2 &= unblind(\overline{\beta_2}, b_2, K_2^{pub}) \\
  \beta_3 &= unblind(\overline{\beta_3}, b_3, K_4^{pub})
\end{align*}

Donor creates the final Donation Receipts $r_1, r_2, r_3$
\begin{align*}
  r_1 &= \langle UDI_1, \beta_1, h(K_1^{pub}) \rangle \\
  r_2 &= \langle UDI_2, \beta_2, h(K_2^{pub}) \rangle \\
  r_3 &= \langle UDI_3, \beta_3, h(K_4^{pub}) \rangle
\end{align*}

These \textbf{Donation Receipt (DR)} are then stored on the donors device.

\subsection{Donor requests a donation statement from the Donau}\label{donor_requests_a_donation_statement_from_the_donau}
To make the donations tax deductable the donor needs to have a final \textbf{Donation Statement} which can be sent to the tax authority. To get the \textbf{Donation Statement} the donor sends the \textbf{Donation Receipts} $\{r_1, r_2, r_3\}$ accumulated throughout the year to the Donau.
This can be done multiple times during the year. It is not done automatically as to obtain \emph{unlinkability} between the \emph{issuance} of the \textbf{Donation Receipts} (which happens upon donation) and their \emph{submission} for the \textbf{Donation Statement}.

Once the Donau receives the \textbf{Donation Receipts} $\{r_1, r_2, r_3\}$ it has to check that for each \textbf{Donation Receipt}:
\begin{itemize}
  \item the public key $K_x^{pub}$ is known.
  \item the signature $\beta$ is correct using the corresponding public key $K_x^{pub}$.
  \item the \textbf{Donor Identifier} is the same as in other \textbf{Donation Receipts}. (With multiple wallets each wallet must simply obtain a separate \textbf{Donation Statement})
  \item the $\texttt{nonce}$ is unique and was not used before by the donor for the corresponding year.
\end{itemize}

The Donau then signs over the total \texttt{amount} donated by the donor, the current \texttt{year} and the \textbf{Donor Identifier}. This results in a final signature called the \textbf{Donation Statement} which is then sent back to the donor.

Donau creates Donation Statement $\sigma_s$:
\begin{align*}
  \sigma_s = sign(\langle i, \texttt{amount}_{Total}, \texttt{year}) \rangle, D^{priv})
\end{align*}

\subsection{Donor sends final statement to a validator}\label{donor_sends_final_statement_to_a_validator}
The Donor uses the \textbf{Donation Statement} to create a QR-Code which then can be included in the tax declaration.

Donor generates a \texttt{QR} code which contains the \textbf{Donation Statement}:
\begin{align*}
  \texttt{QR} = \langle \texttt{taxid}, \texttt{salt}, \texttt{year}, \texttt{amount}, \sigma_s \rangle
\end{align*}

The validator at the tax office then scans the QR code and verifies the \textbf{Donation Statement Signature} $\sigma_s$.
\begin{align*}
  verify(\langle i, \texttt{amount}_{Total}, \texttt{year}) \rangle,\sigma_s, D^{pub})
\end{align*}

