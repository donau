The Donau environment includes three stakeholders.
Donors, charities and the tax authority (See figure \ref{fig:stakeholders}).
The Donau itself is operated by the tax authority while maintaining a list of
verified charities. Each charity maintains a backend solution that allows it
to communicate with the Donau and the donors.

\begin{figure}[ht]
\begin{center}
\begin{tikzpicture}
    \node (image) at (0,0) {\includegraphics[width=0.1\textwidth]{stickman}};
    \node at (0,-1.8) {Donor};
    %arrow
    \draw (1,0) -- (7,0);
    %charity
    \node (image) at (8.5,0) {\includegraphics[width=0.15\textwidth]{charity}};
    \node at (8.5,-1.1) {Charity};
    %arrow
    \draw (7,-1) -- (5,-2.5);
    %server
    \node (image) at (4,-3) {
    \includegraphics[width=0.12\textwidth]{tax-authority}};
    \node at (4,-4.1) {Tax Authority};
    %arrow
    \draw (1,-1) -- (3,-2.5);
\end{tikzpicture}
\end{center}
\caption{Stakeholders present in the Donau system.} \label{fig:stakeholders}
\end{figure}

\section{Issuing Donation Receipts} \label{issuing_donation_receipts}
When donating to a charity the donor sends the payment together with a receipt
request to the charity.
In order to link the donation to the donor so that the donation receipt cannot
be used by someone else, the donor's unique tax identification number is part
of the receipt request.
This tax ID does not cause a problem for anonymity as the whole receipt
including the tax ID is blinded (See section \ref{blind_signatures}).

In figure \ref{fig:issue receipt request} the blinded receipt is illustrated as
an envelope. The charity must verify if the payment was successful and if the
amount written in the receipt request is lower or equal the amount donated.

Next, if the charity approves the receipt request, it signs the unmodified
request and forwards the request to the Donau.
The Donau accepts only issued requests from recognized charities. For a charity
to be recognized, it must first be registered in the relevant Donau.
When the Donau receives an issue receipt request from a charity, it checks the
validity of the charity signature before the Donau issues the actual donation
receipt by signing the request.

This is different from current systems where the charity usually issues the
receipt. By shifting this task to the Donau, the receipts can easily be
verified and unlink the donor from the charity.
Because the Donau does only know the amount and the charity it is signing for,
this first step of issuing receipts anonymizes the data and provides privacy
for the donor. If the payment process also provides anonymity (as is the case
with GNU Taler) the donations are fully anonymous.

\begin{figure}[ht]
\begin{center}
  \begin{tikzpicture}
    \node (image) at (0,0) {\includegraphics[width=0.1\textwidth]{stickman}};
    \draw [-latex] (1,0) -- (4,0);
    \node (image) at (2,0.5) {\includegraphics[width=0.08\textwidth]{letter}};
    \node at (2,0.5) {\Large{5}};
    \node (image) at (3.2,0.4) {\includegraphics[width=0.05\textwidth]{coins}};
    \node (image) at (5.3,0) {\includegraphics[width=0.15\textwidth]{charity}};
    \draw [-latex] (6.5,0) -- (9.5,0);
    \node (image) at (8,0.5) {\includegraphics[width=0.08\textwidth]{letter}};
    \node (image) at (8,0.5) {\includegraphics[width=0.05\textwidth]{blue_wax}};
    \node at (8,0.5) {\Large{5}};
    \node (image) at (11,0) {
    \includegraphics[width=0.12\textwidth]{servers}};
    \node at (11,-1.2) {Donau};
  \end{tikzpicture} \vspace{0.4cm}
\end{center}
\caption{Donor donates to a charity, sending a issue receipt request to the Donau} \label{fig:issue receipt request}
\end{figure}

Upon receiving the signed issue request from the charity, the Donau must verify
the charity signature and check that the yearly donation limit of the charity
is not exceeded.
After successful verification the Donau blind signs the donation receipt which
is then sent via the charity back to the Donor (See figure \ref{fig:issue
receipt response}).
The donor now unblinds the signature from the Donau to make it valid for the
unblinded receipt (for more information on blind signatures see section \ref{blind_signatures}).
The unblinded receipt gets saved locally on the donors device for later.
This process repeats for every donation. At the end of the year the donor may
have accumulated any number of these donation receipts.

\begin{figure}[ht]
\begin{center}
  \begin{tikzpicture}
    \node (image) at (0,0) {\includegraphics[width=0.1\textwidth]{stickman}};
    \draw [-latex] (4,0) -- (1,0);
    \node (image) at (2.5,-0.5) {\includegraphics[width=0.08\textwidth]{letter}};
    \node (image) at (2.5,-0.5) {\includegraphics[width=0.05\textwidth]{red_wax}};
    \node at (2.5,-0.5) {\large{5}};
    \node (image) at (5.3,0) {\includegraphics[width=0.15\textwidth]{charity}};
    \draw [-latex] (9.5,0) -- (6.5,0);
    \node (image) at (8,-0.5) {\includegraphics[width=0.08\textwidth]{letter}};
    \node (image) at (8,-0.5) {\includegraphics[width=0.05\textwidth]{red_wax}};
    \node at (8,-0.5) {\large{5}};
    \node (image) at (11,0) {
    \includegraphics[width=0.12\textwidth]{servers}};
    \node at (11,-1.2) {Donau};
  \end{tikzpicture} \vspace{0.4cm}
\end{center}
\caption{Donor receives the signed receipts from the Donau} \label{fig:issue receipt response}
\end{figure}

\section{Summarize the Receipts}\label{summarize_the_receipts}
When it is time for the tax declaration (usually at the beginning of the next
year) the donor has to request a final donation statement signature from the
Donau, summarizing all the donation receipts of a year (see figure
\ref{fig:summarize receipts}).
This step combines the amounts of the donation receipts in a single total
amount.
This further protects the privacy of the donor as the individual donations
could be enough information to link up the specific donations to their
corresponding charity and donor.
Merging donation receipts also reduces the time and effort for the manual
verification of the tax authority as the donor generates a single QR-Code
containing the donation statement. This statement contains the total amount
donated, year, tax ID and the signature over all of these values.
This signature is used to verify the donation statement by the tax authority.
The donation statement can be requested multiple times during the year for save
keeping. The latest donation statement will always contain all the receipts of
a year - the old receipts (from previous statements of the year) and the new
donation receipts.

\begin{figure}[ht]
\begin{center}
  \begin{tikzpicture}
    \node (image) at (0,0) {\includegraphics[width=0.1\textwidth]{stickman}};
    %receipt
    \node[rectangle, text width=1.65cm, align=center,font=\tiny, draw=black!80, thick, inner sep=2pt, fill=white] at (3,0.9) {AHVN13: 7560001010000};
    \node (image) at (3,0.9) {\includegraphics[width=0.04\textwidth]{red_wax}};
    \node at (3,0.9) {\small{9}};
    %receipt
    \node[rectangle, text width=1.65cm, align=center,font=\tiny, draw=black!80, thick, inner
    sep=2pt, fill=white] at (4,0.8) {AHVN13: 7560001010000};
    \node (image) at (4,0.8) {\includegraphics[width=0.04\textwidth]{red_wax}};
    \node at (4,0.8) {\small{1}};
    %receipt
    \node[rectangle, text width=1.65cm, align=center,font=\tiny, draw=black!80, thick, inner
    sep=2pt, fill=white] at (5,0.7) {AHVN13: 7560001010000};
    \node (image) at (5,0.7) {\includegraphics[width=0.04\textwidth]{red_wax}};
    \node at (5,0.7) {\small{5}};
    %arrows
    \draw [-latex] (1,0.2) -- (7,0.2);
    \draw [-latex] (7,-0.2) -- (1,-0.2);
    %server
    \node (image) at (8.5,0) {
    \includegraphics[width=0.12\textwidth]{servers}};
    \node at (8.5,-1.2) {Donau};
    %donation statement
    \node (image) at (4,-1) {\includegraphics[width=0.1\textwidth]{gold_wax}};
    \node at (4,-1) {\large{15}};
  \end{tikzpicture}\vspace{0.4cm}
\end{center}
\caption{The Donau summarizes the donation receipts and sends the statement back} \label{fig:summarize receipts}
\end{figure}

\section{Validation}\label{validation}
Once the donor has received the donation statement signature, he can summarize
them in a QR code. The donor must submit the QR-Code with his tax documents,
in order to claim the tax reduction (see figure \ref{fig:validation}).
The final check is made by the tax authority, by checking the donation
statement signature. If the signature is valid, this is the proof that the
specified donor indeed has donated the claimed amount in the indicated year.

\begin{figure}[ht]
\begin{center}
  \begin{tikzpicture}
    \node (image) at (0,0) {\includegraphics[width=0.1\textwidth]{stickman}};
    \node at (0,-1.8) {Donor};
    %arrow
    \draw [-latex] (1,-0.5) -- (7,-0.5);
    %QR-Code
    \draw (2.7,2.5) -- (3.4,0.9);
    \draw (4.9,2.5) -- (4.2,0.9);
    \node (image) at (3.8,2) {\includegraphics[width=0.05\textwidth]{gold_wax}};
    \node at (3.8,2) {\small{15}};
    \node at (3.8,2.5) {\tiny{7560001010000}};
    \node at (3.8,1.3) {\small{2024}};
    \node (image) at (3.8,0.2) {\includegraphics[width=0.1\textwidth]{qr-donau}};
    %server
    \node (image) at (8.5,0) {
    \includegraphics[width=0.12\textwidth]{tax-authority}};
    \node at (8.5,-1.8) {Tax Authority};
  \end{tikzpicture}\vspace{0.4cm}
\end{center}
\caption{The tax authority verifies the statement previously created by the Donau} \label{fig:validation}
\end{figure}

The tax authority will not have any information to which charity the donor
has donated money. The tax authority only knows the total donated amount and
that every donation was made to one of the recognized charities in the
specified year.
This way the donor could make an anonymous donation and still have enough proof
to deduct the amount from taxes.
The Donau will keep track of the total amount of the donation receipts issued
for each charity, to enforce donation limits according to local law and
to prevent donation fraud.

\section{Incorporating the Donau}\label{incorporating_the_donau}
Every donor is delegated to only one specific Donau of his location where he is
able to issue and submit donation receipts for deducting taxes.
If a charity wants to be accepted in multiple tax areas, it has to be
registered by all the corresponding donation authorities.
To do so, the charities has to apply to the tax authorities.
The region for which a Donau is responsible depends on the tax area of the tax
authority and their reglementation of what is charitable.
One Donau could be responsible for a geographical area like a canton, a country
or even a confederation of states.


