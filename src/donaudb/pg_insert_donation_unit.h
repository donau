/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_donation_unit.h
 * @brief implementation of the insert_donation_unit function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_INSERT_DONATION_UNIT_H
#define PG_INSERT_DONATION_UNIT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "donaudb_plugin.h"

/**
 * Add donation unit key.
 *
 * @param cls closure
 * @param donation_unit_hash hash of the public key
 * @param donation_unit_pub the actual donation_unit key
 * @param validity_year validity year
 * @param value value that the donation unit represents
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_insert_donation_unit (
  void *cls,
  const struct DONAU_DonationUnitHashP *h_donation_unit_pub,
  const struct DONAU_DonationUnitPublicKey *donation_unit_pub,
  const uint64_t validity_year,
  const struct TALER_Amount *value);

#endif
