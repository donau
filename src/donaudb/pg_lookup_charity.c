/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_lookup_donation_unit_key.c
 * @brief Implementation of the lookup_donation_unit_key function for Postgres
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_lookup_charity.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
DH_PG_lookup_charity (
  void *cls,
  uint64_t charity_id,
  struct DONAUDB_CharityMetaData *meta)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&charity_id),
    GNUNET_PQ_query_param_end
  };
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_auto_from_type ("charity_pub",
                                          &meta->charity_pub),
    GNUNET_PQ_result_spec_string ("charity_name",
                                  &meta->charity_name),
    GNUNET_PQ_result_spec_string ("charity_url",
                                  &meta->charity_url),
    TALER_PQ_RESULT_SPEC_AMOUNT ("max_per_year",
                                 &meta->max_per_year),
    TALER_PQ_RESULT_SPEC_AMOUNT ("receipts_to_date",
                                 &meta->receipts_to_date),
    GNUNET_PQ_result_spec_uint64 ("current_year",
                                  &meta->current_year),
    GNUNET_PQ_result_spec_end
  };

  PREPARE (pg,
           "lookup_charity",
           "SELECT "
           " charity_pub"
           " ,charity_name"
           " ,charity_url"
           " ,max_per_year"
           " ,receipts_to_date"
           " ,current_year"
           " FROM charities"
           " WHERE charity_id=$1;");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "lookup_charity",
                                                   params,
                                                   rs);
}
