/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_iterate_donation_units.c
 * @brief Implementation of the iterate_donation_units function for Postgres
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_iterate_donation_units.h"
#include "pg_helper.h"
#include "donau_pq_lib.h"

/**
 * Closure for #get_donation_units_cb().
 */
struct IterateDonationUnitsContext
{
  /**
   * Function to call per result.
   */
  DONAUDB_IterateDonationUnitsCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

};

/**
 * Invoke the callback for each result.
 *
 * @param cls a `struct MissingWireContext *`
 * @param result SQL result
 * @param num_results number of rows in @a result
 */
static void
iterate_donation_units_cb (void *cls,
                           PGresult *result,
                           unsigned int num_results)
{
  struct IterateDonationUnitsContext *ctx = cls;
  struct PostgresClosure *pg = ctx->pg;

  for (unsigned int i = 0; i < num_results; i++)
  {
    struct DONAU_DonationUnitHashP h_donation_unit_pub;
    struct DONAU_DonationUnitPublicKey donation_unit_pub;
    uint64_t validity_year;
    struct TALER_Amount value;
    enum GNUNET_GenericReturnValue iret;

    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_auto_from_type ("h_donation_unit_pub",
                                            &h_donation_unit_pub),
      DONAU_PQ_result_spec_donation_unit_pub ("donation_unit_pub",
                                              &donation_unit_pub),
      GNUNET_PQ_result_spec_uint64 ("validity_year",
                                    &validity_year),
      TALER_PQ_RESULT_SPEC_AMOUNT ("value",
                                   &value),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      return;
    }

    iret = ctx->cb (ctx->cb_cls,
                    &h_donation_unit_pub,
                    &donation_unit_pub,
                    validity_year,
                    &value);
    GNUNET_PQ_cleanup_result (rs);
    if (GNUNET_OK != iret)
      break;
  }
}


enum GNUNET_DB_QueryStatus
DH_PG_iterate_donation_units (void *cls,
                              DONAUDB_IterateDonationUnitsCallback cb,
                              void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_end
  };
  struct IterateDonationUnitsContext ctx = {
    .cb = cb,
    .cb_cls = cb_cls,
    .pg = pg
  };

  PREPARE (pg,
           "iterate_donation_units",
           "SELECT"
           " h_donation_unit_pub"
           ",donation_unit_pub"
           ",validity_year"
           ",value"
           " FROM donation_units");
  return GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                               "iterate_donation_units",
                                               params,
                                               &iterate_donation_units_cb,
                                               &ctx);
}
