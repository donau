/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_submitted_receipts.h
 * @brief implementation of the insert_submitted_receipts function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_INSERT_SUBMITTED_RECEIPT_H
#define PG_INSERT_SUBMITTED_RECEIPT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "donaudb_plugin.h"
#include "donau_service.h"

/**
 * Insert submitted donation receipt from the donor.
 *
 * @param cls closure
 * @param h_donor_tax_id salted hash of the donors tax number
 * @param num_dr number of donation receipts
 * @param donation_receipts array of donation receipts
 * @param donation_year year of the donation
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_insert_submitted_receipts (
  void *cls,
  struct DONAU_HashDonorTaxId *h_donor_tax_id,
  size_t num_dr,
  const struct DONAU_DonationReceipt donation_receipts[static num_dr],
  uint64_t donation_year
  );

#endif
