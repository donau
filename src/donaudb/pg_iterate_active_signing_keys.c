/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_iterate_active_signing_keys.c
 * @brief Implementation of the iterate_active_signing_keys function for Postgres
 * @author Christian Grothoff
 */
#include <donau_config.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_iterate_active_signing_keys.h"
#include "pg_helper.h"

/**
 * Closure for #signkeys_cb_helper().
 */
struct IterateActiveSigningKeysContext
{
  /**
   * Function to call per result.
   */
  DONAUDB_IterateActiveSigningKeysCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Flag set to #GNUNET_OK as long as everything is fine.
   */
  enum GNUNET_GenericReturnValue status;

};

/**
 * Invoke the callback for each result.
 *
 * @param cls a `struct MissingWireContext *`
 * @param result SQL result
 * @param num_results number of rows in @a result
 */
static void
signkeys_cb_helper (void *cls,
                    PGresult *result,
                    unsigned int num_results)
{
  struct IterateActiveSigningKeysContext *ctx = cls;

  for (unsigned int i = 0; i < num_results; i++)
  {
    struct DONAU_DonauPublicKeyP donau_pub;
    struct DONAUDB_SignkeyMetaData meta;

    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_auto_from_type ("donau_pub",
                                            &donau_pub),
      GNUNET_PQ_result_spec_timestamp ("valid_from",
                                       &meta.valid_from),
      GNUNET_PQ_result_spec_timestamp ("expire_sign",
                                       &meta.expire_sign),
      GNUNET_PQ_result_spec_timestamp ("expire_legal",
                                       &meta.expire_legal),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ctx->status = GNUNET_SYSERR;
      return;
    }
    ctx->cb (ctx->cb_cls,
             &donau_pub,
             &meta);
    GNUNET_PQ_cleanup_result (rs);
  }
}


enum GNUNET_DB_QueryStatus
DH_PG_iterate_active_signing_keys (void *cls,
                                   DONAUDB_IterateActiveSigningKeysCallback cb,
                                   void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_TIME_Absolute now = {0};
  struct IterateActiveSigningKeysContext ctx = {
    .cb = cb,
    .cb_cls = cb_cls,
    .status = GNUNET_OK
  };
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_absolute_time (&now),
    GNUNET_PQ_query_param_end
  };
  enum GNUNET_DB_QueryStatus qs;

  PREPARE (pg,
           "iterate_active_signing_keys",
           "SELECT"
           " donau_pub"
           ",valid_from"
           ",expire_sign"
           ",expire_legal"
           " FROM donau_sign_keys dsk"
           " WHERE"
           "   expire_sign > $1");
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "iterate_active_signing_keys",
                                             params,
                                             &signkeys_cb_helper,
                                             &ctx);
  if (GNUNET_OK != ctx.status)
    return GNUNET_DB_STATUS_HARD_ERROR;
  return qs;
}
