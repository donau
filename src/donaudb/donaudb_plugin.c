/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donaudb/donaudb_plugin.c
 * @brief Logic to load database plugin
 * @author Johannes Casaburi
 */
#include "donau_config.h"
#include "donau_util.h"
#include "donaudb_plugin.h"
#include <ltdl.h>


struct DONAUDB_Plugin *
DONAUDB_plugin_load (const struct GNUNET_CONFIGURATION_Handle *cfg)
{
  char *plugin_name;
  char *lib_name;
  struct DONAUDB_Plugin *plugin;

  if (GNUNET_SYSERR ==
      GNUNET_CONFIGURATION_get_value_string (cfg,
                                             "donau",
                                             "db",
                                             &plugin_name))
  {
    GNUNET_log_config_missing (GNUNET_ERROR_TYPE_ERROR,
                               "donau",
                               "db");
    return NULL;
  }
  GNUNET_asprintf (&lib_name,
                   "libtaler_plugin_donaudb_%s",
                   plugin_name);
  GNUNET_free (plugin_name);
  plugin = GNUNET_PLUGIN_load (DONAU_project_data (),
                               lib_name,
                               (void *) cfg);
  if (NULL != plugin)
    plugin->library_name = lib_name;
  else
    GNUNET_free (lib_name);
  return plugin;
}


void
DONAUDB_plugin_unload (struct DONAUDB_Plugin *plugin)
{
  char *lib_name;

  if (NULL == plugin)
    return;
  lib_name = plugin->library_name;
  GNUNET_assert (NULL == GNUNET_PLUGIN_unload (lib_name,
                                               plugin));
  GNUNET_free (lib_name);
}


/* end of donaudb_plugin.c */
