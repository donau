/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_issued_receipt.h
 * @brief implementation of the insert_issued_receipt function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_INSERT_ISSUED_RECEIPT_H
#define PG_INSERT_ISSUED_RECEIPT_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "donaudb_plugin.h"

/**
 * Insert issued blinded donation receipt to the charity.
 *
 * @param cls closure
 * @param num_blinded_sig
 * @param signatures blinded signatures
 * @param charity_id identifier of the charity
 * @param h_receipt hash of the donation receipt
 * @param amount_receipts_request donation amount
 * @param smaller_than_max_per_year new receipts to day smaller than the max?
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_insert_issued_receipt (
  void *cls,
  const size_t num_blinded_sig,
  const struct DONAU_BlindedDonationUnitSignature signatures[num_blinded_sig],
  const uint64_t charity_id,
  const struct DONAU_DonationReceiptHashP *h_receipt,
  const struct TALER_Amount *amount_receipts_request,
  bool *smaller_than_max_per_year
  );

#endif
