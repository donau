/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_charity.h
 * @brief implementation of the insert_charity function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_INSERT_CHARITY_H
#define PG_INSERT_CHARITY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "donaudb_plugin.h"

/**
 * Add a new charity
 *
 * @param cls closure
 * @param charity_pub charity public key
 * @param charity_name name
 * @param charity_url url
 * @param max_per_year yearly donation limit
 * @param receipts_to_date current amount of donations in the current year
 * @param current_year current year
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_insert_charity (
  void *cls,
  const struct DONAU_CharityPublicKeyP *charity_pub,
  const char *charity_name,
  const char *charity_url,
  struct TALER_Amount *max_per_year,
  struct TALER_Amount *receipts_to_date,
  uint64_t *current_year,
  uint64_t *charity_id);

#endif
