/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_donation_unit.c
 * @brief Implementation of the insert_donation_unit function for Postgres
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_donation_unit.h"
#include "pg_helper.h"
#include "donau_pq_lib.h"


enum GNUNET_DB_QueryStatus
DH_PG_insert_donation_unit (
  void *cls,
  const struct DONAU_DonationUnitHashP *h_donation_unit_pub,
  const struct DONAU_DonationUnitPublicKey *donation_unit_pub,
  const uint64_t validity_year,
  const struct TALER_Amount *value)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam iparams[] = {
    GNUNET_PQ_query_param_auto_from_type (h_donation_unit_pub),
    DONAU_PQ_query_param_donation_unit_pub (donation_unit_pub),
    GNUNET_PQ_query_param_uint64 (&validity_year),
    TALER_PQ_query_param_amount (pg->conn,
                                 value),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "donation_unit_insert",
           "INSERT INTO donation_units "
           "(h_donation_unit_pub"
           ",donation_unit_pub"
           ",validity_year"
           ",value"
           ") VALUES ($1, $2, $3, $4);");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "donation_unit_insert",
                                             iparams);
}
