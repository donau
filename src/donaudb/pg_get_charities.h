/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_get_charities.h
 * @brief implementation of the get_charities function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_GET_CHARITIES_H
#define PG_GET_CHARITIES_H

#include "donaudb_plugin.h"

/**
 * Obtain information about the enabled wire accounts of the exchange.
 *
 * @param cls closure
 * @param cb function to call on each account
 * @param cb_cls closure for @a cb
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_get_charities (void *cls,
                     DONAUDB_GetCharitiesCallback cb,
                     void *cb_cls);

#endif
