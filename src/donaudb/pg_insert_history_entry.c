/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_history_entry.c
 * @brief Implementation of the insert_history_entry function for Postgres
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_history_entry.h"
#include "pg_helper.h"

enum GNUNET_DB_QueryStatus
DH_PG_insert_history_entry (void *cls,
                            const uint64_t charity_id,
                            const struct TALER_Amount *final_amount,
                            const uint64_t donation_year)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&charity_id),
    TALER_PQ_query_param_amount (pg->conn,
                                 final_amount),
    GNUNET_PQ_query_param_uint64 (&donation_year),
    GNUNET_PQ_query_param_end
  };

  PREPARE (pg,
           "insert_history_entry",
           "INSERT INTO history "
           "(charity_id"
           ",final_amount"
           ",donation_year"
           ") VALUES "
           "($1, $2, $3);");
  return GNUNET_PQ_eval_prepared_non_select (pg->conn,
                                             "insert_history_entry",
                                             params);
}
