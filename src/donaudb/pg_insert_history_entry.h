/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_history_entry.h
 * @brief implementation of the insert_history_entry function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_INSERT_HISTORY_ENTRY_H
#define PG_INSERT_HISTORY_ENTRY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "donaudb_plugin.h"

/**
 * Insert history entry of a charity
 *
 * @param cls closure
 * @param charity_id charity id
 * @param final_amount final donation amount at the end of the donation year
 * @param donation_year year of the donations
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_insert_history_entry (void *cls,
                            const uint64_t charity_id,
                            const struct TALER_Amount *final_amount,
                            const uint64_t donation_year);

#endif
