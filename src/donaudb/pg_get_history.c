/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_get_history.c
 * @brief Implementation of the lookup_donation_unit_key function for Postgres
 * @author Johannes Casaburi
 */
#include "donau_config.h"
#include "taler/taler_error_codes.h"
#include "taler/taler_dbevents.h"
#include "taler/taler_pq_lib.h"
#include "pg_get_history.h"
#include "pg_helper.h"


/**
 * Closure for #get_history_cb().
 */
struct GetHistoryContext
{
  /**
   * Function to call per result.
   */
  DONAUDB_GetHistoryCallback cb;

  /**
   * Closure for @e cb.
   */
  void *cb_cls;

  /**
   * Plugin context.
   */
  struct PostgresClosure *pg;

  /**
   * Number of results processed.
   */
  enum GNUNET_DB_QueryStatus qs;

};


/**
 * Invoke the callback for each result.
 *
 * @param cls a `struct GetHistoryContext *`
 * @param result SQL result
 * @param num_results number of rows in @a result
 */
static void
get_history_cb (void *cls,
                PGresult *result,
                unsigned int num_results)
{
  struct GetHistoryContext *ctx = cls;
  struct PostgresClosure *pg = ctx->pg;

  for (unsigned int i = 0; i < num_results; i++)
  {
    uint64_t charity_id;
    struct TALER_Amount final_amount;
    uint64_t donation_year;

    struct GNUNET_PQ_ResultSpec rs[] = {
      GNUNET_PQ_result_spec_uint64 ("charity_id",
                                    &charity_id),
      TALER_PQ_RESULT_SPEC_AMOUNT ("final_amount",
                                   &final_amount),
      GNUNET_PQ_result_spec_uint64 ("donation_year",
                                    &donation_year),
      GNUNET_PQ_result_spec_end
    };

    if (GNUNET_OK !=
        GNUNET_PQ_extract_result (result,
                                  rs,
                                  i))
    {
      GNUNET_break (0);
      ctx->qs = GNUNET_DB_STATUS_HARD_ERROR;
      return;
    }

    ctx->qs = i + 1;
    if (GNUNET_OK !=
        ctx->cb (ctx->cb_cls,
                 charity_id,
                 final_amount,
                 donation_year))
      break;
  }
}


enum GNUNET_DB_QueryStatus
DH_PG_get_history (void *cls,
                   DONAUDB_GetHistoryCallback cb,
                   void *cb_cls)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_end
  };
  struct GetHistoryContext ctx = {
    .cb = cb,
    .cb_cls = cb_cls,
    .pg = pg
  };
  enum GNUNET_DB_QueryStatus qs;

  PREPARE (pg,
           "get_history",
           "SELECT"
           " charity_id"
           ",final_amount"
           ",donation_year"
           " FROM history");
  qs = GNUNET_PQ_eval_prepared_multi_select (pg->conn,
                                             "get_history",
                                             params,
                                             &get_history_cb,
                                             &ctx);
  if (qs <= 0)
    return qs;
  return ctx.qs;
}
