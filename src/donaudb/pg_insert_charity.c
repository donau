/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_charity.c
 * @brief Implementation of the insert_charity function for Postgres
 * @author Johannes Casaburi
 * @author Lukas Matyja
 */
#include <donau_config.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_charity.h"
#include "pg_helper.h"


enum GNUNET_DB_QueryStatus
DH_PG_insert_charity (
  void *cls,
  const struct DONAU_CharityPublicKeyP *charity_pub,
  const char *charity_name,
  const char *charity_url,
  struct TALER_Amount *max_per_year,
  struct TALER_Amount *receipts_to_date,
  uint64_t *current_year,
  uint64_t *charity_id)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (charity_pub),
    GNUNET_PQ_query_param_string (charity_name),
    GNUNET_PQ_query_param_string (charity_url),
    TALER_PQ_query_param_amount (pg->conn,
                                 max_per_year),
    TALER_PQ_query_param_amount (pg->conn,
                                 receipts_to_date),
    GNUNET_PQ_query_param_uint64 (current_year),
    GNUNET_PQ_query_param_end
  };

  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_uint64 ("charity_id",
                                  charity_id),
    GNUNET_PQ_result_spec_end
  };

  PREPARE (pg,
           "insert_charity",
           "INSERT INTO charities "
           "(charity_pub"
           ",charity_name"
           ",charity_url"
           ",max_per_year"
           ",receipts_to_date"
           ",current_year"
           ") VALUES "
           "($1, $2, $3, $4, $5, $6) "
           "RETURNING charity_id;");
  return GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                   "insert_charity",
                                                   params,
                                                   rs);
}
