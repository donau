/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_lookup_issued_receipts.h
 * @brief implementation of the lookup_issued_receipts function for Postgres
 * @author Lukas Matyja
 */
#ifndef PG_GET_ISSUED_RECEIPTS_H
#define PG_GET_ISSUED_RECEIPTS_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "donaudb_plugin.h"
/**
 * Fetch information about an issued receipts request.
 *
 * @param cls the @e cls of this struct with the plugin-specific state
 * @param h_receipts hash over the issued receipt
 * @param meta information with value and other info about the issued receipts
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_lookup_issued_receipts (
  void *cls,
  struct DONAU_DonationReceiptHashP *h_receipts,
  struct DONAUDB_IssuedReceiptsMetaData *meta);

#endif
