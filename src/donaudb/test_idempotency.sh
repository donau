#!/bin/sh
# This file is in the public domain.
set -eu
psql donaucheck < /dev/null || exit 77
echo "Initializing DB"
donau-dbinit -r -c test-donau-db-postgres.conf
echo "Re-initializing DB"
donau-dbinit -c test-donau-db-postgres.conf
echo "Re-loading procedures"
psql donaucheck < procedures.sql
echo "Test PASSED"
exit 0
