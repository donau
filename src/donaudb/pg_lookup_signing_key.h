/*
   This file is part of TALER
   Copyright (C) 2022 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_lookup_signing_key.h
 * @brief implementation of the lookup_signing_key function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_LOOKUP_SIGNING_KEY_H
#define PG_LOOKUP_SIGNING_KEY_H

#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include "donaudb_plugin.h"


/**
 * Lookup signing key meta data.
 *
 * @param cls closure
 * @param donau_pub the donau signing public key
 * @param[out] meta meta data about @a donau_pub
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_lookup_signing_key (
  void *cls,
  const struct DONAU_DonauPublicKeyP *donau_pub,
  struct DONAUDB_SignkeyMetaData *meta);

#endif
