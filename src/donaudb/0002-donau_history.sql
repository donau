--
-- This file is part of TALER
-- Copyright (C) 2023 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--

CREATE TABLE history
  (charity_id BIGINT PRIMARY KEY REFERENCES charities (charity_id) ON DELETE CASCADE
  ,final_amount taler_amount NOT NULL
  ,donation_year INT8 NOT NULL
  );
COMMENT ON TABLE history
  IS 'Table containing the yearly donation amount for each charity.';
COMMENT ON COLUMN history.final_amount
  IS 'Final donation amount that the charity received.';
