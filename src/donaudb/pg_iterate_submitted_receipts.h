/*
   This file is part of TALER
   Copyright (C) 2024 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_iterate_donation_units.h
 * @brief implementation of the iterate_donation_units function for Postgres
 * @author Johannes Casaburi
 */
#ifndef PG_ITERATE_SUBMITTED_RECEIPTS_H
#define PG_ITERATE_SUBMITTED_RECEIPTS_H

#include "donaudb_plugin.h"

/**
 * Obtain information about the enabled wire accounts of the exchange.
 *
 * @param cls closure
 * @param donation_year donation year
 * @param h_donor_tax_id hash of donor tax id
 * @param[out] total_donations amount of total donations
 * @return transaction status code
 */
enum GNUNET_DB_QueryStatus
DH_PG_iterate_submitted_receipts (
  void *cls,
  const uint64_t donation_year,
  const struct DONAU_HashDonorTaxId *h_donor_tax_id,
  struct TALER_Amount *total_donations);

#endif
