--
-- This file is part of TALER
-- Copyright (C) 2024 Taler Systems SA
--
-- TALER is free software; you can redistribute it and/or modify it under the
-- terms of the GNU General Public License as published by the Free Software
-- Foundation; either version 3, or (at your option) any later version.
--
-- TALER is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
--
CREATE OR REPLACE FUNCTION do_insert_submitted_receipts(
  IN in_h_tax_number BYTEA,
  IN ina_h_donation_unit_pubs BYTEA[],
  IN ina_nonces INT4[],
  IN ina_donation_unit_sigs BYTEA[],
  IN in_donation_year INT8,
  --
  OUT out_conflict BOOL[]
 )
LANGUAGE plpgsql
AS $$
DECLARE
  i INT4;
  ini_nonce INT4;
  ini_h_donation_unit_pub BYTEA;
  ini_donation_unit_sig BYTEA;
BEGIN

-- Insert each donation receipt

FOR i IN 1..array_length(ina_h_donation_unit_pubs,1)
LOOP
  ini_nonce = ina_nonces[i];
  ini_h_donation_unit_pub = ina_h_donation_unit_pubs[i];
  ini_donation_unit_sig = ina_donation_unit_sigs[i];

  out_conflict[i] = FALSE;

  INSERT INTO receipts_submitted
    (h_tax_number
    ,nonce
    ,h_donation_unit_pub
    ,donation_unit_sig
    ,donation_year
    )
    VALUES
    (in_h_tax_number
    ,ini_nonce
    ,ini_h_donation_unit_pub
    ,ini_donation_unit_sig
    ,in_donation_year
    )
    ON CONFLICT DO NOTHING;
  IF NOT FOUND
  THEN
    PERFORM FROM receipts_submitted
      WHERE h_tax_number=in_h_tax_number
        AND donation_unit_sig=ini_donation_unit_sig; -- if signature matches, everything must match
    out_conflict[i] = NOT FOUND;
  END IF;
END LOOP; -- end FOR all receipts

END $$;
