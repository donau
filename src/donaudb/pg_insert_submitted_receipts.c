/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_submitted_receipts.c
 * @brief Implementation of the insert_submitted_receipts function for Postgres
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_submitted_receipts.h"
#include "pg_helper.h"
#include "donau_service.h"
#include "donau_pq_lib.h"

enum GNUNET_DB_QueryStatus
DH_PG_insert_submitted_receipts (
  void *cls,
  struct DONAU_HashDonorTaxId *h_donor_tax_id,
  size_t num_dr,
  const struct DONAU_DonationReceipt donation_receipts[static num_dr],
  uint64_t donation_year)
{
  struct PostgresClosure *pg = cls;
  struct GNUNET_HashCode h_donation_unit_pubs[GNUNET_NZL (num_dr)];
  uint32_t nonces[GNUNET_NZL (num_dr)];
  struct DONAU_DonationUnitSignature donation_unit_sigs[GNUNET_NZL (num_dr)];

  for (unsigned int i = 0; i < num_dr; i++)
  {
    const struct DONAU_DonationReceipt *dr = &donation_receipts[i];

    h_donation_unit_pubs[i] = dr->h_donation_unit_pub.hash;
    nonces[i] = dr->nonce.value;
    donation_unit_sigs[i] = dr->donation_unit_sig;

    GNUNET_log (GNUNET_ERROR_TYPE_INFO,
                "Do insert submitted receipt\n");
  }

  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_auto_from_type (h_donor_tax_id),
    GNUNET_PQ_query_param_array_auto_from_type (num_dr,
                                                h_donation_unit_pubs,
                                                pg->conn),
    GNUNET_PQ_query_param_array_uint32 (num_dr,
                                        nonces,
                                        pg->conn),
    DONAU_PQ_query_param_array_donation_unit_sig (num_dr,
                                                  donation_unit_sigs,
                                                  pg->conn),
    GNUNET_PQ_query_param_uint64 (&donation_year),
    GNUNET_PQ_query_param_end
  };

  bool *conflicted;
  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_array_bool (pg->conn,
                                      "conflicted",
                                      &num_dr,
                                      &conflicted),
    GNUNET_PQ_result_spec_end
  };

  enum GNUNET_DB_QueryStatus qs;

  PREPARE (pg,
           "call_insert_submitted_receipts",
           "SELECT "
           " out_conflict AS conflicted"
           " FROM do_insert_submitted_receipts"
           "($1,$2,$3,$4,$5);");

  qs = GNUNET_PQ_eval_prepared_singleton_select (pg->conn,
                                                 "call_insert_submitted_receipts",
                                                 params,
                                                 rs);
  GNUNET_PQ_cleanup_query_params_closures (params);

  for (size_t i = 0; i < num_dr; i++)
  {
    if (conflicted[i])
    {
      GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                  "Submitted donation receipt at index %ld already present!\n",
                  i);
    }
  }
  return qs;
}
