/*
   This file is part of TALER
   Copyright (C) 2023 Taler Systems SA

   TALER is free software; you can redistribute it and/or modify it under the
   terms of the GNU General Public License as published by the Free Software
   Foundation; either version 3, or (at your option) any later version.

   TALER is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with
   TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * @file donaudb/pg_insert_issued_receipt.c
 * @brief Implementation of the insert_issued_receipt function for Postgres
 * @author Johannes Casaburi
 * @author Lukas Matyja
 */
#include <donau_config.h>
#include <gnunet/gnunet_pq_lib.h>
#include <taler/taler_error_codes.h>
#include <taler/taler_dbevents.h>
#include <taler/taler_pq_lib.h>
#include "pg_insert_issued_receipt.h"
#include "pg_helper.h"
#include "donau_service.h"
#include "donau_pq_lib.h"

enum GNUNET_DB_QueryStatus
DH_PG_insert_issued_receipt (
  void *cls,
  const size_t num_blinded_sig,
  const struct DONAU_BlindedDonationUnitSignature signatures[num_blinded_sig],
  const uint64_t charity_id,
  const struct DONAU_DonationReceiptHashP *h_receipt,
  const struct TALER_Amount *amount_receipts_request,
  bool *smaller_than_max_per_year)
{
  struct PostgresClosure *pc = cls;

  struct GNUNET_PQ_ResultSpec rs[] = {
    GNUNET_PQ_result_spec_bool ("smaller_than_max_per_year",
                                smaller_than_max_per_year),
    GNUNET_PQ_result_spec_end
  };

  struct GNUNET_PQ_QueryParam params[] = {
    GNUNET_PQ_query_param_uint64 (&charity_id),
    DONAU_PQ_query_param_array_blinded_donation_unit_sig (num_blinded_sig,
                                                          signatures,
                                                          pc->conn),
    GNUNET_PQ_query_param_auto_from_type (&h_receipt->hash),
    TALER_PQ_query_param_amount (pc->conn,
                                 amount_receipts_request),
    GNUNET_PQ_query_param_end
  };

  enum GNUNET_DB_QueryStatus qs;

  PREPARE (pc,
           "insert_issued_receipts_request",
           "SELECT "
           " out_smaller_than_max_per_year AS smaller_than_max_per_year"
           " FROM do_insert_issued_receipts"
           "($1,$2,$3,$4);");

  qs = GNUNET_PQ_eval_prepared_singleton_select (pc->conn,
                                                 "insert_issued_receipts_request",
                                                 params,
                                                 rs);
  GNUNET_PQ_cleanup_query_params_closures (params);
  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "Is the new receipts_to_day smaller than the max_per_year (1 = true): %d\n",
              (*smaller_than_max_per_year));
  return qs;
}
