/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing/testing_api_cmd_submit_receipts.c
 * @brief Implement the POST /charities test command.
 * @author Lukas Matyja
 */
#include <donau_config.h>
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_testing_lib.h>
#include "donau_testing_lib.h"


/**
 * State for a "status" CMD.
 */
struct StatusState
{
  /**
   * Handle to the "charity status" operation.
   */
  struct DONAU_DonorReceiptsToStatementHandle *bsrh;

  /**
   * Expected HTTP response code.
   */
  unsigned int expected_response_code;

  /**
   * Interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Issue receipts reference.
   */
  const char *issue_receipt_reference;

  /**
   * corresponding year
   */
  unsigned long long year;

  /**
   * number of donation receipts.
   */
  size_t num_receipts;

  /**
   * array of donation receipts
   */
  const struct DONAU_DonationReceipt *receipts;

  /**
   * donau keys
   */
  struct DONAU_Keys *keys;

  /**
   * Hashed and salted tax id of the donor.
   */
  const struct DONAU_HashDonorTaxId *h_donor_tax_id;

};


/**
 * Check that the reserve balance and HTTP response code are
 * both acceptable.
 *
 * @param cls closure.
 * @param rtsresp HTTP response details
 */
static void
submit_receipts_status_cb (void *cls,
                           const struct
                           DONAU_DonorReceiptsToStatementResult *rtsresp)
{
  struct StatusState *ss = cls;

  ss->bsrh = NULL;
  if (ss->expected_response_code != rtsresp->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected HTTP response code: %d in %s:%u\n",
                rtsresp->hr.http_status,
                __FILE__,
                __LINE__);
    json_dumpf (rtsresp->hr.reply,
                stderr,
                0);
    TALER_TESTING_interpreter_fail (ss->is);
    return;
  }

  TALER_TESTING_interpreter_next (ss->is);
}


/**
 * Run the command.
 *
 * @param cls closure.
 * @param cmd the command being executed.
 * @param is the interpreter state.
 */
static void
status_run (void *cls,
            const struct TALER_TESTING_Command *cmd,
            struct TALER_TESTING_Interpreter *is)
{
  struct StatusState *ss = cls;
  ss->is = is;

  /* Get donau keys from trait */
  {
    const struct TALER_TESTING_Command *keys_cmd;
    struct DONAU_Keys *keys;

    keys_cmd = TALER_TESTING_interpreter_lookup_command (is,
                                                         "get-donau");

    if (GNUNET_OK !=
        TALER_TESTING_get_trait_donau_keys (keys_cmd, &keys))
    {
      GNUNET_break (0);
      TALER_TESTING_interpreter_fail (is);
      return;
    }
    ss->keys = keys;
  }

  /* Get donau keys from trait */
  {
    const struct TALER_TESTING_Command *receipts_cmd;
    const struct DONAU_DonationReceipt **receipts;
    const struct DONAU_HashDonorTaxId *h_donor_tax_id;
    const size_t *num_receipts;

    receipts_cmd = TALER_TESTING_interpreter_lookup_command (is,
                                                             ss->
                                                             issue_receipt_reference);

    if (GNUNET_OK !=
        TALER_TESTING_get_trait_donation_receipts (receipts_cmd, &receipts) ||
        GNUNET_OK !=
        TALER_TESTING_get_trait_salted_tax_id_hash (receipts_cmd,
                                                    &h_donor_tax_id) ||
        GNUNET_OK !=
        TALER_TESTING_get_trait_number_receipts (receipts_cmd, &num_receipts) )
    {
      GNUNET_break (0);
      TALER_TESTING_interpreter_fail (is);
      return;
    }
    ss->num_receipts = *num_receipts;
    ss->receipts = *receipts;
    ss->h_donor_tax_id = h_donor_tax_id;
  }

  ss->bsrh = DONAU_donor_receipts_to_statement (
    TALER_TESTING_interpreter_get_context (is),
    TALER_TESTING_get_donau_url (is),
    ss->num_receipts,
    ss->receipts,
    ss->year,
    ss->h_donor_tax_id,
    &submit_receipts_status_cb,
    ss);

}


/**
 * Cleanup the state from a "issue receipt status" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd the command which is being cleaned up.
 */
static void
cleanup (void *cls,
         const struct TALER_TESTING_Command *cmd)
{
  struct StatusState *ss = cls;

  if (NULL != ss->bsrh)
  {
    // log incomplete command
    TALER_TESTING_command_incomplete (ss->is,
                                      cmd->label);
    DONAU_donor_receipts_to_statement_cancel (ss->bsrh);
    ss->bsrh = NULL;
  }
  GNUNET_free (ss);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_submit_receipts (const char *label,
                                   const char *issue_receipt_reference,
                                   const uint64_t year,
                                   unsigned int expected_response_code)
{
  struct StatusState *ss;

  ss = GNUNET_new (struct StatusState);

  ss->year = year;
  ss->expected_response_code = expected_response_code;
  ss->issue_receipt_reference = issue_receipt_reference;

  {
    struct TALER_TESTING_Command cmd = {
      .cls = ss,
      .label = label,
      .run = &status_run,
      .cleanup = &cleanup
    };

    return cmd;

  }
}
