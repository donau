/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing/testing_api_traits.c
 * @brief loop for trait resolution
 * @author Christian Grothoff
 * @author Marcello Stanisci
 */
#include <donau_config.h>
#include "donau_testing_lib.h"


DONAU_TESTING_SIMPLE_TRAITS (TALER_TESTING_MAKE_IMPL_SIMPLE_TRAIT)

DONAU_TESTING_INDEXED_TRAITS (TALER_TESTING_MAKE_IMPL_INDEXED_TRAIT)

const char *
TALER_TESTING_get_donau_url (struct TALER_TESTING_Interpreter *is)
{
  const char *donau_url;
  const struct TALER_TESTING_Command *donau_cmd;

  donau_cmd
    = TALER_TESTING_interpreter_get_command (is,
                                             "donau");
  if (NULL == donau_cmd)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (is);
    return NULL;
  }
  if (GNUNET_OK !=
      TALER_TESTING_get_trait_donau_url (donau_cmd,
                                         &donau_url))
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (is);
    return NULL;
  }
  return donau_url;
}

/* end of testing_api_traits.c */
