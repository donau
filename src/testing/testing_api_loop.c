/*
  This file is part of TALER
  Copyright (C) 2018-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file testing/testing_api_loop.c
 * @brief main interpreter loop for testcases
 * @author Lukas Matyja
 */
#include "donau_config.h"
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_signatures.h>
#include <taler/taler_testing_lib.h>
#include "donau_testing_lib.h"
#include "donau_util.h"


int
DONAU_TESTING_main (char *const *argv,
                    const char *loglevel,
                    const char *cfg_file,
                    struct TALER_TESTING_Credentials *cred,
                    TALER_TESTING_Main main_cb,
                    void *main_cb_cls)
{
  enum GNUNET_GenericReturnValue ret;

  unsetenv ("XDG_DATA_HOME");
  unsetenv ("XDG_CONFIG_HOME");
  GNUNET_log_setup (argv[0],
                    loglevel,
                    NULL);
  cred->cfg = GNUNET_CONFIGURATION_create (DONAU_project_data ());
  if (GNUNET_OK !=
      GNUNET_CONFIGURATION_load (cred->cfg,
                                 cfg_file))
  {
    GNUNET_break (0);
    GNUNET_CONFIGURATION_destroy (cred->cfg);
    return GNUNET_SYSERR;
  }
  if (GNUNET_OK !=
      GNUNET_CONFIGURATION_get_value_string (cred->cfg,
                                             "donau",
                                             "BASE_URL",
                                             &cred->exchange_url))
  {
    GNUNET_log_config_missing (GNUNET_ERROR_TYPE_ERROR,
                               "donau",
                               "BASE_URL");
    return GNUNET_SYSERR;
  }
  // if (GNUNET_OK !=
  //    TALER_TESTING_cleanup_files_cfg (NULL,
  //                                     cred->cfg))
  // {
  //   GNUNET_break (0);
  //   return 77;
  // }
  ret = TALER_TESTING_loop (main_cb,
                            main_cb_cls);
  /* FIXME: should we free 'cred' resources here? */
  return (GNUNET_OK == ret) ? 0 : 1;
}
