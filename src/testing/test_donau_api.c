/*
  This file is part of TALER
  Copyright (C) 2014-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing/test_exchange_api.c
 * @brief testcase to test exchange's HTTP API interface
 * @author Sree Harsha Totakura <sreeharsha@totakura.in>
 * @author Christian Grothoff
 * @author Marcello Stanisci
 * @author Lukas Matyja
 */
#include <donau_config.h>
#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_testing_lib.h>
#include <microhttpd.h>
#include <taler/taler_testing_lib.h>
#include "donau_testing_lib.h"

/**
 * Configuration file we use.  One (big) configuration is used
 * for the various components for this test.
 */
static char *config_file;

/**
 * Our credentials.
 */
static struct TALER_TESTING_Credentials cred;

/**
 * Issue receipts tests behave differently when using CS.
 */
static bool uses_cs;

/**
 * Main function that will tell the interpreter what commands to
 * run.
 *
 * @param cls closure
 * @param is interpreter we use to run commands
 */
static void
run (void *cls,
     struct TALER_TESTING_Interpreter *is)
{
  const struct DONAU_BearerToken bearer = {
    .token = NULL
  };
  {
    struct TALER_TESTING_Command commands[] = {
      TALER_TESTING_cmd_system_start ("start-donau",
                                      config_file,
                                      "-D",
                                      "NULL"),
      TALER_TESTING_cmd_get_donau ("get-donau",
                                   cred.cfg,
                                   true),
      TALER_TESTING_cmd_charity_post ("post-charity",
                                      "example",
                                      "example.com",
                                      "EUR:50", // max_per_year
                                      "EUR:0", // receipts_to_date
                                      2024, // current year
                                      &bearer,
                                      MHD_HTTP_CREATED),
      TALER_TESTING_cmd_charity_get ("get-charity-by-id",
                                     "post-charity", // cmd trait reference
                                     &bearer,
                                     MHD_HTTP_OK),
      TALER_TESTING_cmd_charities_get ("get-charities",
                                       &bearer,
                                       MHD_HTTP_OK),
      // FIXME: CSR signatures
      TALER_TESTING_cmd_issue_receipts ("issue-receipts",
                                        "post-charity",
                                        uses_cs,
                                        2024,
                                        "7560001010000", // tax id
                                        "1234", // salt for tax id hash
                                        MHD_HTTP_CREATED),
      TALER_TESTING_cmd_submit_receipts ("submit-receipts",
                                         "issue-receipts", // cmd trait reference
                                         2024,
                                         MHD_HTTP_CREATED),
      TALER_TESTING_cmd_donation_statement_get ("donation-statement",
                                                2024,
                                                MHD_HTTP_OK),
      TALER_TESTING_cmd_charity_delete ("delete-charity",
                                        "post-charity", // cmd trait reference
                                        &bearer,
                                        MHD_HTTP_NO_CONTENT),
      /* End the suite. */
      TALER_TESTING_cmd_end ()
    };

    TALER_TESTING_run (is,
                       commands);
  }
}


int
main (int argc,
      char *const *argv)
{
  (void) argc;
  {
    char *cipher;

    cipher = GNUNET_STRINGS_get_suffix_from_binary_name (argv[0]);
    GNUNET_assert (NULL != cipher);
    uses_cs = (0 == strcmp (cipher,
                            "cs"));
    GNUNET_asprintf (&config_file,
                     "test_donau_api-%s.conf",
                     cipher);
    GNUNET_free (cipher);
  }
  return DONAU_TESTING_main (argv,
                             "INFO",
                             config_file,
                             &cred,
                             &run,
                             NULL);
}


/* end of test_donau_api.c */
