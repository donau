/*
  This file is part of TALER
  Copyright (C) 2014-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing/testing_api_cmd_charity_get.c
 * @brief Implement the GET /charities/$ID test command.
 * @author Marcello Stanisci
 * @author Lukas Matyja
 */
#include <donau_config.h>
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_testing_lib.h>
#include "donau_testing_lib.h"


/**
 * State for a "status" CMD.
 */
struct StatusState
{
  /**
   * Handle to the "charity status" operation.
   */
  struct DONAU_CharityGetHandle *cgh;

  /**
   * The bearer token for authorization.
   */
  const struct DONAU_BearerToken *bearer;

  /**
   * The ID of the requested charity.
   */
  uint64_t charity_id;

  /**
   * Expected HTTP response code.
   */
  unsigned int expected_response_code;

  /**
   * Interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Reference to charity post command.
   */
  const char *charity_reference;

};

/**
 * Check that the reserve balance and HTTP response code are
 * both acceptable.
 *
 * @param cls closure.
 * @param gcr HTTP response details
 */
static void
charity_status_cb (void *cls,
                   const struct DONAU_GetCharityResponse *gcr)
{
  struct StatusState *ss = cls;

  ss->cgh = NULL;
  if (ss->expected_response_code != gcr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected HTTP response code: %d in %s:%u\n",
                gcr->hr.http_status,
                __FILE__,
                __LINE__);
    json_dumpf (gcr->hr.reply,
                stderr,
                0);
    TALER_TESTING_interpreter_fail (ss->is);
    return;
  }
  TALER_TESTING_interpreter_next (ss->is);
}


/**
 * Run the command.
 *
 * @param cls closure.
 * @param cmd the command being executed.
 * @param is the interpreter state.
 */
static void
status_run (void *cls,
            const struct TALER_TESTING_Command *cmd,
            struct TALER_TESTING_Interpreter *is)
{
  struct StatusState *ss = cls;

  ss->is = is;
  /* Get charity id from trait */
  {
    const struct TALER_TESTING_Command *charity_post_cmd;
    const unsigned long long *charity_id;

    charity_post_cmd = TALER_TESTING_interpreter_lookup_command (is,
                                                                 ss->
                                                                 charity_reference);

    if (GNUNET_OK !=
        TALER_TESTING_get_trait_charity_id (charity_post_cmd,
                                            &charity_id))
    {
      GNUNET_break (0);
      TALER_TESTING_interpreter_fail (is);
      return;
    }
    ss->charity_id = (uint64_t) *(charity_id);
  }

  ss->cgh = DONAU_charity_get (
    TALER_TESTING_interpreter_get_context (is),
    TALER_TESTING_get_donau_url (is),
    ss->charity_id,
    ss->bearer,
    &charity_status_cb,
    ss);
}


/**
 * Cleanup the state from a "reserve status" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd the command which is being cleaned up.
 */
static void
status_cleanup (void *cls,
                const struct TALER_TESTING_Command *cmd)
{
  struct StatusState *ss = cls;

  if (NULL != ss->cgh)
  {
    // log incomplete command
    TALER_TESTING_command_incomplete (ss->is,
                                      cmd->label);
    DONAU_charity_get_cancel (ss->cgh);
    ss->cgh = NULL;
  }
  GNUNET_free (ss);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_charity_get (const char *label,
                               const char *charity_reference,
                               const struct DONAU_BearerToken *bearer,
                               unsigned int expected_response_code)
{
  struct StatusState *ss;
  ss = GNUNET_new (struct StatusState);
  ss->expected_response_code = expected_response_code;
  ss->bearer = bearer;
  ss->charity_reference = charity_reference;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = ss,
      .label = label,
      .run = &status_run,
      .cleanup = &status_cleanup
    };

    return cmd;
  }
}
