/*
  This file is part of TALER
  Copyright (C) 2014-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing/testing_api_cmd_charity_post.c
 * @brief Implement the POST /charities test command.
 * @author Lukas Matyja
 */
#include <donau_config.h>
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_testing_lib.h>
#include "donau_testing_lib.h"


/**
 * State for a "status" CMD.
 */
struct StatusState
{
  /**
   * Handle to the "charity status" operation.
   */
  struct DONAU_CharityPostHandle *cph;

  /**
   * The charity POST request.
   */
  struct DONAU_CharityRequest charity_req;

  /**
   * Private key of the charity, created here.
   */
  struct DONAU_CharityPrivateKeyP charity_priv;

  /**
   * The bearer token for authorization.
   */
  const struct DONAU_BearerToken *bearer;

  /**
   * Expected HTTP response code.
   */
  unsigned int expected_response_code;

  /**
   * Interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * charity id
   */
  unsigned long long charity_id;

};


/**
 * Check that the reserve balance and HTTP response code are
 * both acceptable.
 *
 * @param cls closure.
 * @param gcr HTTP response details
 */
static void
charity_status_cb (void *cls,
                   const struct DONAU_PostCharityResponse *gcr)
{
  struct StatusState *ss = cls;

  ss->cph = NULL;
  if (ss->expected_response_code != gcr->hr.http_status)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected HTTP response code: %d in %s:%u\n",
                gcr->hr.http_status,
                __FILE__,
                __LINE__);
    json_dumpf (gcr->hr.reply,
                stderr,
                0);
    TALER_TESTING_interpreter_fail (ss->is);
    return;
  }
  if (ss->expected_response_code == gcr->hr.http_status)
    ss->charity_id = (unsigned long long) gcr->details.ok.charity_id;
  TALER_TESTING_interpreter_next (ss->is);
}


/**
 * Run the command.
 *
 * @param cls closure.
 * @param cmd the command being executed.
 * @param is the interpreter state.
 */
static void
status_run (void *cls,
            const struct TALER_TESTING_Command *cmd,
            struct TALER_TESTING_Interpreter *is)
{
  struct StatusState *ss = cls;

  ss->is = is;

  ss->cph = DONAU_charity_post (
    TALER_TESTING_interpreter_get_context (is),
    TALER_TESTING_get_donau_url (is),
    &ss->charity_req,
    ss->bearer,
    &charity_status_cb,
    ss);
}


/**
 * Cleanup the state from a "reserve status" CMD, and possibly
 * cancel a pending operation thereof.
 *
 * @param cls closure.
 * @param cmd the command which is being cleaned up.
 */
static void
cleanup (void *cls,
         const struct TALER_TESTING_Command *cmd)
{
  struct StatusState *ss = cls;

  if (NULL != ss->cph)
  {
    // log incomplete command
    TALER_TESTING_command_incomplete (ss->is,
                                      cmd->label);
    DONAU_charity_post_cancel (ss->cph);
    ss->cph = NULL;
  }
  GNUNET_free (ss);
}


/**
 * Offer internal data from a "deposit" CMD, to other commands.
 *
 * @param cls closure.
 * @param[out] ret result.
 * @param trait name of the trait.
 * @param index index number of the object to offer.
 * @return #GNUNET_OK on success.
 */
static enum GNUNET_GenericReturnValue
charity_post_traits (void *cls,
                     const void **ret,
                     const char *trait,
                     unsigned int index)
{
  struct StatusState *ss = cls;
  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_charity_priv (&ss->charity_priv),
    TALER_TESTING_make_trait_charity_pub (&ss->charity_req.charity_pub),
    TALER_TESTING_make_trait_charity_id (&ss->charity_id),
    TALER_TESTING_trait_end ()
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_charity_post (const char *label,
                                const char *name,
                                const char *url,
                                const char *max_per_year,
                                const char *receipts_to_date,
                                uint64_t current_year,
                                const struct DONAU_BearerToken *bearer,
                                unsigned int expected_response_code)
{
  struct StatusState *ss;

  ss = GNUNET_new (struct StatusState);
  GNUNET_CRYPTO_eddsa_key_create (&ss->charity_priv.eddsa_priv);
  GNUNET_CRYPTO_eddsa_key_get_public (&ss->charity_priv.eddsa_priv,
                                      &ss->charity_req.charity_pub.eddsa_pub);
  ss->charity_req.name = name;
  ss->charity_req.charity_url = url;
  // parse string max_per_year to amount
  if (GNUNET_OK !=
      TALER_string_to_amount (max_per_year,
                              &ss->charity_req.max_per_year))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Failed to parse amount `%s' at %s\n",
                max_per_year,
                label);
    GNUNET_assert (0);
  }
  // parse string receipts_to_date to amount
  if (GNUNET_OK !=
      TALER_string_to_amount (receipts_to_date,
                              &ss->charity_req.receipts_to_date))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Failed to parse amount `%s' at %s\n",
                receipts_to_date,
                label);
    GNUNET_assert (0);
  }
  ss->charity_req.current_year = current_year;
  ss->expected_response_code = expected_response_code;
  ss->bearer = bearer;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = ss,
      .label = label,
      .run = &status_run,
      .cleanup = &cleanup,
      .traits = &charity_post_traits
    };

    return cmd;
  }
}
