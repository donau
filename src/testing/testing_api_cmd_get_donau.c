/*
  This file is part of TALER
  (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/
/**
 * @file testing/testing_api_cmd_get_donau.c
 * @brief Command to get an donau handle
 * @author Christian Grothoff
 * @author Lukas Matyja
 */
#include <donau_config.h>
#include <taler/taler_json_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_testing_lib.h>
#include "donau_testing_lib.h"
#include "donau_service.h"

/**
 * State for a "get donau" CMD.
 */
struct GetDonauState
{

  /**
   * Our interpreter state.
   */
  struct TALER_TESTING_Interpreter *is;

  /**
   * Donau handle we produced.
   */
  struct DONAU_GetKeysHandle *donau;

  /**
   * Keys of the donau.
   */
  struct DONAU_Keys *keys;

  /**
   * URL of the donau.
   */
  char *donau_url;

  /**
   * Are we waiting for /keys before continuing?
   */
  bool wait_for_keys;
};


/**
 * Function called with information about what keys the donau is using.
 *
 * @param cls closure
 * @param kr response from /keys
 * @param[in] keys the keys of the donau
 */
static void
cert_cb (void *cls,
         const struct DONAU_KeysResponse *kr,
         struct DONAU_Keys *keys)
{
  struct GetDonauState *ges = cls;
  const struct DONAU_HttpResponse *hr = &kr->hr;
  struct TALER_TESTING_Interpreter *is = ges->is;

  ges->donau = NULL;
  ges->keys = keys;
  switch (hr->http_status)
  {
  case MHD_HTTP_OK:
    if (ges->wait_for_keys)
    {
      ges->wait_for_keys = false;
      TALER_TESTING_interpreter_next (is);
      return;
    }
    return;
  default:
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_WARNING,
                "/keys responded with HTTP status %u\n",
                hr->http_status);
    if (ges->wait_for_keys)
    {
      ges->wait_for_keys = false;
      TALER_TESTING_interpreter_fail (is);
      return;
    }
    return;
  }

  return;
}


/**
 * Run the "get_donau" command.
 *
 * @param cls closure.
 * @param cmd the command currently being executed.
 * @param is the interpreter state.
 */
static void
get_donau_run (void *cls,
               const struct TALER_TESTING_Command *cmd,
               struct TALER_TESTING_Interpreter *is)
{
  struct GetDonauState *ges = cls;

  (void) cmd;
  if (NULL == ges->donau_url)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (is);
    return;
  }

  ges->is = is;
  ges->donau
    = DONAU_get_keys (TALER_TESTING_interpreter_get_context (is),
                      ges->donau_url,
                      &cert_cb,
                      ges);
  if (NULL == ges->donau)
  {
    GNUNET_break (0);
    TALER_TESTING_interpreter_fail (is);
    return;
  }
  if (! ges->wait_for_keys)
    TALER_TESTING_interpreter_next (is);
}


/**
 * Cleanup the state.
 *
 * @param cls closure.
 * @param cmd the command which is being cleaned up.
 */
static void
get_donau_cleanup (void *cls,
                   const struct TALER_TESTING_Command *cmd)
{
  struct GetDonauState *ges = cls;

  if (NULL != ges->donau)
  {
    DONAU_get_keys_cancel (ges->donau);
    ges->donau = NULL;
  }
  DONAU_keys_decref (ges->keys);
  ges->keys = NULL;
  GNUNET_free (ges->donau_url);
  ges->donau_url = NULL;
  GNUNET_free (ges);
}


/**
 * Offer internal data to a "get_donau" CMD state to other commands.
 *
 * @param cls closure
 * @param[out] ret result (could be anything)
 * @param trait name of the trait
 * @param index index number of the object to offer.
 * @return #GNUNET_OK on success
 */
static enum GNUNET_GenericReturnValue
get_donau_traits (void *cls,
                  const void **ret,
                  const char *trait,
                  unsigned int index)
{
  struct GetDonauState *ges = cls;

  struct TALER_TESTING_Trait traits[] = {
    TALER_TESTING_make_trait_donau_url (ges->donau_url),
    TALER_TESTING_make_trait_donau_keys (ges->keys),
    TALER_TESTING_trait_end ()
  };

  return TALER_TESTING_get_trait (traits,
                                  ret,
                                  trait,
                                  index);

}


/**
 * Get the base URL of the donau from @a cfg.
 *
 * @param cfg configuration to evaluate
 * @return base URL of the donau according to @a cfg
 */
static char *
get_donau_base_url (
  const struct GNUNET_CONFIGURATION_Handle *cfg)
{
  char *donau_url;

  if (GNUNET_OK !=
      GNUNET_CONFIGURATION_get_value_string (cfg,
                                             "donau",
                                             "BASE_URL",
                                             &donau_url))
  {
    GNUNET_log_config_missing (GNUNET_ERROR_TYPE_ERROR,
                               "donau",
                               "BASE_URL");
    return NULL;
  }
  return donau_url;
}


struct TALER_TESTING_Command
TALER_TESTING_cmd_get_donau (
  const char *label,
  const struct GNUNET_CONFIGURATION_Handle *cfg,
  bool wait_for_keys)
{
  struct GetDonauState *ges;

  ges = GNUNET_new (struct GetDonauState);
  ges->donau_url = get_donau_base_url (cfg);
  ges->wait_for_keys = wait_for_keys;
  {
    struct TALER_TESTING_Command cmd = {
      .cls = ges,
      .label = label,
      .run = &get_donau_run,
      .cleanup = &get_donau_cleanup,
      .traits = &get_donau_traits,
      .name = "donau"
    };

    return cmd;
  }
}
