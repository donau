/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file lib/donau_api_batch_issue_receipts.c
 * @brief Implementation of the "handle" component of the donau's HTTP API
 * @author Lukas Matyja
 */
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>
#include "donau_service.h"
#include "donau_util.h"
#include "donau_api_curl_defaults.h"
#include "donau_json_lib.h"


/**
 * Handle for a POST /batch-issue/$CHARITY_ID request.
 */
struct DONAU_BatchIssueReceiptHandle
{
  /**
   * The url for the /batch-issue/$CHARITY_ID request.
   */
  char *url;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * Entry for this request with the `struct GNUNET_CURL_Context`.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  DONAU_BatchIssueReceiptsCallback cb;

  /**
   * BUDI-key-pair signature.
   */
  struct DONAU_CharitySignatureP charity_sig;

  /**
   * number of requested signatures.
   */
  size_t num_blinded_sigs;

  /**
   * Closure to pass to @e cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};


/**
 * Decode the JSON in @a resp_obj from the /batch-issue/$CHARITY_ID response
 * and store the data in the @a biresp.
 *
 * @param[in] resp_obj JSON object to parse
 * @param[in] birh contains the callback function
 * @param[out] biresp where to store the results we decoded
 * @return #GNUNET_OK on success, #GNUNET_SYSERR on error
 * (malformed JSON)
 */
static enum GNUNET_GenericReturnValue
handle_batch_issue_ok (const json_t *resp_obj,
                       struct DONAU_BatchIssueReceiptHandle *birh,
                       struct DONAU_BatchIssueResponse *biresp)
{
  struct TALER_Amount issued_amount = biresp->details.ok.issued_amount;

  if (JSON_OBJECT != json_typeof (resp_obj))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }
  const json_t *j_blind_signatures;
  struct GNUNET_JSON_Specification spec[] = {
    TALER_JSON_spec_amount_any ("issued_amount",
                                &issued_amount),
    GNUNET_JSON_spec_array_const ("blind_signatures",
                                  &j_blind_signatures),
    GNUNET_JSON_spec_end ()
  };
  if (GNUNET_OK !=
      GNUNET_JSON_parse (resp_obj,
                         spec,
                         NULL,
                         NULL))
  {
    GNUNET_break_op (0);
    return GNUNET_SYSERR;
  }

  if ( (NULL == j_blind_signatures) ||
       (! json_is_array (j_blind_signatures)) ||
       (birh->num_blinded_sigs != json_array_size (j_blind_signatures)) )
  {
    GNUNET_break (0);
    return GNUNET_SYSERR;
  }
  biresp->details.ok.blinded_sigs =
    GNUNET_new_array (birh->num_blinded_sigs,
                      struct DONAU_BlindedDonationUnitSignature);
  size_t index;
  json_t *du_sig_obj;
  json_array_foreach (j_blind_signatures,
                      index,
                      du_sig_obj)
  {
    struct GNUNET_JSON_Specification spec[] = {
      DONAU_JSON_spec_blinded_donation_unit_sig (
        "blinded_signature",
        &biresp->details.ok.blinded_sigs[index]),
      GNUNET_JSON_spec_end ()
    };
    if (GNUNET_OK !=
        GNUNET_JSON_parse (du_sig_obj,
                           spec,
                           NULL,
                           NULL))
    {
      GNUNET_break_op (0);
      return GNUNET_SYSERR;
    }
  }

  birh->cb (birh->cb_cls,
            biresp);
  birh->cb = NULL;
  return GNUNET_OK;
}


/**
 * Transform issue receipt request into JSON.
 *
 * @param num_bkp number of budi-key-pairs in @bkp
 * @param bkp budi-key-pair array
 * @param year corresponding year
 * @param charity_sig signature from charity over @bkp
 */
static json_t *
issue_receipt_body_to_json (
  const unsigned int num_bkp,
  const struct DONAU_BlindedUniqueDonorIdentifierKeyPair *bkp,
  const uint64_t year,
  const struct DONAU_CharitySignatureP *charity_sig)
{
  json_t *budikeypairs = json_array ();
  GNUNET_assert (NULL != budikeypairs);

  for (size_t i = 0; i < num_bkp; i++)
  {
    json_t *budikeypair = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto ("h_donation_unit_pub",
                                  &bkp[i].h_donation_unit_pub.hash),
      DONAU_JSON_pack_blinded_donation_identifier ("blinded_udi",
                                                   &bkp[i].blinded_udi));
    GNUNET_assert (0 ==
                   json_array_append_new (budikeypairs,
                                          budikeypair));
  }
  return GNUNET_JSON_PACK (
    GNUNET_JSON_pack_array_steal ("budikeypairs",
                                  budikeypairs),
    GNUNET_JSON_pack_data_auto ("charity_sig",
                                &charity_sig->eddsa_sig),
    GNUNET_JSON_pack_uint64 ("year",
                             year));
}


/**
 * Function called when we're done processing the
 * HTTP POST /batch-issue/$CHARITY_ID request.
 *
 * @param cls the `struct KeysRequest`
 * @param response_code HTTP response code, 0 on error
 * @param resp_obj parsed JSON result, NULL on error
 */
static void
handle_batch_issue_finished (void *cls,
                             long response_code,
                             const void *resp_obj)
{
  struct DONAU_BatchIssueReceiptHandle *birh = cls;
  const json_t *j = resp_obj;

  struct DONAU_BatchIssueResponse biresp = {
    .hr.reply = j,
    .hr.http_status = (unsigned int) response_code
  };

  birh->job = NULL;
  switch (response_code)
  {
  case MHD_HTTP_OK:
  case MHD_HTTP_CREATED:
    if (GNUNET_OK !=
        handle_batch_issue_ok (j,
                               birh,
                               &biresp))
    {
      biresp.hr.http_status = 0;
      biresp.hr.ec = TALER_EC_GENERIC_REPLY_MALFORMED;
    }
    break;
  case MHD_HTTP_NO_CONTENT:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // invalid charity signature
  case MHD_HTTP_FORBIDDEN:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // one or more donation units are not known to the Donau
  case MHD_HTTP_NOT_FOUND:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  case MHD_HTTP_CONTENT_TOO_LARGE:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // Donation limit is not sufficent
  case MHD_HTTP_CONFLICT:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // donation unit key is no longer valid
  case MHD_HTTP_GONE:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d for POST %s\n",
                (unsigned int) response_code,
                (int) biresp.hr.ec,
                birh->url);
    break;
  }
  if (NULL != birh->cb)
  {
    birh->cb (birh->cb_cls,
              &biresp);
    birh->cb = NULL;
  }
  DONAU_charity_issue_receipt_cancel (birh);
}


struct DONAU_BatchIssueReceiptHandle *
DONAU_charity_issue_receipt (
  struct GNUNET_CURL_Context *ctx,
  const char *url,
  const struct DONAU_CharityPrivateKeyP *charity_priv,
  const uint64_t charity_id,
  const uint64_t year,
  const size_t num_bkp,
  const struct DONAU_BlindedUniqueDonorIdentifierKeyPair *bkp,
  DONAU_BatchIssueReceiptsCallback cb,
  void *cb_cls)
{
  CURL *eh;
  json_t *body;
  char arg_str[sizeof (charity_id) * 2 + 32];
  struct DONAU_BatchIssueReceiptHandle *birh;

  birh = GNUNET_new (struct DONAU_BatchIssueReceiptHandle);
  birh->num_blinded_sigs = num_bkp;
  DONAU_charity_bkp_sign (num_bkp, bkp,
                          charity_priv,
                          &birh->charity_sig);
  birh->url = GNUNET_strdup (url);
  birh->cb = cb;
  birh->cb_cls = cb_cls;
  birh->ctx = ctx;
  GNUNET_snprintf (arg_str,
                   sizeof (arg_str),
                   "batch-issue/%llu",
                   (unsigned long long) charity_id);
  birh->url = TALER_url_join (url,
                              arg_str,
                              NULL);
  if (NULL == birh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (birh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "issue_receipts_with_URL `%s'.\n",
              birh->url);
  body = issue_receipt_body_to_json (num_bkp,
                                     bkp,
                                     year,
                                     &birh->charity_sig);
  eh = DONAU_curl_easy_get_ (birh->url);
  if ( (NULL == eh) ||
       (GNUNET_OK !=
        TALER_curl_easy_post (&birh->post_ctx,
                              eh,
                              body)) )
  {
    GNUNET_break (0);
    if (NULL != eh)
      curl_easy_cleanup (eh);
    json_decref (body);
    GNUNET_free (birh->url);
    return NULL;
  }
  json_decref (body);
  birh->job = GNUNET_CURL_job_add2 (ctx,
                                    eh,
                                    birh->post_ctx.headers,
                                    &handle_batch_issue_finished,
                                    birh);
  return birh;
}


void
DONAU_charity_issue_receipt_cancel (
  struct DONAU_BatchIssueReceiptHandle *birh)
{
  if (NULL != birh->job)
  {
    GNUNET_CURL_job_cancel (birh->job);
    birh->job = NULL;
  }
  TALER_curl_easy_post_finished (&birh->post_ctx);
  GNUNET_free (birh->url);
  GNUNET_free (birh);
}
