/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file lib/donau_api_batch_submit_receipts.c
 * @brief Implementation of the "handle" component of the donau's HTTP API
 * @author Lukas Matyja
 */
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>
#include "donau_service.h"
#include "donau_util.h"
#include "donau_api_curl_defaults.h"
#include "donau_json_lib.h"


/**
 * Handle for a POST /batch-submit request.
 */
struct DONAU_DonorReceiptsToStatementHandle
{
  /**
   * The url for the /batch-submit request.
   */
  char *url;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * Entry for this request with the `struct GNUNET_CURL_Context`.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  DONAU_DonorReceiptsToStatementResultCallback cb;

  /**
   * Closure to pass to @e cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};

/**
 * Transform submit receipt request into JSON.
 *
 * @param num_drs number of donation receipts in @drs
 * @param drs donation receipts array
 * @param year corresponding year
 * @param h_tax_id salted and hashed tax id
 */
json_t *
submit_request_body_to_json (const size_t num_drs,
                             const struct
                             DONAU_DonationReceipt drs[num_drs],
                             const uint64_t year,
                             const struct DONAU_HashDonorTaxId *h_tax_id)
{
  json_t *donation_receipts = json_array ();
  GNUNET_assert (NULL != donation_receipts);

  for (size_t i = 0; i < num_drs; i++)
  {
    json_t *receipt = GNUNET_JSON_PACK (
      GNUNET_JSON_pack_data_auto ("h_donation_unit_pub",
                                  &drs[i].h_donation_unit_pub),
      GNUNET_JSON_pack_data_auto ("nonce",
                                  &drs[i].nonce),
      DONAU_JSON_pack_donation_unit_sig ("donation_unit_sig",
                                         &drs[i].donation_unit_sig));
    GNUNET_assert (0 ==
                   json_array_append_new (donation_receipts,
                                          receipt));
  }
  return GNUNET_JSON_PACK (
    GNUNET_JSON_pack_array_steal ("donation_receipts",
                                  donation_receipts),
    GNUNET_JSON_pack_data_auto ("h_donor_tax_id",
                                h_tax_id),
    GNUNET_JSON_pack_uint64 ("donation_year",
                             year));
}


/**
 * Function called when we're done processing the
 * HTTP POST /batch-submit request.
 *
 * @param cls the `struct KeysRequest`
 * @param response_code HTTP response code, 0 on error
 * @param resp_obj parsed JSON result, NULL on error
 */
static void
handle_batch_submit_finished (void *cls,
                              long response_code,
                              const void *resp_obj)
{
  struct DONAU_DonorReceiptsToStatementHandle *birh = cls;
  const json_t *j = resp_obj;

  struct DONAU_DonorReceiptsToStatementResult biresp = {
    .hr.reply = j,
    .hr.http_status = (unsigned int) response_code
  };

  birh->job = NULL;
  switch (response_code)
  {
  case MHD_HTTP_OK:
    break;
  case MHD_HTTP_NO_CONTENT:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // One of the signatures is invalid.
  case MHD_HTTP_FORBIDDEN:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // At least one of the donation unit keys is not known to the Donau.
  case MHD_HTTP_NOT_FOUND:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // At least one of the corresponding private keys is deprecated/leaked.
  case MHD_HTTP_GONE:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  case MHD_HTTP_CONTENT_TOO_LARGE:
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    biresp.hr.ec = TALER_JSON_get_error_code (j);
    biresp.hr.hint = TALER_JSON_get_error_hint (j);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d for POST %s\n",
                (unsigned int) response_code,
                (int) biresp.hr.ec,
                birh->url);
    break;
  }
  if (NULL != birh->cb)
  {
    birh->cb (birh->cb_cls,
              &biresp);
    birh->cb = NULL;
  }
  DONAU_donor_receipts_to_statement_cancel (birh);
}


struct DONAU_DonorReceiptsToStatementHandle *
DONAU_donor_receipts_to_statement (
  struct GNUNET_CURL_Context *ctx,
  const char *url,
  const size_t num_drs,
  const struct DONAU_DonationReceipt drs[num_drs],
  const uint64_t year,
  const struct DONAU_HashDonorTaxId *h_tax_id,
  DONAU_DonorReceiptsToStatementResultCallback cb,
  void *cls)
{
  struct DONAU_DonorReceiptsToStatementHandle *birh;
  birh = GNUNET_new (struct DONAU_DonorReceiptsToStatementHandle);
  CURL *eh;
  json_t *body;

  TALER_LOG_DEBUG ("Connecting to the donau (%s)\n",
                   url);
  birh->url = GNUNET_strdup (url);
  birh->cb = cb;
  birh->cb_cls = cls;
  birh->ctx = ctx;
  birh->url = TALER_url_join (url,
                              "batch-submit",
                              NULL);
  if (NULL == birh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct request URL.\n");
    GNUNET_free (birh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "submit_receipts_with_URL `%s'.\n",
              birh->url);
  body = submit_request_body_to_json (num_drs, drs, year, h_tax_id);
  eh = DONAU_curl_easy_get_ (birh->url);
  if ( (NULL == eh) ||
       (GNUNET_OK !=
        TALER_curl_easy_post (&birh->post_ctx,
                              eh,
                              body)) )
  {
    GNUNET_break (0);
    if (NULL != eh)
      curl_easy_cleanup (eh);
    json_decref (body);
    GNUNET_free (birh->url);
    return NULL;
  }
  json_decref (body);
  birh->job = GNUNET_CURL_job_add2 (ctx,
                                    eh,
                                    birh->post_ctx.headers,
                                    &handle_batch_submit_finished,
                                    birh);
  return birh;
}


void
DONAU_donor_receipts_to_statement_cancel (
  struct DONAU_DonorReceiptsToStatementHandle *drsh)
{
  if (NULL != drsh->job)
  {
    GNUNET_CURL_job_cancel (drsh->job);
    drsh->job = NULL;
  }
  TALER_curl_easy_post_finished (&drsh->post_ctx);
  GNUNET_free (drsh->url);
  GNUNET_free (drsh);
}
