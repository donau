/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file lib/donau_api_charity_post.c
 * @brief Implementation of the "handle" component of the donau's HTTP API
 * @author Lukas Matyja
 */
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>
#include "donau_service.h"
#include "donau_api_curl_defaults.h"
#include "donau_json_lib.h"


/**
 * Handle for a POST /charities request.
 */
struct DONAU_CharityPostHandle
{
  /**
   * The url for the /charities request.
   */
  char *url;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * Entry for this request with the `struct GNUNET_CURL_Context`.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  DONAU_PostCharityResponseCallback cb;

  /**
   * Closure to pass to @e cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};

/**
 * Function called when we're done processing the
 * HTTP POST /charities request.
 *
 * @param cls the `struct KeysRequest`
 * @param response_code HTTP response code, 0 on error
 * @param resp_obj parsed JSON result, NULL on error
 */
static void
handle_charity_post_finished (void *cls,
                             long response_code,
                             const void *resp_obj)
{
  struct DONAU_CharityPostHandle *cph = cls;
  const json_t *j = resp_obj;

  struct DONAU_PostCharityResponse pcresp = {
    .hr.reply = j,
    .hr.http_status = (unsigned int) response_code
  };

  cph->job = NULL;
  switch (response_code)
  {
  case MHD_HTTP_CREATED:
	  struct GNUNET_JSON_Specification spec[] = {
	    GNUNET_JSON_spec_uint64 ("charity-id",
	    		&pcresp.details.ok.charity_id),
	    GNUNET_JSON_spec_end ()
	  };
	  if (GNUNET_OK !=
	      GNUNET_JSON_parse (j,
	                         spec,
	                         NULL,
	                         NULL))
	  {
		GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
					"Could not parse response from charity POST\n");
	    GNUNET_break_op (0);
	  }
    break;
  case MHD_HTTP_NO_CONTENT:
    pcresp.hr.ec = TALER_JSON_get_error_code (j);
    pcresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  case MHD_HTTP_FORBIDDEN:
    pcresp.hr.ec = TALER_JSON_get_error_code (j);
    pcresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  case MHD_HTTP_NOT_FOUND:
    pcresp.hr.ec = TALER_JSON_get_error_code (j);
    pcresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  case MHD_HTTP_CONTENT_TOO_LARGE:
    pcresp.hr.ec = TALER_JSON_get_error_code (j);
    pcresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    pcresp.hr.ec = TALER_JSON_get_error_code (j);
    pcresp.hr.hint = TALER_JSON_get_error_hint (j);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d for POST %s\n",
                (unsigned int) response_code,
                (int) pcresp.hr.ec,
                cph->url);
    break;
  }
  if (NULL != cph->cb)
  {
    cph->cb (cph->cb_cls,
             &pcresp);
    cph->cb = NULL;
  }
  DONAU_charity_post_cancel (cph);
}


struct DONAU_CharityPostHandle *
DONAU_charity_post (
  struct GNUNET_CURL_Context *ctx,
  const char *url,
  struct DONAU_CharityRequest *charity_req, // make it const
  const struct DONAU_BearerToken *bearer,
  DONAU_PostCharityResponseCallback cb,
  void *cb_cls)
{
  struct DONAU_CharityPostHandle *cph;
  CURL *eh;
  json_t *body;

  TALER_LOG_DEBUG ("Connecting to the donau (%s)\n",
                   url);
  cph = GNUNET_new (struct DONAU_CharityPostHandle);
  cph->url = GNUNET_strdup (url);
  cph->cb = cb;
  cph->cb_cls = cb_cls;
  cph->ctx = ctx;
  cph->url = TALER_url_join (url,
		  	  	  	  	  	 "charities",
                             NULL);
  if (NULL == cph->url)
  {
	GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
				"Could not construct request URL.\n");
    GNUNET_free (cph);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "POST a charity with URL `%s'.\n",
              cph->url);
  body = GNUNET_JSON_PACK (
	      GNUNET_JSON_pack_data_auto ("charity_pub",
	                                  &charity_req->charity_pub),
	      GNUNET_JSON_pack_string ("charity_url",
	    		  	  	  	  	  	 charity_req->charity_url),
	      GNUNET_JSON_pack_string ("charity_name",
	    		  	  	  	  	   charity_req->name),
	      TALER_JSON_pack_amount ("max_per_year",
	                              &charity_req->max_per_year),
	      TALER_JSON_pack_amount ("receipts_to_date",
	                              &charity_req->receipts_to_date), // really needed?
	      GNUNET_JSON_pack_uint64 ("current_year",
	    		  	  	  	  	  charity_req->current_year));
  eh = DONAU_curl_easy_get_ (cph->url);
  if ( (NULL == eh) ||
       (GNUNET_OK !=
        TALER_curl_easy_post (&cph->post_ctx,
                              eh,
                              body)) )
  {
    GNUNET_break (0);
    if (NULL != eh)
      curl_easy_cleanup (eh);
    json_decref (body);
    GNUNET_free (cph->url);
    return NULL;
  }
  json_decref (body);
  cph->job = GNUNET_CURL_job_add2 (ctx,
                                  eh,
								  cph->post_ctx.headers,
                                  &handle_charity_post_finished,
                                  cph);
  return cph;
}


void
DONAU_charity_post_cancel (
  struct DONAU_CharityPostHandle *cph)
{
  if (NULL != cph->job)
  {
    GNUNET_CURL_job_cancel (cph->job);
    cph->job = NULL;
  }
  TALER_curl_easy_post_finished (&cph->post_ctx);
  GNUNET_free (cph->url);
  GNUNET_free (cph);
}
