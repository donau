/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3, or (at your
  option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file lib/donau_api_csr_post.c
 * @brief Implementation of the "handle" component of the donau's HTTP API
 * @author Lukas Matyja
 */
#include <gnunet/gnunet_curl_lib.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_curl_lib.h>
#include "donau_service.h"
#include "donau_api_curl_defaults.h"
#include "donau_json_lib.h"


/**
 * Handle for a POST /csr-issue request.
 */
struct DONAU_CsRBatchIssueHandle
{
  /**
   * The url for the /csr-issue request.
   */
  char *url;

  /**
   * Minor context that holds body and headers.
   */
  struct TALER_CURL_PostContext post_ctx;

  /**
   * Entry for this request with the `struct GNUNET_CURL_Context`.
   */
  struct GNUNET_CURL_Job *job;

  /**
   * Function to call with the result.
   */
  DONAU_CsRBatchIssueCallback cb;

  /**
   * Closure to pass to @e cb.
   */
  void *cb_cls;

  /**
   * Reference to the execution context.
   */
  struct GNUNET_CURL_Context *ctx;

};

/**
 * Function called when we're done processing the
 * HTTP POST /csr-issue request.
 *
 * @param cls the `struct KeysRequest`
 * @param response_code HTTP response code, 0 on error
 * @param resp_obj parsed JSON result, NULL on error
 */
static void
handle_csr_issue_post_finished (void *cls,
                                long response_code,
                                const void *resp_obj)
{
  struct DONAU_CsRBatchIssueHandle *csrh = cls;
  const json_t *j = resp_obj;

  struct DONAU_CsRBatchIssueResponse csrresp = {
    .hr.reply = j,
    .hr.http_status = (unsigned int) response_code
  };

  csrh->job = NULL;
  switch (response_code)
  {
  case MHD_HTTP_CREATED:
    struct GNUNET_JSON_Specification spec[] = {
      TALER_JSON_spec_exchange_withdraw_values ( // FIXME: method for GNUNET
        "ewv",
        (struct TALER_ExchangeWithdrawValues *) &csrresp.details.ok.alg_values),
      GNUNET_JSON_spec_end ()
    };
    if (GNUNET_OK !=
        GNUNET_JSON_parse (j,
                           spec,
                           NULL,
                           NULL))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Could not parse response from csr POST\n");
      GNUNET_break_op (0);
    }
    csrh->cb (csrh->cb_cls,
              &csrresp);
    csrh->cb = NULL;
    break;
  // Donation unit was revoked.
  case MHD_HTTP_GONE:
    csrresp.hr.ec = TALER_JSON_get_error_code (j);
    csrresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  // Donation unit or endpoint not found.
  case MHD_HTTP_NOT_FOUND:
    csrresp.hr.ec = TALER_JSON_get_error_code (j);
    csrresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  case MHD_HTTP_BAD_REQUEST:
    csrresp.hr.ec = TALER_JSON_get_error_code (j);
    csrresp.hr.hint = TALER_JSON_get_error_hint (j);
    break;
  default:
    /* unexpected response code */
    GNUNET_break_op (0);
    csrresp.hr.ec = TALER_JSON_get_error_code (j);
    csrresp.hr.hint = TALER_JSON_get_error_hint (j);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Unexpected response code %u/%d for POST %s\n",
                (unsigned int) response_code,
                (int) csrresp.hr.ec,
                csrh->url);
    break;
  }
  if (NULL != csrh->cb)
  {
    csrh->cb (csrh->cb_cls,
              &csrresp);
    csrh->cb = NULL;
  }
  DONAU_csr_cancel (csrh);
}


struct DONAU_CsRBatchIssueHandle *
DONAU_csr_issue (
  struct GNUNET_CURL_Context *ctx,
  const char *url,
  const struct DONAU_DonationUnitPublicKey *pk,
  const struct GNUNET_CRYPTO_CsSessionNonce *nonce,
  DONAU_CsRBatchIssueCallback cb,
  void *cb_cls)
{
  struct DONAU_CsRBatchIssueHandle *csrh;
  CURL *eh;
  json_t *body;

  struct DONAU_DonationUnitHashP h_donation_unit_pub;
  DONAU_donation_unit_pub_hash (pk,
                                &h_donation_unit_pub);

  TALER_LOG_DEBUG ("Connecting to the donau (%s)\n",
                   url);
  csrh = GNUNET_new (struct DONAU_CsRBatchIssueHandle);
  csrh->url = GNUNET_strdup (url);
  csrh->cb = cb;
  csrh->cb_cls = cb_cls;
  csrh->ctx = ctx;
  csrh->url = TALER_url_join (url,
                              "csr-issue",
                              NULL);
  if (NULL == csrh->url)
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                "Could not construct requested URL.\n");
    GNUNET_free (csrh);
    return NULL;
  }
  GNUNET_log (GNUNET_ERROR_TYPE_DEBUG,
              "Request CS R with URL `%s'.\n",
              csrh->url);
  body = GNUNET_JSON_PACK (
    GNUNET_JSON_pack_data_varsize ("nonce",
                                   nonce,
                                   sizeof(*nonce)),
    GNUNET_JSON_pack_data_varsize ("du_pub_hash",
                                   &h_donation_unit_pub,
                                   sizeof(h_donation_unit_pub)));
  eh = DONAU_curl_easy_get_ (csrh->url);
  if ( (NULL == eh) ||
       (GNUNET_OK !=
        TALER_curl_easy_post (&csrh->post_ctx,
                              eh,
                              body)) )
  {
    GNUNET_break (0);
    if (NULL != eh)
      curl_easy_cleanup (eh);
    json_decref (body);
    GNUNET_free (csrh->url);
    return NULL;
  }
  json_decref (body);
  csrh->job = GNUNET_CURL_job_add2 (ctx,
                                    eh,
                                    csrh->post_ctx.headers,
                                    &handle_csr_issue_post_finished,
                                    csrh);
  return csrh;
}


void
DONAU_csr_cancel (
  struct DONAU_CsRBatchIssueHandle *csrh)
{
  if (NULL != csrh->job)
  {
    GNUNET_CURL_job_cancel (csrh->job);
    csrh->job = NULL;
  }
  TALER_curl_easy_post_finished (&csrh->post_ctx);
  GNUNET_free (csrh->url);
  GNUNET_free (csrh);
}
