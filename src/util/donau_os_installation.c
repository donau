/*
     This file is part of GNU Taler.
     Copyright (C) 2023 Taler Systems SA

     Taler is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     Taler is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Taler; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/
/**
 * @file donau_os_installation.c
 * @brief initialize libgnunet OS subsystem for Donau.
 * @author Christian Grothoff
 * @author Lukas Matyja
 */
#include "donau_config.h"
#include <taler/taler_util.h>
#include <gnunet/gnunet_util_lib.h>


/**
 * Default project data used for installation path detection
 * for Donau of GNU Taler. //von libgnunetutil
 */
static const struct GNUNET_OS_ProjectData donau_pd = {
  .libname = "libdonauutil",
  .project_dirname = "donau",
  .binary_name = "donau-httpd",
  .env_varname = "DONAU_PREFIX",
  .base_config_varname = "TALER_BASE_CONFIG",
  .bug_email = "taler@gnu.org",
  .homepage = "http://www.gnu.org/s/taler/",
  .config_file = "donau.conf",
  .user_config_file = "~/.config/donau.conf",
  .version = PACKAGE_VERSION "-" VCS_VERSION,
  .is_gnu = 1,
  .gettext_domain = "donau",
  .gettext_path = NULL,
};


/**
 * Return default project data used by Donau.
 */
const struct GNUNET_OS_ProjectData *
DONAU_project_data (void)
{
  return &donau_pd;
}


/* end of donau_os_installation.c */
