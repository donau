/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file qr.c
 * @brief Utility functions for donation statement QR Code
 * @author Lukas Matyja
 */
#include "donau_config.h"
#include <taler/taler_util.h>
#include "donau_util.h"
#include "donau_service.h"

// FIXME: remove public key, only needed for testing.
// The verification app should get the public key directly from the Donau.
char *
generate_QR_string (const struct DONAU_DonauPublicKeyP *pub_key,
                    const struct DONAU_DonationStatement *donation_statement)
{
  /* The string will be structured as follows: YEAR/TOTALAMOUNT/TAXID/TAXIDSALT/ED25519SIGNATURE/PUBKEY */
  uint64_t total_size = 0;

  char *total_amount_string = TALER_amount_to_string (
    &donation_statement->total_amount);
  total_size += strlen (total_amount_string);
  total_size += strlen (donation_statement->donor_tax_id);
  total_size += strlen (donation_statement->salt);

  char *end_sig;
  total_size += sizeof (struct DONAU_DonauSignatureP) * 2;
  char sig_str[sizeof (struct DONAU_DonauSignatureP) * 2];
  end_sig = GNUNET_STRINGS_data_to_string (
    &donation_statement->donation_statement_sig,
    sizeof (struct DONAU_DonauSignatureP),
    sig_str,
    sizeof (sig_str));
  *end_sig = '\0';

  char *end_pub;
  total_size += sizeof (struct DONAU_DonauPublicKeyP) * 2;
  char pub_str[sizeof (struct DONAU_DonauPublicKeyP) * 2];
  end_pub = GNUNET_STRINGS_data_to_string (
    pub_key,
    sizeof (struct DONAU_DonauPublicKeyP),
    pub_str,
    sizeof (pub_str));
  *end_pub = '\0';

  char *qr_string = GNUNET_malloc (total_size + 1);
  GNUNET_assert (0 <= GNUNET_snprintf (qr_string,
                                       total_size,
                                       "%llu/%s/%s/%s/%s/%s",
                                       (unsigned long long)
                                       donation_statement->year,
                                       total_amount_string,
                                       donation_statement->donor_tax_id,
                                       donation_statement->salt,
                                       sig_str,
                                       pub_str));

  return qr_string;
}
