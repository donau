/*
  This file is part of TALER
  Copyright (C) 2021, 2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau_signatures.c
 * @brief Utility functions for Taler donau security module signatures
 * @author Christian Grothoff
 * @author Lukas Matyja
 */
#include "donau_config.h"
#include <taler/taler_util.h>
#include <taler/taler_signatures.h>
#include "donau_util.h"
#include "donau_signatures.h"

GNUNET_NETWORK_STRUCT_BEGIN

/**
 * @brief Format used to generate the signature/donation statement
 * over the total amount and a donor identifier of a year.
 */
struct DONAU_DonationStatementConfirmationPS
{
  /**
   * Purpose must be #DONAU_SIGNATURE_DONAU_DONATION_STATEMENT. Signed
   * by a `struct DONAU_DonauPublicKeyP` using EdDSA.
   */
  struct GNUNET_CRYPTO_EccSignaturePurpose purpose;

  /**
   * Total amount donated of a specific @a year.
   */
  struct TALER_AmountNBO amount_tot;

  /**
   * The hash of the identifier of the donor.
   */
  struct DONAU_HashDonorTaxId i;

  /**
   * The corresponding year.
   */
  uint32_t year;

};

GNUNET_NETWORK_STRUCT_END


enum TALER_ErrorCode
DONAU_donation_statement_sign (
  DONAU_DonauSignCallback scb,
  const struct TALER_Amount *amount_tot,
  const uint64_t year,
  const struct DONAU_HashDonorTaxId *i,
  struct DONAU_DonauPublicKeyP *donau_pub,
  struct DONAU_DonauSignatureP *donau_sig)
{
  struct DONAU_DonationStatementConfirmationPS confirm = {
    .purpose.purpose = htonl (DONAU_SIGNATURE_DONAU_DONATION_STATEMENT),
    .purpose.size = htonl (sizeof (confirm)),
    .year = htonl (year),
    .i = *i
  };

  TALER_amount_hton (&confirm.amount_tot,
                     amount_tot);

  return scb (&confirm.purpose,
              donau_pub,
              donau_sig);
}


enum GNUNET_GenericReturnValue
DONAU_donation_statement_verify (
  const struct TALER_Amount *amount_tot,
  const uint64_t year,
  const struct DONAU_HashDonorTaxId *i,
  const struct DONAU_DonauPublicKeyP *donau_pub,
  const struct DONAU_DonauSignatureP *statement_sig)
{
  struct DONAU_DonationStatementConfirmationPS confirm = {
    .purpose.purpose = htonl (DONAU_SIGNATURE_DONAU_DONATION_STATEMENT),
    .purpose.size = htonl (sizeof (confirm)),
    .year = htonl (year),
    .i = *i
  };

  TALER_amount_hton (&confirm.amount_tot,
                     amount_tot);

  return
    GNUNET_CRYPTO_eddsa_verify (DONAU_SIGNATURE_DONAU_DONATION_STATEMENT,
                                &confirm,
                                &statement_sig->eddsa_sig,
                                &donau_pub->eddsa_pub);
}


/* end of donau_signatures.c */
