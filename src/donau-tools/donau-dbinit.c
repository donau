/*
  This file is part of TALER
  Copyright (C) 2014-2022 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-tools/donau-dbinit.c
 * @brief Create tables for the donau database.
 * @author Florian Dold
 * @author Christian Grothoff
 */
#include "donau_config.h"
#include <gnunet/gnunet_util_lib.h>
#include "donaudb_lib.h"
#include "donau_util.h"


/* LSB-style exit status codes */
#ifndef EXIT_INVALIDARGUMENT
/**
 * Command-line arguments are invalid.
 * Restarting useless.
 */
#define EXIT_INVALIDARGUMENT 2
#endif

#ifndef EXIT_NOPERMISSION
/**
 * Permissions needed to run are not available.
 * Restarting useless.
 */
#define EXIT_NOPERMISSION 4
#endif

#ifndef EXIT_NOTINSTALLED
/**
 * Key resources are not installed.
 * Restarting useless.
 */
#define EXIT_NOTINSTALLED 5
#endif


/**
 * Return value from main().
 */
static int global_ret;

/**
 * -r option: do full DB reset
 */
static int reset_db;

/**
 * Main function that will be run.
 *
 * @param cls closure
 * @param args remaining command-line arguments
 * @param cfgfile name of the configuration file used (for saving, can be NULL!)
 * @param cfg configuration
 */
static void
run (void *cls,
     char *const *args,
     const char *cfgfile,
     const struct GNUNET_CONFIGURATION_Handle *cfg)
{
  struct DONAUDB_Plugin *plugin;

  (void) cls;
  (void) args;
  (void) cfgfile;
  if (NULL ==
      (plugin = DONAUDB_plugin_load (cfg)))
  {
    fprintf (stderr,
             "Failed to initialize database plugin.\n");
    global_ret = EXIT_NOTINSTALLED;
    return;
  }

  if (reset_db)
  {
    if (GNUNET_OK !=
        plugin->drop_tables (plugin->cls))
    {
      GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                  "Could not drop tables as requested. Either database was not yet initialized, or permission denied. Consult the logs. Will still try to create new tables.\n");
    }
  }

  if (GNUNET_OK !=
      plugin->create_tables (plugin->cls))
  {
    fprintf (stderr,
             "Failed to initialize database.\n");
    DONAUDB_plugin_unload (plugin);
    plugin = NULL;
    global_ret = EXIT_NOPERMISSION;
    return;
  }

  DONAUDB_plugin_unload (plugin);
  plugin = NULL;
}


/**
 * The main function of the database initialization tool.
 * Used to initialize the Taler Donau's database.
 *
 * @param argc number of arguments from the command line
 * @param argv command line arguments
 * @return 0 ok, non-zero on error
 */
int
main (int argc,
      char *const *argv)
{
  const struct GNUNET_GETOPT_CommandLineOption options[] = {
    GNUNET_GETOPT_option_flag ('r',
                               "reset",
                               "reset database (DANGEROUS: all existing data is lost!)",
                               &reset_db),
    GNUNET_GETOPT_OPTION_END
  };
  enum GNUNET_GenericReturnValue ret;

  ret = GNUNET_PROGRAM_run (
    DONAU_project_data (),
    argc, argv,
    "donau-dbinit",
    gettext_noop ("Initialize Donau database"),
    options,
    &run, NULL);
  if (GNUNET_SYSERR == ret)
    return EXIT_INVALIDARGUMENT;
  if (GNUNET_NO == ret)
    return EXIT_SUCCESS;
  return global_ret;
}


/* end of donau-dbinit.c */
