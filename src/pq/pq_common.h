/*
  This file is part of TALER
  Copyright (C) 2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file pq/pq_common.h
 * @brief common defines for the pq functions
 * @author Johannes Casaburi
 */
#ifndef DONAU_PQ_COMMON_H_
#define DONAU_PQ_COMMON_H_

#include "taler/taler_util.h"

/**
 * Internal types that are supported as TALER-exchange-specific array types.
 *
 * To support a new type,
 *   1. add a new entry into this list,
 *   2. for query-support, implement the size calculation and memory copying in
 *      qconv_array() accordingly, in pq_query_helper.c
 *   3. provide a query-API for arrays of the type, by calling
 *      query_param_array_generic with the appropriate parameters,
 *      in pq_query_helper.c
 *   4. for result-support, implement memory copying by adding another case
 *      to extract_array_generic, in pq_result_helper.c
 *   5. provide a result-spec-API for arrays of the type,
 *      in pq_result_helper.c
 *   6. expose the API's in taler_pq_lib.h
 */
enum DONAU_PQ_ArrayType
{
  DONAU_PQ_array_of_blinded_du_sig,
  DONAU_PQ_array_of_unblinded_du_sig,
};


#endif  /* DONAU_PQ_COMMON_H_ */
/* end of pg/pq_common.h */
