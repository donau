/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file json/json_pack.c
 * @brief helper functions for JSON object packing
 * @author Christian Grothoff
 * @author Lukas Matyja
 */
#include "donau_config.h"
#include <gnunet/gnunet_util_lib.h>
#include <taler/taler_util.h>
#include <taler/taler_json_lib.h>
#include <donau_util.h>

struct GNUNET_JSON_PackSpec
DONAU_JSON_pack_donation_unit_pub (
  const char *name,
  const struct DONAU_DonationUnitPublicKey *pk)
{
  const struct GNUNET_CRYPTO_BlindSignPublicKey *bsp;
  struct GNUNET_JSON_PackSpec ps = {
    .field_name = name,
  };

  if (NULL == pk)
    return ps;
  bsp = pk->bsign_pub_key;
  switch (bsp->cipher)
  {
  case GNUNET_CRYPTO_BSA_INVALID:
    break;
  case GNUNET_CRYPTO_BSA_RSA:
    ps.object
      = GNUNET_JSON_PACK (
          GNUNET_JSON_pack_string ("cipher",
                                   "RSA"),
          GNUNET_JSON_pack_rsa_public_key ("rsa_public_key",
                                           bsp->details.rsa_public_key));
    return ps;
  case GNUNET_CRYPTO_BSA_CS:
    ps.object
      = GNUNET_JSON_PACK (
          GNUNET_JSON_pack_string ("cipher",
                                   "CS"),
          GNUNET_JSON_pack_data_varsize ("cs_public_key",
                                         &bsp->details.cs_public_key,
                                         sizeof (bsp->details.cs_public_key)));
    return ps;
  }
  GNUNET_assert (0);
  return ps;
}


/* End of json/json_pack.c */
