#!/bin/bash
#
# This file is part of TALER
# Copyright (C) 2015-2020, 2023 Taler Systems SA
#
#  TALER is free software; you can redistribute it and/or modify it under the
#  terms of the GNU Affero General Public License as published by the Free Software
#  Foundation; either version 3, or (at your option) any later version.
#
#  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
#  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License along with
#  TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
#
#
# This script uses 'curl' to POST various ill-formed requests to the
# donau-httpd.  Basically, the goal is to make sure that the
# HTTP server survives (and produces the 'correct' error code).
#
#
# Clear environment from variables that override config.
unset XDG_DATA_HOME
unset XDG_CONFIG_HOME

set -eu
#
echo -n "Launching donau ..."

. setup.sh

# Setup database
setup -c "test_donau_httpd.conf" \
      -D \
 
# Finally run test...
echo -n "Running tests ..."
# We read the JSON snippets to GET from test_donau_httpd.get
cat test_donau_httpd.get | grep -v ^\# | awk '{ print "curl http://localhost:8089" $1 }' | bash &> /dev/null
echo -n .
# Also try them with various headers: Language
cat test_donau_httpd.get | grep -v ^\# | awk '{ print "curl -H \"Accept-Language: fr,en;q=0.4,de\" http://localhost:8089" $1 }' | bash &> /dev/null
echo -n .
# Also try them with various headers: Accept encoding (wildcard #1)
cat test_donau_httpd.get | grep -v ^\# | awk '{ print "curl -H \"Accept: text/*\" http://localhost:8089" $1 }' | bash &> /dev/null
echo -n .
# Also try them with various headers: Accept encoding (wildcard #2)
cat test_donau_httpd.get | grep -v ^\# | awk '{ print "curl -H \"Accept: */plain\" http://localhost:8089" $1 }' | bash &> /dev/null

echo " DONE"
exit 0

