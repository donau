/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_charity.h
 * @brief Handle /charity requests
 * @author Johannes Casaburi
 */
#ifndef DONAU_HTTPD_CHARITY_H
#define DONAU_HTTPD_CHARITY_H

#include <microhttpd.h>
#include "donau-httpd.h"
#include "donaudb_plugin.h"


/**
 * Handle a POST "/charity" request.
 *
 * @param connection the MHD connection to handle
 * @param root uploaded JSON data
 * @return MHD result code
 */
MHD_RESULT
DH_handler_charity_post (
  struct DH_RequestContext *rc,
  const json_t *root,
  const char *const args[]);


/**
 * Handle a GET "/charities/$CHARITY_ID" request.
 *
 * @param rc request context
 * @param args GET arguments (should be one)
 * @return MHD result code
 */
MHD_RESULT
DH_handler_charity_get (
  struct DH_RequestContext *rc,
  const char *const args[1]);


/**
 * Handle a GET "/charities" request.
 *
 * @param rc request context
 * @param args GET arguments (should be one)
 * @return MHD result code
 */
MHD_RESULT
DH_handler_charities_get (
  struct DH_RequestContext *rc);

/**
 * Handle a DELETE "/charity/$CHARITY_ID" request.
 *
 * @param rc request details about the request to handle
 * @param args argument with the public key of the purse
 * @return MHD result code
 */
MHD_RESULT
DH_handler_charity_delete (
  struct DH_RequestContext *rc,
  const char *const args[1]);

#endif
