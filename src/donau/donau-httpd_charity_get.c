/*
  This file is part of TALER
  Copyright (C) 2023-2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_charity_get.c
 * @brief Return summary information about a charity
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <gnunet/gnunet_util_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include <pthread.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_mhd_lib.h>
#include <taler/taler_signatures.h>
#include "donaudb_plugin.h"
#include "donau-httpd_charity.h"


/**
 * Maximum number of records we return per request.
 */
#define MAX_RECORDS 1024

MHD_RESULT
DH_handler_charity_get (
  struct DH_RequestContext *rc,
  const char *const args[1])
{
  unsigned long long charity_id;
  char dummy;

  if ( (NULL == args[0]) ||
       (1 != sscanf (args[0],
                     "%llu%c",
                     &charity_id,
                     &dummy)) )
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (rc->connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "charity_id");
  }

  {
    struct DONAUDB_CharityMetaData meta;
    enum GNUNET_DB_QueryStatus qs;
    MHD_RESULT result;

    qs = DH_plugin->lookup_charity (DH_plugin->cls,
                                    (uint64_t) charity_id,
                                    &meta);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (rc->connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         NULL);
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      return TALER_MHD_reply_static (
        rc->connection,
        MHD_HTTP_NO_CONTENT,
        NULL,
        NULL,
        0);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      break;
    }

    result = TALER_MHD_REPLY_JSON_PACK (
      rc->connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_data_auto ("charity_pub",
                                  &meta.charity_pub),
      GNUNET_JSON_pack_string ("url",
                               meta.charity_url),
      GNUNET_JSON_pack_string ("name",
                               meta.charity_name),
      TALER_JSON_pack_amount ("max_per_year",
                              &meta.max_per_year),
      TALER_JSON_pack_amount ("receipts_to_date",
                              &meta.receipts_to_date),
      GNUNET_JSON_pack_uint64 ("current_year",
                               meta.current_year));

    GNUNET_free (meta.charity_url);
    GNUNET_free (meta.charity_name);
    return result;
  }
}


/* end of donau-httpd_charity_get.c */
