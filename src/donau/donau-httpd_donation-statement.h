/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_donation-statement.h
 * @brief Handle /donation-statement requests
 * @author Johannes Casaburi
 */
#ifndef DONAU_HTTPD_DONATION_STATEMENT_H
#define DONAU_HTTPD_DONATION_STATEMENT_H

#include <microhttpd.h>
#include "donau-httpd.h"
#include "donaudb_plugin.h"


/**
 * Handle a GET "/charities/$YEAR/$H_DONOR_TAX_ID" request.
 *
 * @param rc request context
 * @param args GET arguments (should be two)
 * @return MHD result code
 */
MHD_RESULT
DH_handler_donation_statement_get (
  struct DH_RequestContext *rc,
  const char *const args[2]);

#endif
