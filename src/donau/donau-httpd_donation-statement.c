/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_donation-statement_get.c
 * @brief Return donation statement
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <gnunet/gnunet_util_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include <pthread.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_mhd_lib.h>
#include <taler/taler_signatures.h>
#include "donaudb_plugin.h"
#include "donau-httpd_keys.h"
#include "donau-httpd_donation-statement.h"


MHD_RESULT
DH_handler_donation_statement_get (
  struct DH_RequestContext *rc,
  const char *const args[2])
{
  unsigned long long donation_year;
  struct DONAU_HashDonorTaxId h_donor_tax_id;
  char dummy;

  if ( (NULL == args[0]) ||
       (1 != sscanf (args[0],
                     "%llu%c",
                     &donation_year,
                     &dummy)) )
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (rc->connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "donation_year");
  }

  if (GNUNET_OK !=
      GNUNET_STRINGS_string_to_data (args[1],
                                     strlen (args[1]),
                                     &h_donor_tax_id,
                                     sizeof (h_donor_tax_id)))
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (rc->connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_PARAMETER_MALFORMED,
                                       "h_donor_tax_id");
  }

  {
    struct TALER_Amount total_donations;
    struct DONAU_DonauPublicKeyP donau_pub;
    struct DONAU_DonauSignatureP donau_sig;
    enum GNUNET_DB_QueryStatus qs;
    MHD_RESULT result;

    qs = DH_plugin->iterate_submitted_receipts (DH_plugin->cls,
                                                (uint64_t) donation_year,
                                                &h_donor_tax_id,
                                                &total_donations);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
    case GNUNET_DB_STATUS_SOFT_ERROR:
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (rc->connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         NULL);
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      return TALER_MHD_reply_static (
        rc->connection,
        MHD_HTTP_NO_CONTENT,
        NULL,
        NULL,
        0);
      break;
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      enum TALER_ErrorCode ec;
      ec = DONAU_donation_statement_sign (
        &DH_keys_donau_sign_,
        &total_donations,
        donation_year,
        &h_donor_tax_id,
        &donau_pub,
        &donau_sig);

      if (TALER_EC_NONE != ec)
      {
        GNUNET_break (0);
        return TALER_MHD_reply_with_error (rc->connection,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           ec,
                                           NULL);
      }
      break;
    }

    result = TALER_MHD_REPLY_JSON_PACK (
      rc->connection,
      MHD_HTTP_OK,
      TALER_JSON_pack_amount ("total", &total_donations),
      GNUNET_JSON_pack_data_auto ("donation_statement_sig",
                                  &donau_sig),
      GNUNET_JSON_pack_data_auto ("donau_pub", &donau_pub));

    return result;
  }
}


/* end of donau-httpd_donation-statement.c */
