/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau/donau-httpd_db.h
 * @brief High-level (transactional-layer) database operations for the donau
 * @author Johannes Casaburi
 */
#ifndef DONAU_HTTPD_DB_H
#define DONAU_HTTPD_DB_H

#include <microhttpd.h>
#include <gnunet/gnunet_mhd_compat.h>


/**
 * How often should we retry a transaction before giving up
 * (for transactions resulting in serialization/dead locks only).
 *
 * The current value is likely too high for production. We might want to
 * benchmark good values once we have a good database setup.  The code is
 * expected to work correctly with any positive value, albeit inefficiently if
 * we too aggressively force clients to retry the HTTP request merely because
 * we have database serialization issues.
 */
#define MAX_TRANSACTION_COMMIT_RETRIES 100


/**
 * Function implementing a database transaction.  Runs the transaction
 * logic; IF it returns a non-error code, the transaction logic MUST
 * NOT queue a MHD response.  IF it returns an hard error, the
 * transaction logic MUST queue a MHD response and set @a mhd_ret.  IF
 * it returns the soft error code, the function MAY be called again to
 * retry and MUST not queue a MHD response.
 *
 * @param cls closure
 * @param connection MHD request which triggered the transaction
 * @param[out] mhd_ret set to MHD response status for @a connection,
 *             if transaction failed (!)
 * @return transaction status
 */
typedef enum GNUNET_DB_QueryStatus
(*DH_DB_TransactionCallback)(void *cls,
                             struct MHD_Connection *connection,
                             MHD_RESULT *mhd_ret);


/**
 * Run a database transaction for @a connection.
 * Starts a transaction and calls @a cb.  Upon success,
 * attempts to commit the transaction.  Upon soft failures,
 * retries @a cb a few times.  Upon hard or persistent soft
 * errors, generates an error message for @a connection.
 *
 * @param connection MHD connection to run @a cb for, can be NULL
 * @param name name of the transaction (for debugging)
 * @param[out] mhd_ret set to MHD response code, if transaction failed (returned #GNUNET_SYSERR);
 *             NULL if we are not running with a @a connection and thus
 *             must not queue MHD replies
 * @param cb callback implementing transaction logic
 * @param cb_cls closure for @a cb, must be read-only!
 * @return #GNUNET_OK on success, #GNUNET_SYSERR on failure
 */
enum GNUNET_GenericReturnValue
DH_DB_run_transaction (struct MHD_Connection *connection,
                       const char *name,
                       MHD_RESULT *mhd_ret,
                       DH_DB_TransactionCallback cb,
                       void *cb_cls);


#endif
/* DONAU_HTTPD_DB_H */
