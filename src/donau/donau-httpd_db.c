/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_db.c
 * @brief Generic database operations for the donau.
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <pthread.h>
#include <jansson.h>
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_mhd_lib.h>
#include "donaudb_lib.h"
#include "donau-httpd_db.h"
#include "donau-httpd.h"


enum GNUNET_GenericReturnValue
DH_DB_run_transaction (struct MHD_Connection *connection,
                       const char *name,
                       MHD_RESULT *mhd_ret,
                       DH_DB_TransactionCallback cb,
                       void *cb_cls)
{
  if (NULL != mhd_ret)
    *mhd_ret = -1; /* set to invalid value, to help detect bugs */
  if (GNUNET_OK !=
      DH_plugin->preflight (DH_plugin->cls))
  {
    GNUNET_break (0);
    if (NULL != mhd_ret)
      *mhd_ret = TALER_MHD_reply_with_error (connection,
                                             MHD_HTTP_INTERNAL_SERVER_ERROR,
                                             TALER_EC_GENERIC_DB_SETUP_FAILED,
                                             NULL);
    return GNUNET_SYSERR;
  }
  for (unsigned int retries = 0;
       retries < MAX_TRANSACTION_COMMIT_RETRIES;
       retries++)
  {
    enum GNUNET_DB_QueryStatus qs;

    if (GNUNET_OK !=
        DH_plugin->start (DH_plugin->cls,
                          name))
    {
      GNUNET_break (0);
      if (NULL != mhd_ret)
        *mhd_ret = TALER_MHD_reply_with_error (connection,
                                               MHD_HTTP_INTERNAL_SERVER_ERROR,
                                               TALER_EC_GENERIC_DB_START_FAILED,
                                               NULL);
      return GNUNET_SYSERR;
    }
    qs = cb (cb_cls,
             connection,
             mhd_ret);
    if (0 > qs)
    {
      DH_plugin->rollback (DH_plugin->cls);
      if (GNUNET_DB_STATUS_HARD_ERROR == qs)
        return GNUNET_SYSERR;
    }
    else
    {
      qs = DH_plugin->commit (DH_plugin->cls);
      if (GNUNET_DB_STATUS_HARD_ERROR == qs)
      {
        DH_plugin->rollback (DH_plugin->cls);
        if (NULL != mhd_ret)
          *mhd_ret = TALER_MHD_reply_with_error (connection,
                                                 MHD_HTTP_INTERNAL_SERVER_ERROR,
                                                 TALER_EC_GENERIC_DB_COMMIT_FAILED,
                                                 NULL);
        return GNUNET_SYSERR;
      }
      if (0 > qs)
        DH_plugin->rollback (DH_plugin->cls);
    }
    /* make sure callback did not violate invariants! */
    GNUNET_assert ( (NULL == mhd_ret) ||
                    (-1 == (int) *mhd_ret) );
    if (0 <= qs)
      return GNUNET_OK;
  }
  DH_plugin->rollback (DH_plugin->cls);
  TALER_LOG_ERROR ("Transaction `%s' commit failed %u times\n",
                   name,
                   MAX_TRANSACTION_COMMIT_RETRIES);
  if (NULL != mhd_ret)
    *mhd_ret = TALER_MHD_reply_with_error (connection,
                                           MHD_HTTP_INTERNAL_SERVER_ERROR,
                                           TALER_EC_GENERIC_DB_SOFT_FAILURE,
                                           NULL);
  return GNUNET_SYSERR;
}


/* end of donau-httpd_db.c */
