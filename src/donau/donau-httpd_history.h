/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_history.h
 * @brief Handle /history requests
 * @author Johannes Casaburi
 */
#ifndef DONAU_HTTPD_HISTORY_H
#define DONAU_HTTPD_HISTORY_H

#include <microhttpd.h>
#include "donau-httpd.h"

/**
 * Handle a GET "/history" request.
 *
 * @param rc request context
 * @param args GET arguments (should be one)
 * @return MHD result code
 */
MHD_RESULT
DH_handler_history_get (
  struct DH_RequestContext *rc,
  const char *const args[]);

/**
 * Handle a GET "/history/$charity_id" request.
 *
 * @param rc request context
 * @param args GET arguments (should be one)
 * @return MHD result code
 */
MHD_RESULT
DH_handler_history_entry_get (
  struct DH_RequestContext *rc,
  const char *const args[]);

#endif
