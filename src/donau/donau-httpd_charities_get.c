/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_charities_get.c
 * @brief Return charities
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <gnunet/gnunet_util_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_mhd_lib.h>
#include <taler/taler_signatures.h>
#include "donau-httpd.h"
#include "donaudb_plugin.h"
#include "donau-httpd_charity.h"


/**
 * Maximum number of charities we return per request.
 */
#define MAX_RECORDS 1024

/**
 * Return charities information.
 *
 * @param cls closure
 */
static enum GNUNET_GenericReturnValue
charities_cb (
  void *cls,
  const struct DONAU_CharityPublicKeyP charity_pub,
  const char *charity_name,
  const char *charity_url,
  struct TALER_Amount max_per_year,
  struct TALER_Amount receipts_to_date,
  uint64_t current_year)
{
  json_t *charities = cls;

  GNUNET_assert (
    0 ==
    json_array_append (
      charities,
      GNUNET_JSON_PACK (
        GNUNET_JSON_pack_data_auto ("charity_pub",
                                    &charity_pub),
        GNUNET_JSON_pack_string ("url",
                                 charity_url),
        GNUNET_JSON_pack_string ("name",
                                 charity_name),
        TALER_JSON_pack_amount ("max_per_year",
                                &max_per_year),
        TALER_JSON_pack_amount ("receipts_to_date",
                                &receipts_to_date),
        GNUNET_JSON_pack_int64 ("current_year",
                                current_year))));
  return GNUNET_OK;
}


MHD_RESULT
DH_handler_charities_get (
  struct DH_RequestContext *rc)
{

  {
    json_t *charities;
    enum GNUNET_DB_QueryStatus qs;

    charities = json_array ();
    GNUNET_assert (NULL != charities);
    qs = DH_plugin->get_charities (DH_plugin->cls,
                                   &charities_cb,
                                   charities);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
    case GNUNET_DB_STATUS_SOFT_ERROR:
      json_decref (charities);
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (rc->connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         NULL);
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      return TALER_MHD_reply_static (
        rc->connection,
        MHD_HTTP_NO_CONTENT,
        NULL,
        NULL,
        0);
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      break;
    }
    return TALER_MHD_REPLY_JSON_PACK (
      rc->connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_array_steal ("charities",
                                    charities));
  }
}


/* end of donau-httpd_charities_get.c */
