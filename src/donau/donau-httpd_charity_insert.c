/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_charity_insert.c
 * @brief Handle request to insert a charity.
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_json_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include <pthread.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_mhd_lib.h>
#include <taler/taler_signatures.h>
#include "donaudb_plugin.h"
#include "donau-httpd_charity.h"
#include "donau-httpd_db.h"


/**
 * Closure for #insert_charity()
 */
struct InsertCharityContext
{
  struct DONAU_CharityPublicKeyP charity_pub;
  const char *charity_name;
  const char *charity_url;
  struct TALER_Amount max_per_year;
  struct TALER_Amount receipts_to_date;
  uint64_t current_year;
  uint64_t charity_id;
};


/**
 * Function implementing insert charity transaction.
 *
 * Runs the transaction logic; IF it returns a non-error code, the
 * transaction logic MUST NOT queue a MHD response.  IF it returns an hard
 * error, the transaction logic MUST queue a MHD response and set @a mhd_ret.
 * IF it returns the soft error code, the function MAY be called again to
 * retry and MUST not queue a MHD response.
 *
 * @param cls closure with a `struct InsertCharityContext`
 * @param connection MHD request which triggered the transaction
 * @param[out] mhd_ret set to MHD response status for @a connection,
 *             if transaction failed (!)
 * @return transaction status
 */
static enum GNUNET_DB_QueryStatus
insert_charity (void *cls,
                struct MHD_Connection *connection,
                MHD_RESULT *mhd_ret)
{
  struct InsertCharityContext *icc = cls;
  enum GNUNET_DB_QueryStatus qs;

  qs = DH_plugin->insert_charity (DH_plugin->cls,
                                  &icc->charity_pub,
                                  icc->charity_name,
                                  icc->charity_url,
                                  &icc->max_per_year,
                                  &icc->receipts_to_date,
                                  &icc->current_year,
                                  &icc->charity_id);
  if (qs <= 0)
  {
    if (GNUNET_DB_STATUS_SOFT_ERROR != qs)
    {
      GNUNET_break (0);
      *mhd_ret = TALER_MHD_reply_with_error (connection,
                                             MHD_HTTP_INTERNAL_SERVER_ERROR,
                                             TALER_EC_GENERIC_DB_STORE_FAILED,
                                             "insert_charity");
      return GNUNET_DB_STATUS_HARD_ERROR;
    }
    return qs;
  }

  return GNUNET_DB_STATUS_SUCCESS_ONE_RESULT;
}


MHD_RESULT
DH_handler_charity_post (struct DH_RequestContext *rc,
                         const json_t *root,
                         const char *const args[])
{
  struct InsertCharityContext icc;

  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_fixed_auto ("charity_pub",
                                 &icc.charity_pub),
    GNUNET_JSON_spec_string ("charity_name",
                             &icc.charity_name),
    GNUNET_JSON_spec_string ("charity_url",
                             &icc.charity_url),
    TALER_JSON_spec_amount ("max_per_year",
                            DH_currency,
                            &icc.max_per_year),
    TALER_JSON_spec_amount ("receipts_to_date",
                            DH_currency,
                            &icc.receipts_to_date),
    GNUNET_JSON_spec_uint64 ("current_year",
                             &icc.current_year),
    GNUNET_JSON_spec_end ()
  };

  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (rc->connection,
                                     root,
                                     spec);
    if (GNUNET_SYSERR == res)
      return MHD_NO; /* hard failure */
    if (GNUNET_NO == res)
    {
      GNUNET_break_op (0);
      return MHD_YES; /* failure */
    }
  }

  {
    MHD_RESULT mhd_ret;

    if (GNUNET_OK !=
        DH_DB_run_transaction (rc->connection,
                               "insert_charity",
                               &mhd_ret,
                               &insert_charity,
                               &icc))
    {
      return mhd_ret;
    }
  }

  GNUNET_log (GNUNET_ERROR_TYPE_INFO,
              "generated charity id: %lu\n",
              icc.charity_id);

  return TALER_MHD_REPLY_JSON_PACK (
    rc->connection,
    MHD_HTTP_CREATED,
    GNUNET_JSON_pack_uint64 ("charity-id",
                             icc.charity_id));
}


/* end of donau-httpd_charity_insert.c */
