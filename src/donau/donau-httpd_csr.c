/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation; either version 3,
  or (at your option) any later version.

  TALER is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty
  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General
  Public License along with TALER; see the file COPYING.  If not,
  see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_csr.c
 * @brief Handle /csr requests
 * @author Johannes Casaburi
 */
#include <donau_config.h>
#include <gnunet/gnunet_util_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include <pthread.h>
#include <taler/taler_json_lib.h>
#include <taler/taler_mhd_lib.h>
#include <taler/taler_signatures.h>
#include "donaudb_plugin.h"
#include "donau-httpd_keys.h"
#include "donau-httpd_csr.h"


/**
 * Maximum number of csr records we return per request.
 */
#define MAX_RECORDS 1024


MHD_RESULT
DH_handler_csr_issue (struct DH_RequestContext *rc,
                      const json_t *root,
                      const char *const args[])
{
  struct GNUNET_CRYPTO_CsSessionNonce nonce;
  struct DONAU_DonationUnitHashP du_pub_hash;
  struct GNUNET_CRYPTO_BlindingInputValues ewv = {
    .cipher = GNUNET_CRYPTO_BSA_CS
  };
  struct GNUNET_JSON_Specification spec[] = {
    GNUNET_JSON_spec_fixed_auto ("nonce",
                                 &nonce),
    GNUNET_JSON_spec_fixed_auto ("du_pub_hash",
                                 &du_pub_hash),
    GNUNET_JSON_spec_end ()
  };
  struct DH_DonationUnitKey *dk;

  (void) args;
  {
    enum GNUNET_GenericReturnValue res;

    res = TALER_MHD_parse_json_data (rc->connection,
                                     root,
                                     spec);
    if (GNUNET_OK != res)
      return (GNUNET_SYSERR == res) ? MHD_NO : MHD_YES;
  }

  {
    dk = DH_keys_donation_unit_by_hash (&du_pub_hash);
    if (NULL == dk)
    {
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (
        rc->connection,
        MHD_HTTP_INTERNAL_SERVER_ERROR,
        TALER_EC_DONAU_GENERIC_KEYS_MISSING,
        NULL);
    }
    if (GNUNET_CRYPTO_BSA_CS !=
        dk->donation_unit_pub.bsign_pub_key->cipher)
    {
      /* donation_unit is valid but not for CS */
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (
        rc->connection,
        MHD_HTTP_INTERNAL_SERVER_ERROR,
        TALER_EC_DONAU_GENERIC_KEYS_MISSING,
        NULL);
    }
  }

  /* derive r_pub */
  {
    enum TALER_ErrorCode ec;
    ec = DH_keys_donation_unit_cs_r_pub (&du_pub_hash,
                                         &nonce,
                                         &ewv.details.cs_values);
    if (TALER_EC_NONE != ec)
    {
      GNUNET_break (0);
      return TALER_MHD_reply_with_ec (rc->connection,
                                      ec,
                                      NULL);
    }
  }
  {
    struct TALER_ExchangeWithdrawValues exw = {
      .blinding_inputs = &ewv
    };

    return TALER_MHD_REPLY_JSON_PACK (
      rc->connection,
      MHD_HTTP_CREATED,
      TALER_JSON_pack_exchange_withdraw_values ("ewv",
                                                &exw));
  }
}


/* end of donau-httpd_csr.c */
