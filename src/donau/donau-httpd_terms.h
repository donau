/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_terms.h
 * @brief Handle /terms requests to return the terms of service
 * @author Christian Grothoff
 */
#ifndef DONAU_HTTPD_TERMS_H
#define DONAU_HTTPD_TERMS_H
#include <donau_config.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_json_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include <taler/taler_mhd_lib.h>
#include "donau-httpd.h"


/**
 * Handle a "/terms" request.
 *
 * @param rc request context
 * @param args array of additional options (must be empty for this function)
 * @return MHD result code
 */
MHD_RESULT
DH_handler_terms (struct DH_RequestContext *rc,
                  const char *const args[]);


/**
 * Handle a "/privacy" request.
 *
 * @param rc request context
 * @param args array of additional options (must be empty for this function)
 * @return MHD result code
 */
MHD_RESULT
DH_handler_privacy (struct DH_RequestContext *rc,
                    const char *const args[]);


/**
 * Load our terms of service as per configuration.
 *
 * @param cfg configuration to process
 */
void
DH_load_terms (const struct GNUNET_CONFIGURATION_Handle *cfg);


#endif
