/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU Affero General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file donau-httpd_get-history.c
 * @brief Return history
 * @author Johannes Casaburi
 */
#include "donau_config.h"
#include <gnunet/gnunet_util_lib.h>
#include <jansson.h>
#include <microhttpd.h>
#include "taler/taler_json_lib.h"
#include "taler/taler_mhd_lib.h"
#include "taler/taler_signatures.h"
#include "donau-httpd.h"
#include "donaudb_plugin.h"
#include "donau-httpd_history.h"


/**
 * Maximum number of history we return per request.
 */
// #define MAX_RECORDS 1024

/**
 * Return history information.
 *
 * @param cls closure
 */
enum GNUNET_GenericReturnValue
history_cb (
  void *cls,
  unsigned long long charity_id,
  struct TALER_Amount final_amount,
  uint64_t donation_year)
{
  json_t *history = cls;

  GNUNET_assert (
    0 ==
    json_array_append (
      history,
      GNUNET_JSON_PACK (
        GNUNET_JSON_pack_int64 ("charity_id",
                                charity_id),
        TALER_JSON_pack_amount ("final_amount",
                                &final_amount),
        GNUNET_JSON_pack_int64 ("donation_year",
                                donation_year))));

  return GNUNET_OK;
}


MHD_RESULT
DH_handler_history_get (
  struct DH_RequestContext *rc,
  const char *const args[])
{

  if (NULL != args[1])
  {
    GNUNET_break_op (0);
    return TALER_MHD_reply_with_error (rc->connection,
                                       MHD_HTTP_BAD_REQUEST,
                                       TALER_EC_GENERIC_ENDPOINT_UNKNOWN,
                                       args[1]);
  }

  {
    json_t *history;
    enum GNUNET_DB_QueryStatus qs;

    history = json_array ();
    GNUNET_assert (NULL != history);
    qs = DH_plugin->get_history (DH_plugin->cls,
                                 &history_cb,
                                 history);
    switch (qs)
    {
    case GNUNET_DB_STATUS_HARD_ERROR:
    case GNUNET_DB_STATUS_SOFT_ERROR:
      json_decref (history);
      GNUNET_break (0);
      return TALER_MHD_reply_with_error (rc->connection,
                                         MHD_HTTP_INTERNAL_SERVER_ERROR,
                                         TALER_EC_GENERIC_DB_FETCH_FAILED,
                                         NULL);
    case GNUNET_DB_STATUS_SUCCESS_NO_RESULTS:
      return TALER_MHD_reply_static (
        rc->connection,
        MHD_HTTP_NO_CONTENT,
        NULL,
        NULL,
        0);
    case GNUNET_DB_STATUS_SUCCESS_ONE_RESULT:
      break;
    }
    return TALER_MHD_REPLY_JSON_PACK (
      rc->connection,
      MHD_HTTP_OK,
      GNUNET_JSON_pack_array_steal ("history",
                                    history));
  }
}


/* end of donau-httpd_get-history.c */
