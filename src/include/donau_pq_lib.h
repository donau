/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/donau_pq_lib.h
 * @brief helper functions for DB interactions
 * @author Johannes Casaburi
 * @author Lukas Matyja
 */
#ifndef DONAU_PQ_LIB_H_
#define DONAU_PQ_LIB_H_

#include <gnunet/gnunet_common.h>
#include <gnunet/gnunet_pq_lib.h>
#include "taler/taler_util.h"

/**
 * Generate query parameter for a donation unit public key
 * key. Internally, the various attributes of the
 * public key will be serialized into on variable-size
 * BLOB.
 *
 * @param donation_unit_pub pointer to the query parameter to pass
 */
struct GNUNET_PQ_QueryParam
DONAU_PQ_query_param_donation_unit_pub (
  const struct DONAU_DonationUnitPublicKey *donation_unit_pub);


/**
 * Generate query parameter for an array of blinded donation unit signatures
 *
 * @param num number of elements in @e du_sigs
 * @param du_sigs array of blinded donation unit signatures
 * @param db context for the db-connection
 */
struct GNUNET_PQ_QueryParam
DONAU_PQ_query_param_array_blinded_donation_unit_sig (
  size_t num,
  const struct DONAU_BlindedDonationUnitSignature *du_sigs,
  struct GNUNET_PQ_Context *db
  );


/**
 * Generate query parameter for an array of donation unit signatures
 *
 * @param num number of elements in @e du_sigs
 * @param du_sigs array of donation unit signatures
 * @param db context for the db-connection
 */
struct GNUNET_PQ_QueryParam
DONAU_PQ_query_param_array_donation_unit_sig (
  size_t num,
  const struct DONAU_DonationUnitSignature *du_sigs,
  struct GNUNET_PQ_Context *db
  );


/**
 * Donation unit public key expected.
 *
 * @param name name of the field in the table
 * @param[out] donation_unit_pub where to store the public key
 * @return array entry for the result specification to use
 */
struct GNUNET_PQ_ResultSpec
DONAU_PQ_result_spec_donation_unit_pub (
  const char *name,
  struct DONAU_DonationUnitPublicKey *donation_unit_pub);


/**
 * Array of blinded donation unit signature expected
 *
 * @param db context of the database connection
 * @param name name of the field in the table
 * @param[out] num number of elements in @e denomdu_sigs_sigs
 * @param[out] du_sigs where to store the result
 * @return array entry for the result specification to use
 */
struct GNUNET_PQ_ResultSpec
DONAU_PQ_result_spec_array_blinded_donation_unit_sig (
  struct GNUNET_PQ_Context *db,
  const char *name,
  size_t *num,
  struct DONAU_BlindedDonationUnitSignature **du_sigs);

#endif  /* DONAU_PQ_LIB_H_ */

/* end of include/donau_pq_lib.h */