/*
This file is part of TALER
  Copyright (C) 2014 - 2020 Taler Systems SA

TALER is free software; you can redistribute it and / or modify it under the
terms of the GNU General Public License as published by the Free Software
                                             Foundation; either version 3,
or (at your option) any later version.

TALER is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
                                               TALER; see the file
COPYING.If not, see <http:                        // www.gnu.org/licenses/>
*/
/**
 *@file include/donaudb_lib.h
 * @brief IO operations for the donau's private keys
 * @author Florian Dold
 * @author Benedikt Mueller
 * @author Christian Grothoff
 */
#ifndef DONAUDB_LIB_H
#define DONAUDB_LIB_H

#include "donau_signatures.h"
#include "donaudb_plugin.h"


/**
 * Initialize the plugin.
 *
 * @param cfg configuration to use
 * @return NULL on failure
 */
struct DONAUDB_Plugin *
DONAUDB_plugin_load (const struct GNUNET_CONFIGURATION_Handle *cfg);


/**
 * Shutdown the plugin.
 *
 * @param plugin plugin to unload
 */
void
DONAUDB_plugin_unload (struct DONAUDB_Plugin *plugin);

#endif
