/*
  This file is part of TALER
  Copyright (C) 2014-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/donau_util.h
 * @brief Interface for common utility functions
 * @author Sree Harsha Totakura <sreeharsha@totakura.in>
 */
#ifndef DONAU_UTIL_H
#define DONAU_UTIL_H

#include <gnunet/gnunet_common.h>
#define __DONAU_UTIL_LIB_H_INSIDE__

#include <gnunet/gnunet_util_lib.h>
#include <microhttpd.h>
#include <taler/taler_util.h>
#include "donau_crypto_lib.h"

/**
 * Return default project data used by Taler.
 */
const struct GNUNET_OS_ProjectData *
DONAU_project_data (void);


// FIXME: remove public key, only needed for testing.
/**
  * Generates the String for the QR Code containing the donation statement.
  *
  * The returned string will be freshly allocated, and must be free'd
  * with #GNUNET_free().
  */
char *
generate_QR_string (const struct DONAU_DonauPublicKeyP *pub_key,
                    const struct DONAU_DonationStatement *donation_statement);

#endif
