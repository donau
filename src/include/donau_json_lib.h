/*
  This file is part of TALER
  Copyright (C) 2024 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software
  Foundation; either version 3, or (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
  A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
*/
/**
 * @file include/donau_json_lib.h
 * @brief helper functions for JSON processing using libjansson
 * @author Lukas Matyja
 */
#ifndef DONAU_JSON_LIB_H_
#define DONAU_JSON_LIB_H_

#include <jansson.h>
#include <gnunet/gnunet_json_lib.h>
#include <gnunet/gnunet_curl_lib.h>
#include <gnunet/gnunet_json_lib.h>
#include <taler/taler_util.h>
#include <taler/taler_error_codes.h>
#include "donau_util.h"
#include <taler/taler_json_lib.h>

/**
 * Generate a parser specification for a donation unit public key.
 *
 * @param field name of the field
 * @param[out] pk key to fill
 * @return corresponding field spec
 */
struct GNUNET_JSON_Specification
DONAU_JSON_spec_donation_unit_pub (
  const char *field,
  struct DONAU_DonationUnitPublicKey *pk);


/**
 * Generate a parser for a group of donation units.
 *
 * @param[in] field name of the field, maybe NULL
 * @param[in] currency name of the currency
 * @param[out] group donation unit group information
 * @return corresponding field spec
 */
struct GNUNET_JSON_Specification
DONAU_JSON_spec_donation_unit_group (const char *field,
                                     const char *currency,
                                     struct DONAU_DonationUnitGroup *group);


/**
 * Generate line in parser specification for a
 * blinded unique identifier.
 *
 * @param field name of the field
 * @param[out] blinded_udi the blinded unique identifier to initialize
 * @return corresponding field spec
 */
struct GNUNET_JSON_Specification
DONAU_JSON_spec_blinded_donation_identifier (
  const char *field,
  struct DONAU_BlindedUniqueDonorIdentifier *blinded_udi);


/**
 * Generate line in parser specification for donation unit signature.
 *
 * @param field name of the field
 * @param[out] sig the signature to initialize
 * @return corresponding field spec
 */
struct GNUNET_JSON_Specification
DONAU_JSON_spec_blinded_donation_unit_sig (const char *field,
                                           struct
                                           DONAU_BlindedDonationUnitSignature *
                                           sig);


/**
 * Generate packer instruction for a JSON field of type
 * unsigned integer.
 *
 * @param name name of the field to add to the object
 * @param num numeric value
 * @return json pack specification
 */
struct GNUNET_JSON_PackSpec
DONAU_JSON_pack_uint32 (const char *name,
                        uint64_t num);

/**
 * Generate packer instruction for a JSON field of type
 * denomination public key.
 *
 * @param name name of the field to add to the object
 * @param pk public key
 * @return json pack specification
 */
struct GNUNET_JSON_PackSpec
DONAU_JSON_pack_donation_unit_pub (
  const char *name,
  const struct DONAU_DonationUnitPublicKey *pk);


/**
 * Generate packer instruction for a JSON field of type
 * donation unit signature.
 *
 * @param name name of the field to add to the object
 * @param sig signature
 * @return json pack specification
 */
struct GNUNET_JSON_PackSpec
DONAU_JSON_pack_donation_unit_sig (
  const char *name,
  const struct DONAU_DonationUnitSignature *sig);


/**
 * Generate packer instruction for a JSON field of type
 * blinded donation unit signature (that needs to be
 * unblinded before it becomes valid).
 *
 * @param name name of the field to add to the object
 * @param sig signature
 * @return json pack specification
 */
struct GNUNET_JSON_PackSpec
DONAU_JSON_pack_blinded_donation_unit_sig (
  const char *name,
  const struct DONAU_BlindedDonationUnitSignature *sig);

/**
 * Generate packer instruction for a JSON field of type
 * blinded udi.
 *
 * @param name name of the field to add to the object
 * @param blinded_udi blinded unique donation identifier
 * @return json pack specification
 */
struct GNUNET_JSON_PackSpec
DONAU_JSON_pack_blinded_donation_identifier (
  const char *name,
  const struct DONAU_BlindedUniqueDonorIdentifier *blinded_udi);

#endif
