/*
  This file is part of TALER
  (C) 2018-2023 Taler Systems SA

  TALER is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3, or
  (at your option) any later version.

  TALER is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with TALER; see the file COPYING.  If not, see
  <http://www.gnu.org/licenses/>
*/

/**
 * @file include/donau_testing_lib.h
 * @brief API for writing an interpreter to test Taler components
 * @author Christian Grothoff <christian@grothoff.org>
 * @author Marcello Stanisci
 * @author Lukas Matyja
 *
 */
#ifndef DONAU_TESTING_LIB_H
#define DONAU_TESTING_LIB_H

#include <stdint.h>
#include <taler/taler_testing_lib.h>
#include "donau_service.h"


/**
 * Connects to the exchange.
 *
 * @param label command label
 * @param cfg configuration to use
 * @param wait_for_keys block until we got /keys
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_get_donau (
  const char *label,
  const struct GNUNET_CONFIGURATION_Handle *cfg,
  bool wait_for_keys);

/**
 * Create a GET "charity" command.
 *
 * @param label the command label.
 * @param charity_reference reference for traits
 * @param bearer authorization token
 * @param expected_response_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_charity_get (const char *label,
                               const char *charity_reference,
                               const struct DONAU_BearerToken *bearer,
                               unsigned int expected_response_code);

/**
 * Create a POST "charity" command.
 *
 * @param label the command label.
 * @param name of the charity
 * @param url of the charity
 * @param max_per_year max donation receipt amount per year
 * @param receipts_to_date
 * @param bearer authorization token
 * @param expected_response_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_charity_post (const char *label,
                                const char *name,
                                const char *url,
                                const char *max_per_year,
                                const char *receipts_to_date,
                                uint64_t current_year,
                                const struct DONAU_BearerToken *bearer,
                                unsigned int expected_response_code);

/**
 * Create a DELETE "charity" command.
 *
 * @param label the command label.
 * @param charity_reference reference for traits
 * @param bearer authorization token
 * @param expected_response_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_charity_delete (const char *label,
                                  const char *charity_reference,
                                  const struct DONAU_BearerToken *bearer,
                                  unsigned int expected_response_code);

/**
 * Create a GET "charities" command.
 *
 * @param label the command label.
 * @param bearer authorization token
 * @param expected_response_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_charities_get (const char *label,
                                 const struct DONAU_BearerToken *bearer,
                                 unsigned int expected_response_code);

/**
 * Create a POST "batch-issue" command.
 *
 * @param label the command label.
 * @param charity_reference reference for traits
 * @param uses_cs true for cs and false for rsa.
 * @param year current year (mostly)
 * @param donor_tax_id tax id of the donor
 * @param salt for tax id hash
 * @param expected_response_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_issue_receipts (const char *label,
                                  const char *charity_reference,
                                  const bool uses_cs,
                                  const uint64_t year,
                                  const char *donor_tax_id,
                                  const char *salt,
                                  unsigned int expected_response_code);


/**
 * Create a POST "batch-issue" command.
 *
 * @param label the command label.
 * @param issue_receipt_reference reference for traits
 * @param year current or past year (mostly)
 * @param expected_response_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_submit_receipts (const char *label,
                                   const char *issue_receipt_reference,
                                   const uint64_t year,
                                   unsigned int expected_response_code);


/**
 * Create a GET "donation-statement" command.
 *
 * @param label the command label.
 * @param year current or past year (mostly)
 * @param expected_response_code expected HTTP response code.
 * @return the command.
 */
struct TALER_TESTING_Command
TALER_TESTING_cmd_donation_statement_get (const char *label,
                                          uint64_t year,
                                          unsigned int expected_response_code);


/**
 * Convenience function to run a test.
 *
 * @param argv command-line arguments given
 * @param loglevel log level to use
 * @param cfg_file configuration file to use
 * @param[in,out] cred global credentials to initialize
 * @param main_cb main test function to run
 * @param main_cb_cls closure for @a main_cb
 * @return 0 on success, 77 on setup trouble, non-zero process status code otherwise
 */
int
DONAU_TESTING_main (char *const *argv,
                    const char *loglevel,
                    const char *cfg_file,
                    struct TALER_TESTING_Credentials *cred,
                    TALER_TESTING_Main main_cb,
                    void *main_cb_cls);

/* ****************** convenience functions ************** */

/**
 * Get donau URL from interpreter. Convenience function.
 *
 * @param is interpreter state.
 * @return the donau URL, or NULL on error
 */
const char *
TALER_TESTING_get_donau_url (
  struct TALER_TESTING_Interpreter *is);

/* ****** Specific traits supported by this component ******* */


/**
 * Call #op on all simple traits.
 */
#define DONAU_TESTING_SIMPLE_TRAITS(op) \
  op (charity_priv, const struct DONAU_CharityPrivateKeyP)        \
  op (charity_pub, const struct DONAU_CharityPublicKeyP)        \
  op (charity_id, const unsigned long long)        \
  op (donau_url, const char)                                                \
  op (donau_keys, struct DONAU_Keys) \
  op (donor_salt, const char)                                                \
  op (donor_tax_id, const char)                                                \
  op (salted_tax_id_hash, const struct DONAU_HashDonorTaxId) \
  op (donation_receipts, const struct DONAU_DonationReceipt*) \
  op (number_receipts, const size_t)


/**
 * Call #op on all indexed traits.
 */
#define DONAU_TESTING_INDEXED_TRAITS(op)                                          \
  op (donation_unit_pub, const struct DONAU_DonationUnitInformation)        \
  op (donau_pub, const struct TALER_ExchangePublicKeyP)

DONAU_TESTING_SIMPLE_TRAITS (TALER_TESTING_MAKE_DECL_SIMPLE_TRAIT)
DONAU_TESTING_INDEXED_TRAITS (TALER_TESTING_MAKE_DECL_INDEXED_TRAIT)

#endif
